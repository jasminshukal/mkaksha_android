package com.zetta10.mkaksha.PushNotification;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.legacy.content.WakefulBroadcastReceiver;

import com.zetta10.mkaksha.Activity.MainActivity;
import com.zetta10.mkaksha.Utility.MyApp;
import com.zetta10.mkaksha.Utility.Session_for_notification;

public class FirebaseBackgroundService extends WakefulBroadcastReceiver {

    Session_for_notification session;
    int title_key = 0, desc_key = 0;
    String title,desc;
    @Override
    public void onReceive(Context context, Intent intent) {

        session = new Session_for_notification(MyApp.getInstance());
       // Log.e("intent_title",intent.getExtras().getString("message")/*.get("title")*/.toString());
        for (String key : intent.getExtras().keySet()){
            if (key.equals("gcm.notification.title")) {
                Object values = intent.getExtras().get(key);
                title = String.valueOf(values);
                //intent.getData();
            }if (key.equals("gcm.notification.body")){
                Object values = intent.getExtras().get(key);
                desc = String.valueOf(values);
            }
        }
        title_key = session.getTitle_key();
        desc_key = session.getDesc_key();

        session.setTitle(title);
        session.setDesc(desc);
        session.setTitle_key();
        session.setDesc_key();

        /*Intent intent1 = new Intent(context, MainActivity.class);
        context.startActivity(intent1);*/
        /*Log.e("intent_body",intent.getExtras().get("body").toString());
        Log.e("intent_data",intent.getExtras().get("data").toString());*/
        Toast.makeText(context, "on receive", Toast.LENGTH_SHORT).show();
    }
}
