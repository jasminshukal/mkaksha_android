package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.Screenshot;
import com.zetta10.mkaksha.Adapter.Adapter_all_subject_result;
import com.zetta10.mkaksha.Adapter.Adapter_for_attempted_test;
import com.zetta10.mkaksha.Adapter.Adapter_for_not_attempted_test;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Model.pojoclass.Data_not_attempted_test;
import com.zetta10.mkaksha.Model.pojoclass.Main_not_attempted_test_list;
import com.zetta10.mkaksha.Model.pojoclass.Model_for_all_subject;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Allsubject extends Fragment {

    ArrayList<Model_for_all_subject> all_subjectArrayList;
    RecyclerView recyclerview_for_all_subject;
    Adapter_for_attempted_test adapter_for_attempted_test;
    ProgressDialog progressDialog;
    LinearLayout ly_share;
    CardView cardview_for_result;
    Context context;
    TextView textview_stud_name, textview_for_standard, textview_for_percentage, textview_total_marks, textview_total_obtain_mark,text_for_pass_or_fail;
    PrefManager prefManager;
    String total_sub_marks, obtain_sub_marks, fname, lname, std, percentage, total_marks, obtain_mark;

    public Allsubject() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_allsubject, container, false);
        all_subjectArrayList = new ArrayList<>();
        prefManager = new PrefManager(getActivity());
        ly_share = view.findViewById(R.id.ly_share);
        cardview_for_result = view.findViewById(R.id.cardview_for_result);
        textview_stud_name = view.findViewById(R.id.textview_stud_name);
        textview_for_standard = view.findViewById(R.id.textview_for_standard);
        textview_for_percentage = view.findViewById(R.id.textview_for_percentage);
        textview_total_marks = view.findViewById(R.id.textview_total_marks);
        recyclerview_for_all_subject = view.findViewById(R.id.recyclerview_for_all_subject);
        textview_total_obtain_mark = view.findViewById(R.id.textview_total_obtain_mark);
        text_for_pass_or_fail = view.findViewById(R.id.text_for_pass_or_fail);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        get_allsubject_result();

        ly_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bitmap bitmap = Screenshot.takescreenshotOfRootView(cardview_for_result);
                store(bitmap, "result.png");
                Log.e("bitmap", bitmap.toString());
            }
        });
        return view;
    }

    public void store(Bitmap bm, String fileName) {

        Log.e("bitmap", bm.toString());
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            shareImage(file);
            Log.e("bitmap", bm.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareImage(File file) {
        Uri uri = FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".provider", file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            startActivity(Intent.createChooser(intent, "Share Screenshot"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), "No App Available", Toast.LENGTH_SHORT).show();
        }
    }

    public void get_allsubject_result() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> all_subject_result_Call = apiinterface.all_subject_result(prefManager.gettokenId(), prefManager.getLoginId());

        all_subject_result_Call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                all_subjectArrayList.clear();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                        JSONObject jsonObject_result = jsonObject_data.getJSONObject("Result");
                        JSONObject jsonObject_stud_detail = jsonObject_data.getJSONObject("student_detail");
                        fname = jsonObject_stud_detail.getString("fname");
                        lname = jsonObject_stud_detail.getString("lname");
                        JSONObject jsonObject_s_detail = jsonObject_stud_detail.getJSONObject("detail");
                        std = jsonObject_s_detail.getString("current_std");
                        JSONObject jsonObject_all_result = jsonObject_data.getJSONObject("all_result");
                        total_marks = jsonObject_all_result.getString("total_all_mark");
                        obtain_mark = jsonObject_all_result.getString("total_all_obtain");
                        percentage = jsonObject_all_result.getString("total_all_persontage");

                        setdata();

                        Iterator subject_keys = jsonObject_result.keys();

                        while (subject_keys.hasNext()) {
                            total_sub_marks = "";
                            obtain_sub_marks = "";
                            String currentsubjectkey = (String) subject_keys.next();
                            JSONObject jsonObject_subject = jsonObject_result.getJSONObject(currentsubjectkey);
                            if (jsonObject_subject.length() > 0) {
                                total_sub_marks = jsonObject_subject.getString("total");
                                obtain_sub_marks = jsonObject_subject.getString("obtain");
                            }
                            all_subjectArrayList.add(new Model_for_all_subject(currentsubjectkey, total_sub_marks, obtain_sub_marks));
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        recyclerview_for_all_subject.setLayoutManager(layoutManager);

                        Adapter_all_subject_result adapter_all_subject_result = new Adapter_all_subject_result(all_subjectArrayList);
                        recyclerview_for_all_subject.setAdapter(adapter_all_subject_result);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void setdata() {

        textview_stud_name.setText(fname + " " + lname);
        textview_for_standard.setText(std);
        textview_for_percentage.setText(percentage);
        textview_total_marks.setText(total_marks);
        textview_total_obtain_mark.setText(obtain_mark);
        float percent = Float.parseFloat(percentage);
        if (percent >= 35){
            text_for_pass_or_fail.setText("Congratulation you pass the Exam");
        }else {
            text_for_pass_or_fail.setText("Sorry you fail in this exam");
        }
    }
}
