package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Adapter.Adapter_for_attempted_test;
import com.zetta10.mkaksha.Adapter.Adapter_for_not_attempted_test;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Model.pojoclass.Data_not_attempted_test;
import com.zetta10.mkaksha.Model.pojoclass.Main_not_attempted_test_list;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotAttempted_testFragment extends Fragment {

    ArrayList<Data_not_attempted_test> not_attempted_testslist;
    RecyclerView recyclerview_not_attempted_test;
    Adapter_for_attempted_test adapter_for_attempted_test;
    ProgressDialog progressDialog;
    PrefManager prefManager;

    public NotAttempted_testFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_not_attempted_test, container, false);
        recyclerview_not_attempted_test = view.findViewById(R.id.recyclerview_not_attempted_test);
        not_attempted_testslist = new ArrayList<>();
        prefManager = new PrefManager(getActivity());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        get_not_attempted_test();
        return view;
    }

    public void get_not_attempted_test() {
        try {
            progressDialog.show();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<Main_not_attempted_test_list> attempted_test_Call = apiinterface.getNotattempted_test(prefManager.gettokenId(), prefManager.getLoginId());

            attempted_test_Call.enqueue(new Callback<Main_not_attempted_test_list>() {
                @Override
                public void onResponse(Call<Main_not_attempted_test_list> call, Response<Main_not_attempted_test_list> response) {
                    not_attempted_testslist.clear();
                    try {
                        if (response.body().getStatus().equals("1")) {
                            for (int i = 0; i < response.body().data.size(); i++) {

                                Data_not_attempted_test data_not_attempted_test = new Data_not_attempted_test();
                                data_not_attempted_test.setName(response.body().data.get(i).getName());
                                data_not_attempted_test.setStart_date(response.body().data.get(i).getStart_date());

                                not_attempted_testslist.add(data_not_attempted_test);
                            }
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                            recyclerview_not_attempted_test.setLayoutManager(layoutManager);

                            Adapter_for_not_attempted_test adapter_for_not_attempted_test = new Adapter_for_not_attempted_test(getContext(), not_attempted_testslist);
                            recyclerview_not_attempted_test.setAdapter(adapter_for_not_attempted_test);

                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {
                        Log.e("n1_error==>", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Main_not_attempted_test_list> call, Throwable t) {
                    Log.e("n_t==>", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Log.e("n2_error==>", "" + e.getMessage());
        }
    }
}
