package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonIOException;
import com.zetta10.mkaksha.Activity.Test_history_Activity;
import com.zetta10.mkaksha.Adapter.Adapter_for_ava_test;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Model.pojoclass.Available_test_list_pojo;
import com.zetta10.mkaksha.Model.pojoclass.Available_test_main_pojo;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailableTestFragment extends Fragment {

    RecyclerView recyclerView_for_avail_test;
    ArrayList<Available_test_list_pojo> arrayList_available_test;
    Adapter_for_ava_test adapter_for_ava_test;
    ProgressDialog progressDialog;
    LinearLayout linear_for_test_history, ly_dataNotFound;
    String current_time, current_date, test_time, test_date, current_date_time, start_date_time;
    Date date1;
    Date date2;
    PrefManager prefManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_available_test, container, false);
        recyclerView_for_avail_test = view.findViewById(R.id.recyclerview_available_test);
        ly_dataNotFound = view.findViewById(R.id.ly_dataNotFound);
        arrayList_available_test = new ArrayList();
        linear_for_test_history = view.findViewById(R.id.linear_for_test_history);
        prefManager = new PrefManager(getContext());

        /*prefManager.setValue("login_token", login_token);
        prefManager.setValue("user_id", user_id);*/

        linear_for_test_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), Test_history_Activity.class);
                startActivity(intent);
            }
        });
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        getavailable_test();
        return view;
    }

    public void getavailable_test() {
        try {
            progressDialog.show();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<Available_test_main_pojo> available_test_call = apiinterface.getavailable_test(prefManager.gettokenId(), prefManager.getLoginId());

            available_test_call.enqueue(new Callback<Available_test_main_pojo>() {
                @Override
                public void onResponse(Call<Available_test_main_pojo> call, Response<Available_test_main_pojo> response) {
                    arrayList_available_test.clear();
                    try {
                        if (response.body().getStatus().equals("1")) {
                            for (int i = 0; i < response.body().data.availableTest.size(); i++) {

                                Available_test_list_pojo available_test_list_pojo = new Available_test_list_pojo();
                                available_test_list_pojo.setName(response.body().data.availableTest.get(i).getName());
                                available_test_list_pojo.setStartDate(response.body().data.availableTest.get(i).getStartDate());
                                available_test_list_pojo.setStartTimeOnly(response.body().data.availableTest.get(i).getStartTimeOnly());
                                available_test_list_pojo.setCurrentDate(response.body().data.availableTest.get(i).getCurrentDate());
                                available_test_list_pojo.setCurrentTime(response.body().data.availableTest.get(i).getCurrentTime());
                                available_test_list_pojo.setCurrentDateTime(response.body().data.availableTest.get(i).getCurrentDateTime());
                                available_test_list_pojo.setStartTime(response.body().data.availableTest.get(i).getStartTime());
                                available_test_list_pojo.setDuration(response.body().data.availableTest.get(i).getDuration());
                                available_test_list_pojo.setId(response.body().data.availableTest.get(i).getId());
                                available_test_list_pojo.setIsGift(response.body().data.availableTest.get(i).getIsGift());
                                available_test_list_pojo.setGiftDescription(response.body().data.availableTest.get(i).getGiftDescription());
                                arrayList_available_test.add(available_test_list_pojo);
                            }
                            if (arrayList_available_test.size() > 0) {
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                                recyclerView_for_avail_test.setLayoutManager(layoutManager);

                                Adapter_for_ava_test adapter_for_ava_test = new Adapter_for_ava_test(getContext(), arrayList_available_test);
                                recyclerView_for_avail_test.setAdapter(adapter_for_ava_test);
                                progressDialog.dismiss();
                            }else {
                                recyclerView_for_avail_test.setVisibility(View.GONE);
                                ly_dataNotFound.setVisibility(View.VISIBLE);
                            }
                        }

                    } catch (JsonIOException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<Available_test_main_pojo> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
        }
    }
   /* public void getavailable_test() {
        //  progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<Available_test_main_pojo> available_test_call = apiinterface.getavailable_test(login_token, user_id);

        available_test_call.enqueue(new Callback<Available_test_main_pojo>() {
            @Override
            public void onResponse(Call<Available_test_main_pojo> call, Response<Available_test_main_pojo> response) {

                try {
                    if (response.body().getStatus().equals("1")) {
                        for (int i = 0; i < response.body().data.availableTest.size(); i++) {

                            Available_test_list_pojo available_test_list_pojo = new Available_test_list_pojo();
                            available_test_list_pojo.setName(response.body().data.availableTest.get(i).getName());


                            arrayList_available_test.add(available_test_list_pojo);
                        }
                        for (int j = 0; j < 1; j++) {
                            dummy_arrayList_available_test.add(arrayList_available_test.get(j));
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        recyclerView_for_avail_test.setLayoutManager(layoutManager);

                        adapter_for_attempted_test = new Adapter_for_attempted_test(getContext(), dummy_arrayList_available_test);
                        adapter_for_attempted_test.setCallback(AvailableTestFragment.this);
                        adapter_for_attempted_test.setWithFooter(true);
                        recyclerView_for_avail_test.setAdapter(adapter_for_attempted_test);
                        //   progressDialog.dismiss();
                    }

                } catch (JsonIOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Available_test_main_pojo> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClickLoadMore() {

        adapter_for_attempted_test.setWithFooter(false);

        for (int i = 2; i < arrayList_available_test.size(); i++) {

            dummy_arrayList_available_test.add(arrayList_available_test.get(i));
        }
        adapter_for_attempted_test.notifyDataSetChanged();

    }*/
}
