package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Adapter.Adapter_for_each_subject;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Model.pojoclass.Each_subject_pojo;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Eachsubject extends Fragment {

    ArrayList<Each_subject_pojo> each_sub_list;
    RecyclerView recyclerview_each_subject;
    Adapter_for_each_subject adapter_for_each_subject;
    ProgressDialog progressDialog;
    String login_token;
    String user_id;
    PrefManager prefManager;
    LinearLayout ly_dataNotFound;

    public Eachsubject() {

        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_each_subject, container, false);
        recyclerview_each_subject = view.findViewById(R.id.recyclerview_each_subject);
        ly_dataNotFound = view.findViewById(R.id.ly_dataNotFound);
        each_sub_list = new ArrayList<>();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getContext());

        get_each_sub_result();


        return view;
    }

    public void get_each_sub_result() {
        progressDialog.show();
        Log.e("ee==>", "in");
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> attempted_test_Call = apiinterface.get_each_sub_result(prefManager.gettokenId(), prefManager.getLoginId());

        attempted_test_Call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                each_sub_list.clear();
                try {
                    Log.e("ee==>", "in1");
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    /*for (int i = 0; i < jsonObject_data.length(); i++) {*/
                    Iterator keys = jsonObject_data.keys();
                    while (keys.hasNext()) {
                        Log.e("ee==>", "in2");
                        String currentdynamickey = (String) keys.next();
                        JSONObject currentdynamicvalue = jsonObject_data.getJSONObject(currentdynamickey);
                        if (currentdynamicvalue.length() > 0) {

                            String correct = currentdynamicvalue.getString("obtain");
                            String wrong = currentdynamicvalue.getString("total");
                            String percentage = currentdynamicvalue.getString("percentage");
                            each_sub_list.add(new Each_subject_pojo(currentdynamickey, correct, wrong, percentage));
                        }

                    }

                    if (each_sub_list.size() > 0) {
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        recyclerview_each_subject.setLayoutManager(layoutManager);

                        Adapter_for_each_subject adapter_for_each_subject = new Adapter_for_each_subject(getContext(), each_sub_list);
                        recyclerview_each_subject.setAdapter(adapter_for_each_subject);
                    } else {
                        recyclerview_each_subject.setVisibility(View.GONE);
                        ly_dataNotFound.setVisibility(View.VISIBLE);
                    }
                    /*}*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("ee==>", "" + e.getMessage());
                    progressDialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("ee==>", "" + e.getMessage());
                    progressDialog.dismiss();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
