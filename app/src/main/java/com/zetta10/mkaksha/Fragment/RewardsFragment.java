package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonIOException;
import com.zetta10.mkaksha.Adapter.Adapter_for_reward;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Model.RewardModel.Data_Reward_Model;
import com.zetta10.mkaksha.Model.RewardModel.Main_Reward_model;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RewardsFragment extends Fragment {

    RecyclerView recyclerview_reward;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    ArrayList<Data_Reward_Model> reward_ArrayList;
    LinearLayout ly_dataNotFound;

    public RewardsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.rewards_fragment, container, false);

        recyclerview_reward = view.findViewById(R.id.recyclerview_reward);
        ly_dataNotFound = view.findViewById(R.id.ly_dataNotFound);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getContext());
        reward_ArrayList = new ArrayList<>();
        get_reward_detail();
        return view;
    }

    public void get_reward_detail() {
        try {
            progressDialog.show();

            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<Main_Reward_model> reward_modelCall = apiinterface.get_reward_detail(prefManager.gettokenId(), prefManager.getLoginId());

            reward_modelCall.enqueue(new Callback<Main_Reward_model>() {
                @Override
                public void onResponse(Call<Main_Reward_model> call, Response<Main_Reward_model> response) {
                    reward_ArrayList.clear();
                    try {
                        if (response.body().getStatus() == 1) {


                            for (int i = 0; i < response.body().data.size(); i++) {

                                Data_Reward_Model data_reward_model = new Data_Reward_Model();
                                data_reward_model.setGiftDescription(response.body().data.get(i).getGiftDescription());
                                data_reward_model.setImg(response.body().data.get(i).getImg());
                                data_reward_model.setAdded_date(response.body().data.get(i).getAdded_date());
                                reward_ArrayList.add(data_reward_model);
                            }

                            if (reward_ArrayList.size() > 0) {
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                                recyclerview_reward.setLayoutManager(layoutManager);

                                Adapter_for_reward adapter_for_reward = new Adapter_for_reward(getContext(), reward_ArrayList);
                                recyclerview_reward.setAdapter(adapter_for_reward);
                                progressDialog.dismiss();
                            } else {
                                ly_dataNotFound.setVisibility(View.VISIBLE);
                                recyclerview_reward.setVisibility(View.GONE);

                            }
                        }


                    } catch (JsonIOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<Main_Reward_model> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
        }
    }
}
