package com.zetta10.mkaksha.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zetta10.mkaksha.Activity.TestDetailActivity;
import com.zetta10.mkaksha.R;


public class ResultFragment extends Fragment {

    LinearLayout linear_for_thismonth, linear_for_3month, linear_for_6month, linear_for_9month, linear_for_12month;
    LinearLayout linear_for_test_history;
    TextView textview_for_test_this_month, textview_for_3month, textview_for_6month, textview_for_test_month9, textview_for_test_month12,textview_for_test_current_month;

    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        linear_for_test_history = view.findViewById(R.id.linear_for_test_history);
        linear_for_thismonth = view.findViewById(R.id.linear_for_thismonth);
        linear_for_3month = view.findViewById(R.id.linear_for_3month);
        linear_for_6month = view.findViewById(R.id.linear_for_6month);
        linear_for_9month = view.findViewById(R.id.linear_for_9month);
        linear_for_12month = view.findViewById(R.id.linear_for_12month);

        textview_for_test_current_month = view.findViewById(R.id.textview_for_test_current_month);
        textview_for_test_this_month = view.findViewById(R.id.textview_for_test_this_month);
        textview_for_3month = view.findViewById(R.id.textview_for_3month);
        textview_for_6month = view.findViewById(R.id.textview_for_6month);
        textview_for_test_month9 = view.findViewById(R.id.textview_for_test_month9);
        textview_for_test_month12 = view.findViewById(R.id.textview_for_test_month12);


        linear_for_thismonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String get_month = textview_for_test_current_month.getText().toString();
                Intent intent = new Intent(getContext(), TestDetailActivity.class);
                intent.putExtra("month", get_month);
                startActivity(intent);
            }
        });
        /*linear_for_3month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String get_month = textview_for_3month.getText().toString();
                Intent intent = new Intent(getContext(), TestDetailActivity.class);
                intent.putExtra("month", get_month);
                startActivity(intent);
            }
        });*/
        linear_for_6month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String get_month = textview_for_6month.getText().toString();
                Intent intent = new Intent(getContext(), TestDetailActivity.class);
                intent.putExtra("month", get_month);
                startActivity(intent);
            }
        });
       /* linear_for_9month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String get_month = textview_for_test_month9.getText().toString();
                Intent intent = new Intent(getContext(), TestDetailActivity.class);
                intent.putExtra("month", get_month);
                startActivity(intent);
            }
        });*/
        linear_for_12month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String get_month = textview_for_test_month12.getText().toString();
                Intent intent = new Intent(getContext(), TestDetailActivity.class);
                intent.putExtra("month", get_month);
                startActivity(intent);
            }
        });
        /*linear_for_test_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
        return view;
    }
}
