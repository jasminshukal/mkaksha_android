package com.zetta10.mkaksha.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.viewpagerindicator.LinePageIndicator;
import com.zetta10.mkaksha.Activity.LoginActivity;
import com.zetta10.mkaksha.Activity.PopupActivity;
import com.zetta10.mkaksha.Activity.ResumeJournyActivity;
import com.zetta10.mkaksha.Adapter.BannerAdapter;
import com.zetta10.mkaksha.Adapter.SubjectListAdapter;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.BuildConfig;
import com.zetta10.mkaksha.Model.BannerListModel.BannerListModel;
import com.zetta10.mkaksha.Model.SubjectListModel.Data;
import com.zetta10.mkaksha.Model.SubjectListModel.SubjectListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends android.app.Fragment {
    PrefManager prefManager;
    ProgressDialog progressDialog;
    RecyclerView rv_subject_list;
    List<Data> subject_dataList;
    String subjectListModels;
    SubjectListAdapter subjectListAdapter;
    String user_id;
    String api_token, payment_array_size = String.valueOf(0);
    String status, resume_image, resume_video_data;
    List<com.zetta10.mkaksha.Model.BannerListModel.Data> BannerList;
    ViewPager mViewPager;
    LinePageIndicator linePageIndicator;
    BannerAdapter bannerAdapter;
    Timer timer;
    LinearLayout ly_share;
    TextView txt_coin, txt_subject, txt_title;
    Dialog dialog;
    String strtext = String.valueOf(0);
    private SwipeRefreshLayout mSwipeRefreshLayout;
    LinearLayout ly_dataNotFound,ly_resume;
    ImageView iv_thumb_resume_journey;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        /*KNetwork.INSTANCE.bind(getActivity(), getLifecycle).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });*/
//        String strtext = getArguments().getString("payment_data");

        prefManager = new PrefManager(getActivity());
        rv_subject_list = view.findViewById(R.id.rv_subject_list);
        txt_title = view.findViewById(R.id.txt_title);
        ly_resume = view.findViewById(R.id.ly_resume);
        ly_dataNotFound = view.findViewById(R.id.ly_dataNotFound);
        mViewPager = view.findViewById(R.id.viewPager);
        linePageIndicator = view.findViewById(R.id.line_indictor);
        mSwipeRefreshLayout = view.findViewById(R.id.container);
        ly_share = view.findViewById(R.id.ly_share);
        txt_subject = view.findViewById(R.id.txt_subject);
        iv_thumb_resume_journey = view.findViewById(R.id.iv_thumb_resume_journey);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("token==>", "" + refreshedToken);

        resume_image = prefManager.getValue("resume_image");
        resume_video_data = prefManager.getValue("resume_video_data");

        Log.e("resume_image==>", "" + resume_image);
        Log.e("resume_video_data==>", "" + resume_video_data);

      /*  FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Toast.makeText(getActivity(), "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();*/
//        strtext = prefManager.getValue("p_data");
   /*     strtext = prefManager.get_check_payment();
        Log.e("strtext==>", "" + strtext);*/

        int c1 = getResources().getColor(R.color.splash_color);
        int c2 = getResources().getColor(R.color.login_blue);
        int c3 = getResources().getColor(R.color.colorPrimaryDark);

        mSwipeRefreshLayout.setColorSchemeColors(c1, c2, c3);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                get_payment_detail();
                DisplayBanner();
                subject_list_Data();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        get_payment_detail();
        DisplayBanner();
        subject_list_Data();

        String coin = prefManager.getcoin();
        if (!coin.equals("0")) {
            popup_coin(coin);
            prefManager.setcoin("0");
        } else {
            Log.e("error", "");
        }

        ly_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String referral_code = prefManager.getValue("referral_code");
                    if (!referral_code.equals("0")) {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        String shareMessage1 = "\n" + "Refer code : " + referral_code + "\n\n";
                        shareMessage = shareMessage + shareMessage1 + "https://play.google.com/store/apps/details?id="
                                + BuildConfig.APPLICATION_ID + "\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } else {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id="
                                + BuildConfig.APPLICATION_ID + "\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    }
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });

        if (resume_image.equals("0")) {
//            iv_thumb_resume_journey.setVisibility(View.GONE);
//            txt_title.setVisibility(View.GONE);
            ly_resume.setVisibility(View.GONE);
        } else {
            Glide.with(getActivity()).load(resume_image).into(iv_thumb_resume_journey);
            txt_title.setText(resume_video_data);
        }

        iv_thumb_resume_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Log.e("click", "call");
                Intent intent = new Intent(getActivity(), ResumeJournyActivity.class);
                intent.putExtra("video_id", prefManager.getValue_resume("resume_video_id"));
                intent.putExtra("video_data", prefManager.getValue("resume_video_data"));
                intent.putExtra("seek_current", prefManager.getValue("seek_current_position"));
                intent.putExtra("current_resume_image", prefManager.getValue("resume_image"));
                Log.e("video_id==>", "" + prefManager.getValue("resume_video_data"));
                getActivity().startActivity(intent);
            }
        });
        return view;
    }

    private void popup_coin(String coin) {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.coin_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_coin = dialog.findViewById(R.id.txt_coin);
        txt_coin.setText("You Earn " + coin + " Coins");

        ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
        dialog.show();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void DisplayBanner() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<BannerListModel> call = bookNPlayAPI.GetSlider(prefManager.gettokenId(), prefManager.getLoginId());
        call.enqueue(new Callback<BannerListModel>() {
            @Override
            public void onResponse(Call<BannerListModel> call, Response<BannerListModel> response) {
                try {
                    progressDialog.dismiss();
                    if (response.code() == 200) {
                        BannerList = new ArrayList<>();
                        BannerList = response.body().getData();
                        Log.e("BannerList", "" + BannerList.size());
                        SetBanner();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<BannerListModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void SetBanner() {
        bannerAdapter = new BannerAdapter(getActivity(), BannerList);
        mViewPager.setAdapter(bannerAdapter);
        linePageIndicator.setViewPager(mViewPager);

        if (BannerList.size() > 0) {
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    mViewPager.post(new Runnable() {
                        @Override
                        public void run() {
                            mViewPager.setCurrentItem((mViewPager.getCurrentItem() + 1) % BannerList.size());
                        }
                    });
                }
            };
            timer = new Timer();
            timer.schedule(timerTask, 10000, 10000);
        }
    }

    private void subject_list_Data() {
        try {
            progressDialog.show();
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            Log.e("login_id", "" + user_id);
            Log.e("==>login_token", "" + api_token);
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<SubjectListModel> call = appAPI.subject_list_call(api_token, user_id);
            call.enqueue(new Callback<SubjectListModel>() {
                @Override
                public void onResponse(Call<SubjectListModel> call, Response<SubjectListModel> response) {
                    subject_dataList = new ArrayList<>();
                    subject_dataList = response.body().getData();
                    prefManager.setValue("std_ids", response.body().getData().get(0).getStdId());
                    status = response.body().getStatus();
                   /* Log.e("status==>", "" + subject_dataList.get(0).getStatus());
                    status = subject_dataList.get(0).getStatus();*/
                    Log.e("status==>", "" + status);
                    if (status.equals("1")) {
                        if (response.isSuccessful()) {
                            Log.e("data1", "" + response.body());
                            Log.e("data2", "" + response.body().getData());
                            Log.e("data3", "" + response.body().getData().get(0).getSubjectImg());
                            if (subject_dataList.size() > 0) {
                                subjectListAdapter = new SubjectListAdapter(getActivity(), subject_dataList, "Home", payment_array_size);
                                rv_subject_list.setHasFixedSize(true);
                                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getActivity(),
                                        LinearLayoutManager.HORIZONTAL, false);
                                GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2,
                                        LinearLayoutManager.VERTICAL, false);
                                rv_subject_list.setLayoutManager(gridLayoutManager);
                                rv_subject_list.setItemAnimator(new DefaultItemAnimator());
                                rv_subject_list.setAdapter(subjectListAdapter);
                                subjectListAdapter.notifyDataSetChanged();
                                rv_subject_list.setVisibility(View.VISIBLE);
                                rv_subject_list.setVisibility(View.VISIBLE);
                            } else {
                                rv_subject_list.setVisibility(View.GONE);
                                txt_subject.setVisibility(View.GONE);
                                ly_dataNotFound.setVisibility(View.VISIBLE);
                            }
//                            progressDialog.dismiss();
                        }
                    }

                    else if (status.equals("2")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "InValid Token", Toast.LENGTH_SHORT).show();
                            prefManager.setLoginId("0");
                            prefManager.settoken("0");
                            String refer_code = String.valueOf(0);
                            prefManager.setValue("referral_code", refer_code);
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                        } catch (Exception e) {
                            Log.e("token_error==>", "" + e.getMessage());
                        }
                    }

                    else if (status.equals("3")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "maintenance", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), PopupActivity.class);
                            intent.putExtra("status_data", status);
                            startActivity(intent);
                            getActivity().finish();
                        } catch (Exception e) {
                            Log.e("error3==>", "" + e.getMessage());
                        }
                    }

                    else if (status.equals("4")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Update", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), PopupActivity.class);
                            intent.putExtra("status_data", status);
                            startActivity(intent);
                            getActivity().finish();
                        } catch (Exception e) {
                            Log.e("error4==>", "" + e.getMessage());
                        }
                    }
//                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<SubjectListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }/*catch (NullPointerException e){
            e.printStackTrace();
        }
*/
    }

    public void get_payment_detail() {
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> check_payment_call = apiinterface.checkpayment(prefManager.gettokenId(), prefManager.getLoginId());

        check_payment_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        // payment_data_list.add("payment_data");
                        payment_array_size = String.valueOf(jsonArray.length());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("==", "" + t.getMessage());
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        /*resume_image = prefManager.getValue("resume_image");
        resume_video_data = prefManager.getValue("resume_video_data");

        Glide.with(getActivity()).load(resume_image).into(iv_thumb_resume_journey);
        txt_title.setText(resume_video_data);*/
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        get_payment_detail();
        DisplayBanner();
        subject_list_Data();
        resume_image = prefManager.getValue("resume_image");
        resume_video_data = prefManager.getValue("resume_video_data");

        Log.e("resume_image1==>", "" + resume_image);
        Log.e("resume_video_data1==>", "" + resume_video_data);
        if (resume_image.equals("0")) {
//            iv_thumb_resume_journey.setVisibility(View.GONE);
//            txt_title.setVisibility(View.GONE);
            ly_resume.setVisibility(View.GONE);
        } else {
            ly_resume.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(resume_image).into(iv_thumb_resume_journey);
            txt_title.setText(resume_video_data);
        }

        /*Glide.with(getActivity()).load(resume_image).into(iv_thumb_resume_journey);
        txt_title.setText(resume_video_data);*/
    }
}

