package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Adapter.Adapter_for_attempted_test;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Model.pojoclass.Data_attempted_test_list;
import com.zetta10.mkaksha.Model.pojoclass.Main_attempted_test_list;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Attempted_test_Fragment extends Fragment {
    ArrayList<Data_attempted_test_list> attemptedTestLists;
    RecyclerView recyclerview_attempted_test;
    Adapter_for_attempted_test adapter_for_attempted_test;
    ProgressDialog progressDialog;
    PrefManager prefManager;

    public Attempted_test_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attempted_test, container, false);
        recyclerview_attempted_test = view.findViewById(R.id.recyclerview_attempted_test);
        attemptedTestLists = new ArrayList<>();
        prefManager = new PrefManager(getActivity());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        get_attempted_test();
        return view;
    }

    public void get_attempted_test() {
        try {
            progressDialog.show();
            attemptedTestLists.clear();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<Main_attempted_test_list> attempted_test_Call = apiinterface.getattempted_test(prefManager.gettokenId(), prefManager.getLoginId());

            attempted_test_Call.enqueue(new Callback<Main_attempted_test_list>() {
                @Override
                public void onResponse(Call<Main_attempted_test_list> call, Response<Main_attempted_test_list> response) {
                    try {
                        attemptedTestLists.clear();
                        if (attemptedTestLists.size() > 0) {
                            if (response.body().getStatus().equals("1")) {
                                for (int i = 0; i < response.body().data.size(); i++) {
                                    Data_attempted_test_list data_attempted_test_list = new Data_attempted_test_list();
                                    data_attempted_test_list.setName(response.body().data.get(i).getName());
                                    data_attempted_test_list.setStart_date(response.body().data.get(i).getStart_date());

                                    attemptedTestLists.add(data_attempted_test_list);
                                }
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                                recyclerview_attempted_test.setLayoutManager(layoutManager);

                                Adapter_for_attempted_test adapter_for_attempted_test = new Adapter_for_attempted_test(getContext(), attemptedTestLists);
                                recyclerview_attempted_test.setAdapter(adapter_for_attempted_test);

                            }
                            progressDialog.dismiss();
                        }
                        progressDialog.dismiss();
                    } catch (Exception e) {
                        Log.e("e==>", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Main_attempted_test_list> call, Throwable t) {
                    Log.e("t==>", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }
    }
}
