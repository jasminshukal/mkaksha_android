package com.zetta10.mkaksha.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Adapter.CoinListAdapter;
import com.zetta10.mkaksha.Model.CoinModel.History;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoinsFragment extends Fragment {

    TextView txt_total_coin;
    RecyclerView rv_coin;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    LinearLayout ly_history, ly_main, ly_dataNotFound;
    NestedScrollView nested_scroll;

    List<History> coin_dataList;

    CoinListAdapter coinListAdapter;

    public CoinsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.coins_fragment, container, false);
        prefManager = new PrefManager(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        rv_coin = view.findViewById(R.id.rv_coin);
        ly_history = view.findViewById(R.id.ly_history);
        ly_main = view.findViewById(R.id.ly_main);
        txt_total_coin = view.findViewById(R.id.txt_total_coin);
        ly_dataNotFound = view.findViewById(R.id.ly_dataNotFound);
        nested_scroll = view.findViewById(R.id.nested_scroll);

        coin_Data();

        return view;
    }

    private void coin_Data() {
        try {
            progressDialog.show();
            Log.e("login_id", "" + prefManager.getLoginId());
            Log.e("==>login_token", "" + prefManager.gettokenId());
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<ResponseBody> call = appAPI.CoinList(prefManager.gettokenId(), prefManager.getLoginId());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    coin_dataList = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = String.valueOf(jsonObject.get("msg"));
                        Log.e("msg", "" + msg);
                        txt_total_coin.setText(jsonObject.getJSONObject("data").getString("total_coin"));
                        Log.e("data=>", "" + jsonObject.getJSONObject("data").getString("total_coin"));

                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("history");
                        JSONObject listObj = null;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            listObj = jsonArray.getJSONObject(i);
                            String id = listObj.getString("id");
                            String operation = listObj.getString("operation");
                            String massage = listObj.getString("massage");
                            String value = listObj.getString("value");
                            String student_id = listObj.getString("student_id");
                            String date = listObj.getString("date");
                            coin_dataList.add(new History(id, operation, massage, value, student_id, date));
                        }
                        if (response.isSuccessful()) {
                            if (coin_dataList.size() > 0) {
                                coinListAdapter = new CoinListAdapter(getActivity(), coin_dataList, "Home");
                                rv_coin.setHasFixedSize(true);
                                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getActivity(),
                                        LinearLayoutManager.VERTICAL, false);
                                GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2,
                                        LinearLayoutManager.VERTICAL, false);

                                rv_coin.setLayoutManager(mLayoutManager3);
                                rv_coin.setItemAnimator(new DefaultItemAnimator());
                                rv_coin.setAdapter(coinListAdapter);
//                                coinListAdapter.notifyDataSetChanged();
                                rv_coin.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();
                            } else {
                                nested_scroll.setVisibility(View.GONE);
                                ly_dataNotFound.setVisibility(View.VISIBLE);
                                rv_coin.setVisibility(View.GONE);
                                ly_main.setVisibility(View.GONE);
                                ly_history.setVisibility(View.GONE);
//                            ly_paid_book.setVisibility(View.GONE);
                            }
                            progressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        ly_main.setVisibility(View.GONE);
                        ly_history.setVisibility(View.GONE);
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        ly_main.setVisibility(View.GONE);
                        ly_history.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                    ly_main.setVisibility(View.GONE);
                    ly_history.setVisibility(View.GONE);
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }

    }
}
