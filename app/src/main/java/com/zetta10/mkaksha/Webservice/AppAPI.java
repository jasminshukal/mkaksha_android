package com.zetta10.mkaksha.Webservice;


import com.zetta10.mkaksha.Model.BannerListModel.BannerListModel;
import com.zetta10.mkaksha.Model.ChapaterListModel.ChapaterListModel;
import com.zetta10.mkaksha.Model.CommentModel.CommentModel;
import com.zetta10.mkaksha.Model.MyJourneyModel.MyJourneyModel;
import com.zetta10.mkaksha.Model.MySubjectWiseModel.MySubjectWiseModel;
import com.zetta10.mkaksha.Model.PackagesListModel.PackagesListModel;
import com.zetta10.mkaksha.Model.PdfViewerModel.PdfViewerModel;
import com.zetta10.mkaksha.Model.PrivacyModel.PrivacyModel;
import com.zetta10.mkaksha.Model.PromoModel.PromoModel;
import com.zetta10.mkaksha.Model.SubjectListModel.SubjectListModel;
import com.zetta10.mkaksha.Model.SubscriptionListModel.SubscriptionListModel;
import com.zetta10.mkaksha.Model.TopicListModel.TopicListModel;
import com.zetta10.mkaksha.Model.VideoDesListModel.VideoDesListModel;
import com.zetta10.mkaksha.Model.VideoListModel.VideoListModel;
import com.zetta10.mkaksha.Model.WatchlistModel.WatchlistModel;
import com.zetta10.mkaksha.Model.pojoclass.Available_test_main_pojo;
import com.zetta10.mkaksha.Model.pojoclass.Main_attempted_test_list;
import com.zetta10.mkaksha.Model.pojoclass.Main_not_attempted_test_list;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AppAPI {


    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> logincall(@Field("password") String password,
                                 @Field("device_name") String device_name,
                                 @Field("longitude") String longitude,
                                 @Field("latitude") String latitude,
                                 @Field("email") String email,
                                 @Field("udid") String udid,
                                 @Field("version") String version,
                                 @Field("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("register")
    Call<ResponseBody> registercall(@Field("password") String password,
                                    @Field("device_name") String device_name,
                                    @Field("lname") String lname,
                                    @Field("fname") String fname,
                                    @Field("phno") String phno,
                                    @Field("email") String email,
                                    @Field("board") String board,
                                    @Field("medium") String medium,
                                    @Field("latitude") String latitude,
                                    @Field("longitude") String longitude,
                                    @Field("udid") String udid,
                                    @Field("std") String std,
                                    @Field("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("GetRegisterDetail/BORD")
    Call<ResponseBody> bord_call();

    @FormUrlEncoded
    @POST("subjectlist")
    Call<SubjectListModel> subject_list_call(@Field("login_token") String login_token,
                                             @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("chapterlist")
    Call<ChapaterListModel> chapter_list_call(@Field("login_token") String login_token,
                                              @Field("user_id") String user_id,
                                              @Field("subject_id") Integer subject_id);

    @FormUrlEncoded
    @POST("TopicList")
    Call<TopicListModel> topic_list_call(@Field("login_token") String login_token,
                                         @Field("user_id") String user_id,
                                         @Field("subject_id") int subject_id,
                                         @Field("chapter_id") int chapter_id);

    @FormUrlEncoded
    @POST("VideoList")
    Call<VideoListModel> video_list_call(@Field("login_token") String login_token,
                                         @Field("user_id") String user_id,
                                         @Field("topic_id") int topic_id);

    @FormUrlEncoded
    @POST("VideoDetail")
    Call<VideoDesListModel> video_desc_list_call(@Field("login_token") String login_token,
                                                 @Field("user_id") String user_id,
                                                 @Field("video_id") int video_id);

    @FormUrlEncoded
    @POST("PackagesList")
    Call<PackagesListModel> PackagesList_list_call(@Field("login_token") String login_token,
                                                   @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("PackagesDetail")
    Call<SubscriptionListModel> PackagesDetail_call(@Field("login_token") String login_token,
                                                    @Field("user_id") String user_id,
                                                    @Field("pakage_id") Integer pakage_id);

    @FormUrlEncoded
    @POST("AddTransaction")
    Call<ResponseBody> AddTransaction_call(@Field("login_token") String login_token,
                                           @Field("user_id") String user_id,
                                           @Field("package_id") Integer package_id,
                                           @Field("transaction_id") String transaction_id,
                                           @Field("price") String price,
                                           @Field("reference_number") String reference_number,
                                           @Field("coin_user") Integer coin_user,
                                           @Field("coin_use") Integer coin_use);

    @FormUrlEncoded
    @POST("LikeVideo")
    Call<ResponseBody> LikeModel_call(@Field("login_token") String login_token,
                                      @Field("user_id") String user_id,
                                      @Field("video_id") Integer video_id);

    @FormUrlEncoded
    @POST("VideoFev")
    Call<ResponseBody> VideoFev(@Field("login_token") String login_token,
                                @Field("user_id") String user_id,
                                @Field("video_id") Integer video_id);

    @FormUrlEncoded
    @POST("SubmitComment")
    Call<CommentModel> SubmitComment(@Field("login_token") String login_token,
                                     @Field("user_id") String user_id,
                                     @Field("topic_id") String topic_id,
                                     @Field("comment") String comment);

    @FormUrlEncoded
    @POST("GetPolicy")
    Call<PrivacyModel> GetPolicy(@Field("login_token") String login_token,
                                 @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetTerms")
    Call<PrivacyModel> GetTerms(@Field("login_token") String login_token,
                                @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetSlider")
    Call<BannerListModel> GetSlider(@Field("login_token") String login_token,
                                    @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Promo")
    Call<PromoModel> Promo(@Field("login_token") String login_token,
                           @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAttachments")
    Call<PdfViewerModel> GetAttachments(@Field("login_token") String login_token,
                                        @Field("user_id") String user_id,
                                        @Field("video_id") Integer video_id);

    @FormUrlEncoded
    @POST("Search/Chapter")
    Call<ChapaterListModel> Search_Chapter(@Field("login_token") String login_token,
                                           @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Search/Topic")
    Call<TopicListModel> Search_Topic(@Field("login_token") String login_token,
                                      @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Search/Video")
    Call<VideoListModel> Search_Video(@Field("login_token") String login_token,
                                      @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("quize")
    Call<ResponseBody> QuizModel(@Field("login_token") String login_token,
                                 @Field("user_id") String user_id,
                                 @Field("quiz_id") String quiz_id);

    @FormUrlEncoded
    @POST("SubmitQuiz")
    Call<ResponseBody> SubmitQuiz(@Field("login_token") String login_token,
                                  @Field("user_id") String user_id,
                                  @Field("quiz_id") String quiz_id,
                                  @Field("correct_ans") Integer correct_ans,
                                  @Field("wrong_ans") Integer wrong_ans);

    @FormUrlEncoded
    @POST("GenerateReferralCode")
    Call<ResponseBody> GenerateReferralCode(@Field("login_token") String login_token,
                                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("CheckReferralCode")
    Call<ResponseBody> CheckReferralCode(@Field("code") String code);

    @FormUrlEncoded
    @POST("GetAvailableTest")
    Call<Available_test_main_pojo> getavailable_test(@Field("login_token") String login_token,
                                                     @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAttemptTest")
    Call<Main_attempted_test_list> getattempted_test(@Field("login_token") String login_token,
                                                     @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetNotAttempt")
    Call<Main_not_attempted_test_list> getNotattempted_test(@Field("login_token") String login_token,
                                                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("SubmitTest")
    Call<ResponseBody> submit_test(@Field("login_token") String login_token,
                                   @Field("user_id") String user_id,
                                   @Field("test_id") String test_id,
                                   @Field("duration") String duration,
                                   @Field("correct_ans") int correct_ans,
                                   @Field("wrong_ans") int wrong_ans,
                                   @Field("attend_question") int attend_question);

    @FormUrlEncoded
    @POST("GetTestDetail")
    Call<ResponseBody> get_test_detail(@Field("login_token") String login_token,
                                       @Field("user_id") String user_id,
                                       @Field("test_id") String test_id);

    @FormUrlEncoded
    @POST("GetEachResult")
    Call<ResponseBody> get_each_sub_result(@Field("login_token") String login_token,
                                           @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("CoinList")
    Call<ResponseBody> CoinList(@Field("login_token") String login_token,
                                @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("SubmitWatch")
    Call<ResponseBody> SubmitWatch(@Field("login_token") String login_token,
                                   @Field("user_id") String user_id,
                                   @Field("video_id") Integer video_id);

    @FormUrlEncoded
    @POST("FevList")
    Call<WatchlistModel> FevList(@Field("login_token") String login_token,
                                 @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("MyJourney")
    Call<MyJourneyModel> MyJourney(@Field("login_token") String login_token,
                                   @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("MySubjectWise")
    Call<MySubjectWiseModel> MySubjectWise(@Field("login_token") String login_token,
                                           @Field("user_id") String user_id,
                                           @Field("sub_id") int sub_id);


}
