package com.zetta10.mkaksha.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session_for_search_history {

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String search_text;
    int search_count = -1;

    public Session_for_search_history(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
        editor = prefs.edit();
    }

    public String getSearch_text(int search_count) {
        search_count++;
        search_text = prefs.getString("key" + search_count, "");
        return search_text;
    }

    public void setSearch_text(String search_text) {
        search_count++;
        editor.putString("key" + search_count, search_text).apply();
        this.search_text = search_text;
    }

    public int getSearch_count() {
        search_count = prefs.getInt("searchkey", -1);
        return search_count;
    }

    public void setSearch_count() {
        editor.putInt("searchkey", search_count).apply();
    }

    public void remove_search_text(Context context, int key) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove("key" + key).apply();
    }

    public void session_destroy() {
        prefs.getAll().clear();
    }
}
