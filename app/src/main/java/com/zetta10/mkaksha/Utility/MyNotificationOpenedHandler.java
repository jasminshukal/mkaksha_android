package com.zetta10.mkaksha.Utility;

import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.zetta10.mkaksha.Activity.MainActivity;
import com.zetta10.mkaksha.Activity.Notification_display;
import com.zetta10.mkaksha.Activity.TestDetailActivity;
import com.zetta10.mkaksha.Activity.Wallet_Activity;

import org.json.JSONException;
import org.json.JSONObject;

class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    MyNotificationOpenedHandler myNotificationOpenedHandler;
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        JSONObject data = result.notification.payload.additionalData;
        Log.e("additionalData", String.valueOf(data));
        Intent intent1 = new Intent(MyApp.getInstance(), MainActivity.class);
        MyApp.getInstance().startActivity(intent1);
        try {
            if (data != null) {
                String payload_data = data.getString("click_action");
                Log.e("payload_data",payload_data);
                if (payload_data.equals("OPEN_ACTIVITY_2")) {

                    Intent intent = new Intent(MyApp.getInstance(), Wallet_Activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApp.getInstance().startActivity(intent);
                }
                if (payload_data.equals("OPEN_ACTIVITY_1")) {

                    Intent intent = new Intent(MyApp.getInstance(), TestDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApp.getInstance().startActivity(intent);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
