package com.zetta10.mkaksha.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    public String connected;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
        editor = prefs.edit();
    }

    public void setconnected(String connected) {
        editor.putString("networkstate", connected).apply();
    }

    public String getconnected() {

        connected = prefs.getString("networkstate","");
        return connected;

    }
    public void session_destroy(){
        editor.clear();
    }
}
