package com.zetta10.mkaksha.Utility;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.rezwan.knetworklib.KNetwork;

public class MyApp extends Application implements OneSignal.NotificationOpenedHandler {

    private static final String TAG = MyApp.class.getSimpleName();
    private static MyApp singleton = null;
   // PrefManager prefManager;

  /*  @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }*/

    public static MyApp getPhotoApp() {
        return singleton;
    }

    public static MyApp getInstance() {
        if (singleton == null) {
            singleton = new MyApp();
        }
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        singleton = this;
       // prefManager = new PrefManager(this);


       // Log.e("ID==>", "" + prefManager.getValue("publisher_id"));
//        MobileAds.initialize(this, prefManager.getValue("publisher_id"));
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler() )
                .init();

        FirebaseApp.initializeApp(this);

        KNetwork.INSTANCE.initialize(this);
    }

    public void initAppLanguage(Context context) {

        LocaleUtils.initialize(context, LocaleUtils.getSelectedLanguageId());

    }

    public Context getContext() {
        return singleton.getContext();
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

    }
}