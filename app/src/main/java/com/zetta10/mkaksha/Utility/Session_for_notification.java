package com.zetta10.mkaksha.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session_for_notification {

    public String connected;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String title,desc;
    int title_key = -1,desc_key = -1;
    public Session_for_notification(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
        editor = prefs.edit();
    }

    public String getTitle(int title_key) {
       // title_key = 0;
        title_key++;
        title = prefs.getString("title"+title_key,"");
        return title;
    }

    public void setTitle(String title) {
        title_key++;
        editor.putString("title"+title_key, title).apply();
    }

    public String getDesc(int desc_key) {
      //  desc_key = 0;
        desc_key++;
        desc = prefs.getString("desc"+desc_key ,"");
        return desc;
    }

    public void setDesc(String desc) {
        desc_key++;
        editor.putString("desc"+desc_key,desc).apply();
    }

    public int getTitle_key() {
        title_key = prefs.getInt("title_key",-1);
        return title_key;
    }

    public void setTitle_key() {
        editor.putInt("title_key",title_key).apply();
    }

    public int getDesc_key() {
        desc_key = prefs.getInt("desc_key",-1);
        return desc_key;
    }

    public void setDesc_key() {
        editor.putInt("desc_key",desc_key).apply();
    }

    public void setconnected(String connected) {

        editor.putString("networkstate", connected).apply();
    }

    public String getconnected() {

        connected = prefs.getString("networkstate","");
        return connected;

    }
    public void session_destroy(){
        editor.clear();
    }
    public void remove_title(Context context,int title_key){
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove("title"+title_key).apply();
    }
    public void remove_desc(Context context,int desc_key){
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove("desc"+desc_key).apply();
    }
}
