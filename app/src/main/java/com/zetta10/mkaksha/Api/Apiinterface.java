package com.zetta10.mkaksha.Api;

import com.zetta10.mkaksha.Model.RewardModel.Main_Reward_model;
import com.zetta10.mkaksha.Model.pojoclass.Available_test_main_pojo;
import com.zetta10.mkaksha.Model.pojoclass.Main_attempted_test_list;
import com.zetta10.mkaksha.Model.pojoclass.Main_not_attempted_test_list;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Apiinterface {

    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> logincall(@Field("password") String password,
                                 @Field("device_name") String device_name,
                                 @Field("longitude") String longitude,
                                 @Field("latitude") String latitude,
                                 @Field("email") String email,
                                 @Field("udid") String udid,
                                 @Field("version") String version,
                                 @Field("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("register")
    Call<ResponseBody> registercall(@Field("password") String password,
                                    @Field("device_name") String device_name,
                                    @Field("lname") String lname,
                                    @Field("fname") String fname,
                                    @Field("phno") String phno,
                                    @Field("email") String email,
                                    @Field("board") String board,
                                    @Field("medium") String medium,
                                    @Field("latitude") String latitude,
                                    @Field("longitude") String longitude,
                                    @Field("udid") String udid,
                                    @Field("std") String std,
                                    @Field("fcm_token") String fcm_token,
                                    @Field("referral_code") String referral_code);


    @POST("GetRegisterDetail/BORD")
    Call<ResponseBody> getboard();


    @POST("GetRegisterDetail/STD")
    Call<ResponseBody> getstandard();


    @POST("GetRegisterDetail/MEDIUM")
    Call<ResponseBody> getmedium();

    @FormUrlEncoded
    @POST("phoneexist")
    Call<ResponseBody> getphonenumber(@Field("phno") String phno);

    @FormUrlEncoded
    @POST("ChangePassword")
    Call<ResponseBody> changepassword(@Field("login_token") String login_token,
                                      @Field("user_id") String user_id,
                                      @Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("GetProfile")
    Call<ResponseBody> student_basic_info(@Field("user_id") String user_id,
                                          @Field("login_token") String login_token);

    @FormUrlEncoded
    @POST("ProfileUpdate")
    Call<ResponseBody> student_personal_info(@Field("login_token") String login_token,
                                             @Field("user_id") String user_id,
                                             @Field("dob") String dob,
                                             @Field("gender") String gender,
                                             @Field("nationality") String nationality,
                                             @Field("blood_group") String blood_group,
                                             @Field("about") String about);

    @FormUrlEncoded
    @POST("ProfileUpdate")
    Call<ResponseBody> student_guardian_info(@Field("login_token") String login_token,
                                             @Field("user_id") String user_id,
                                             @Field("guardian_name") String guardian_name,
                                             @Field("guardian_number") String guardian_number);

    @FormUrlEncoded
    @POST("ProfileUpdate")
    Call<ResponseBody> student_edu_info(@Field("login_token") String login_token,
                                        @Field("user_id") String user_id,
                                        @Field("current_std") String cur_standard,
                                        @Field("name_institute") String name_institute);

    @FormUrlEncoded
    @POST("ProfileUpdate")
    Call<ResponseBody> student_address_info(@Field("login_token") String login_token,
                                            @Field("user_id") String user_id,
                                            @Field("city") String city,
                                            @Field("permanent_address") String permanent_address,
                                            @Field("correspondence_address") String correspondence_address);

    @Multipart
    @POST("ProfileUpdate")
    Call<ResponseBody> student_prof_info(@Part("login_token") RequestBody login_token,
                                         @Part("user_id") RequestBody id,
                                         @Part MultipartBody.Part image2);

    @FormUrlEncoded
    @POST("GetAvailableTest")
    Call<Available_test_main_pojo> getavailable_test(@Field("login_token") String login_token,
                                                     @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAttemptTest")
    Call<Main_attempted_test_list> getattempted_test(@Field("login_token") String login_token,
                                                     @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetNotAttempt")
    Call<Main_not_attempted_test_list> getNotattempted_test(@Field("login_token") String login_token,
                                                            @Field("user_id") String user_id);

   /* @FormUrlEncoded
    @POST("SubmitTest")
    Call<ResponseBody> submit_test(@Field("login_token") String login_token,
                                   @Field("user_id") String user_id,
                                   @Field("test_id") String test_id,
                                   @Field("duration") String duration,
                                   @Field("correct_ans") int correct_ans,
                                   @Field("wrong_ans") int wrong_ans,
                                   @Field("attend_question") int attend_question);*/

    @FormUrlEncoded
    @POST("GetTestDetail")
    Call<ResponseBody> get_test_detail(@Field("login_token") String login_token,
                                       @Field("user_id") String user_id,
                                       @Field("test_id") String test_id);

    @FormUrlEncoded
    @POST("GetEachResult")
    Call<ResponseBody> get_each_sub_result(@Field("login_token") String login_token,
                                           @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("CheckPayment")
    Call<ResponseBody> checkpayment(@Field("login_token") String login_token,
                                    @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("SubmitTest")
    Call<ResponseBody> submit_test(@Field("login_token") String login_token,
                                   @Field("user_id") String user_id,
                                   @Field("test_id") String test_id,
                                   @Field("duration") String duration,
                                   @Field("correct_ans") int correct_ans,
                                   @Field("wrong_ans") int wrong_ans,
                                   @Field("attend_question") int attend_question);

    @FormUrlEncoded
    @POST("AllResult")
    Call<ResponseBody> all_subject_result(@Field("login_token") String login_token,
                                          @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("Reward")
    Call<Main_Reward_model> get_reward_detail(@Field("login_token") String login_token,
                                              @Field("user_id") String user_id);

}
