
package com.zetta10.mkaksha.Model.BannerListModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    /*  @SerializedName("id")
      @Expose
      private Integer id;

      @SerializedName("name")
      @Expose
      private String name;

      @SerializedName("value")
      @Expose
      private String value;

      @SerializedName("intent")
      @Expose
      private String intent;

      @SerializedName("action")
      @Expose
      private String action;

      @SerializedName("url")
      @Expose
      private String url;

      @SerializedName("video_id")
      @Expose
      private int video_id;

      @SerializedName("video_name")
      @Expose
      private String video_name;

      @SerializedName("thumbnail")
      @Expose
      private String thumbnail;


      public Integer getId() {
          return id;
      }

      public void setId(Integer id) {
          this.id = id;
      }

      public String getName() {
          return name;
      }

      public void setName(String name) {
          this.name = name;
      }

      public String getValue() {
          return value;
      }

      public void setValue(String value) {
          this.value = value;
      }

      public String getIntent() {
          return intent;
      }

      public void setIntent(String intent) {
          this.intent = intent;
      }

      public String getAction() {
          return action;
      }

      public void setAction(String action) {
          this.action = action;
      }

      public String getUrl() {
          return url;
      }

      public void setUrl(String url) {
          this.url = url;
      }

      public int getVideo_id() {
          return video_id;
      }

      public void setVideo_id(int video_id) {
          this.video_id = video_id;
      }

      public String getVideo_name() {
          return video_name;
      }

      public void setVideo_name(String video_name) {
          this.video_name = video_name;
      }

      public String getThumbnail() {
          return thumbnail;
      }

      public void setThumbnail(String thumbnail) {
          this.thumbnail = thumbnail;
      }*/
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("intent")
    @Expose
    private String intent;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("video_id")
    @Expose
    private String videoId;
    @SerializedName("video_name")
    @Expose
    private String videoName;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

}
