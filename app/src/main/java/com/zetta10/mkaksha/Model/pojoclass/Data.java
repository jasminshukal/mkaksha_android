package com.zetta10.mkaksha.Model.pojoclass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("student_detail")
    @Expose
    private Student_detail studentDetail;

    public Student_detail getStudentDetail() {
        return studentDetail;
    }

    public void setStudentDetail(Student_detail studentDetail) {
        this.studentDetail = studentDetail;
    }
}
