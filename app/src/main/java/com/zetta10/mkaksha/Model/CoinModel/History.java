
package com.zetta10.mkaksha.Model.CoinModel;

public class History {

   /* @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("massage")
    @Expose
    private String massage;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("student_id")
    @Expose
    private String studentId;
     @SerializedName("date")
    @Expose
    private String date;
    */

    public History(String id, String operation, String massage, String value, String student_id, String date) {
        this.id = id;
        this.operation = operation;
        this.massage = massage;
        this.value = value;
        this.studentId = student_id;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String id, operation, massage, value, studentId, date;

}
