
package com.zetta10.mkaksha.Model.MyJourneyModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("subject")
    @Expose
    private List<Subject> subject = null;
    @SerializedName("total_video")
    @Expose
    private Integer totalVideo;
    @SerializedName("total_you_watch_video")
    @Expose
    private Integer totalYouWatchVideo;
    @SerializedName("your_laval")
    @Expose
    private float yourLaval;

    public List<Subject> getSubject() {
        return subject;
    }

    public void setSubject(List<Subject> subject) {
        this.subject = subject;
    }

    public Integer getTotalVideo() {
        return totalVideo;
    }

    public void setTotalVideo(Integer totalVideo) {
        this.totalVideo = totalVideo;
    }

    public Integer getTotalYouWatchVideo() {
        return totalYouWatchVideo;
    }

    public void setTotalYouWatchVideo(Integer totalYouWatchVideo) {
        this.totalYouWatchVideo = totalYouWatchVideo;
    }

    public float getYourLaval() {
        return yourLaval;
    }

    public void setYourLaval(float yourLaval) {
        this.yourLaval = yourLaval;
    }

}
