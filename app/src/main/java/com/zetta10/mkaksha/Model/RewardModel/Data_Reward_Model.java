package com.zetta10.mkaksha.Model.RewardModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data_Reward_Model {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("gift_description")
    @Expose
    private String giftDescription;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("added_date")
    @Expose
    private String added_date;

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGiftDescription() {
        return giftDescription;
    }

    public void setGiftDescription(String giftDescription) {
        this.giftDescription = giftDescription;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
