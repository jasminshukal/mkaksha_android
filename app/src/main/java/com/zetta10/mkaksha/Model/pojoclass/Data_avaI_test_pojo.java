package com.zetta10.mkaksha.Model.pojoclass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data_avaI_test_pojo {

    @SerializedName("available_test")
    @Expose
    public List<Available_test_list_pojo> availableTest = null;

    public List<Available_test_list_pojo> getAvailableTest() {
        return availableTest;
    }

    public void setAvailableTest(List<Available_test_list_pojo> availableTest) {
        this.availableTest = availableTest;
    }

}
