
package com.zetta10.mkaksha.Model.VideoDesListModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("topic_id")
    @Expose
    private String topicId;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("video_name")
    @Expose
    private String videoName;
    @SerializedName("video_path")
    @Expose
    private String videoPath;
    @SerializedName("video_description")
    @Expose
    private String videoDescription;
    @SerializedName("is_attachment")
    @Expose
    private String isAttachment;
    @SerializedName("quiz_id")
    @Expose
    private String quizId;

    public String getSelf_fev() {
        return self_fev;
    }

    public void setSelf_fev(String self_fev) {
        this.self_fev = self_fev;
    }

    @SerializedName("self_Like")
    @Expose
    private String self_Like;
    @SerializedName("self_fev")
    @Expose
    private String self_fev;

    public String getSelf_Like() {
        return self_Like;
    }

    public void setSelf_Like(String self_Like) {
        this.self_Like = self_Like;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getIsAttachment() {
        return isAttachment;
    }

    public void setIsAttachment(String isAttachment) {
        this.isAttachment = isAttachment;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

}
