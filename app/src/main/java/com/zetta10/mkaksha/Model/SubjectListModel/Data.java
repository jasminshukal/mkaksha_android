
package com.zetta10.mkaksha.Model.SubjectListModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("std_id")
    @Expose
    private String stdId;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("subject_name_en")
    @Expose
    private String subjectNameEn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("subject_img")
    @Expose
    private String subjectImg;
    @SerializedName("subject_img_en")
    @Expose
    private String subjectImgEn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectNameEn() {
        return subjectNameEn;
    }

    public void setSubjectNameEn(String subjectNameEn) {
        this.subjectNameEn = subjectNameEn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubjectImg() {
        return subjectImg;
    }

    public void setSubjectImg(String subjectImg) {
        this.subjectImg = subjectImg;
    }

    public String getSubjectImgEn() {
        return subjectImgEn;
    }

    public void setSubjectImgEn(String subjectImgEn) {
        this.subjectImgEn = subjectImgEn;
    }

}
