package com.zetta10.mkaksha.Model.pojoclass;

public class Model_for_all_subject {

    String subject_name,total_mark,obtain_mark;

    public Model_for_all_subject(String subject_name, String total_mark, String obtain_mark) {
        this.subject_name = subject_name;
        this.total_mark = total_mark;
        this.obtain_mark = obtain_mark;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getTotal_mark() {
        return total_mark;
    }

    public void setTotal_mark(String total_mark) {
        this.total_mark = total_mark;
    }

    public String getObtain_mark() {
        return obtain_mark;
    }

    public void setObtain_mark(String obtain_mark) {
        this.obtain_mark = obtain_mark;
    }
}
