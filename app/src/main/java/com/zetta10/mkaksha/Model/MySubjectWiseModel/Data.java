
package com.zetta10.mkaksha.Model.MySubjectWiseModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("Chapters")
    @Expose
    private List<Chapter> chapters = null;
    @SerializedName("total_video")
    @Expose
    private Integer totalVideo;
    @SerializedName("total_you_watch_video")
    @Expose
    private Integer totalYouWatchVideo;
    @SerializedName("your_laval")
    @Expose
    private float yourLaval;

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Integer getTotalVideo() {
        return totalVideo;
    }

    public void setTotalVideo(Integer totalVideo) {
        this.totalVideo = totalVideo;
    }

    public Integer getTotalYouWatchVideo() {
        return totalYouWatchVideo;
    }

    public void setTotalYouWatchVideo(Integer totalYouWatchVideo) {
        this.totalYouWatchVideo = totalYouWatchVideo;
    }

    public float getYourLaval() {
        return yourLaval;
    }

    public void setYourLaval(float yourLaval) {
        this.yourLaval = yourLaval;
    }

}
