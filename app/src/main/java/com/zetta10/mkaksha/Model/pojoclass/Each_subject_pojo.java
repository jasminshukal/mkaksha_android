package com.zetta10.mkaksha.Model.pojoclass;

public class Each_subject_pojo {

    String sub_name,correct,wrong,percentage;

    public Each_subject_pojo(String sub_name, String correct, String wrong, String percentage) {
        this.sub_name = sub_name;
        this.correct = correct;
        this.wrong = wrong;
        this.percentage = percentage;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public String getWrong() {
        return wrong;
    }

    public void setWrong(String wrong) {
        this.wrong = wrong;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }
}
