package com.zetta10.mkaksha.Model.pojoclass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("current_std")
    @Expose
    private String currentStd;
    @SerializedName("board")
    @Expose
    private String board;
    @SerializedName("medium")
    @Expose
    private String medium;
    @SerializedName("profile_img")
    @Expose
    private String profileImg;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("blood_group")
    @Expose
    private String bloodGroup;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("permanent_address")
    @Expose
    private String permanentAddress;
    @SerializedName("p_pincode")
    @Expose
    private String pPincode;
    @SerializedName("correspondence_address")
    @Expose
    private String correspondenceAddress;
    @SerializedName("c_pincode")
    @Expose
    private String cPincode;
    @SerializedName("name_institute")
    @Expose
    private String nameInstitute;
    @SerializedName("guardian_name")
    @Expose
    private String guardianName;
    @SerializedName("guardian_number")
    @Expose
    private String guardianNumber;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCurrentStd() {
        return currentStd;
    }

    public void setCurrentStd(String currentStd) {
        this.currentStd = currentStd;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPPincode() {
        return pPincode;
    }

    public void setPPincode(String pPincode) {
        this.pPincode = pPincode;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getCPincode() {
        return cPincode;
    }

    public void setCPincode(String cPincode) {
        this.cPincode = cPincode;
    }

    public String getNameInstitute() {
        return nameInstitute;
    }

    public void setNameInstitute(String nameInstitute) {
        this.nameInstitute = nameInstitute;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getGuardianNumber() {
        return guardianNumber;
    }

    public void setGuardianNumber(String guardianNumber) {
        this.guardianNumber = guardianNumber;
    }

}
