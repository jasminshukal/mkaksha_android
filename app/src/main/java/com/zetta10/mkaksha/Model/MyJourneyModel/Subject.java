
package com.zetta10.mkaksha.Model.MyJourneyModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subject {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("percentage")
    @Expose
    private float percentage;
    @SerializedName("total_watch")
    @Expose
    private Integer totalWatch;
    @SerializedName("total_video")
    @Expose
    private Integer totalVideo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public Integer getTotalWatch() {
        return totalWatch;
    }

    public void setTotalWatch(Integer totalWatch) {
        this.totalWatch = totalWatch;
    }

    public Integer getTotalVideo() {
        return totalVideo;
    }

    public void setTotalVideo(Integer totalVideo) {
        this.totalVideo = totalVideo;
    }

}
