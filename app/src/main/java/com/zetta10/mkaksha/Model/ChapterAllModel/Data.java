
package com.zetta10.mkaksha.Model.ChapterAllModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subject_id")
    @Expose
    private String subjectId;
    @SerializedName("chapter_name")
    @Expose
    private String chapterName;
    @SerializedName("chapter_name_en")
    @Expose
    private String chapterNameEn;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterNameEn() {
        return chapterNameEn;
    }

    public void setChapterNameEn(String chapterNameEn) {
        this.chapterNameEn = chapterNameEn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
