
package com.zetta10.mkaksha.Model.MySubjectWiseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chapter {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("total_video")
    @Expose
    private Integer totalVideo;
    @SerializedName("total_watch")
    @Expose
    private Integer totalWatch;
    @SerializedName("persontage")
    @Expose
    private float persontage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalVideo() {
        return totalVideo;
    }

    public void setTotalVideo(Integer totalVideo) {
        this.totalVideo = totalVideo;
    }

    public Integer getTotalWatch() {
        return totalWatch;
    }

    public void setTotalWatch(Integer totalWatch) {
        this.totalWatch = totalWatch;
    }

    public float getPersontage() {
        return persontage;
    }

    public void setPersontage(float persontage) {
        this.persontage = persontage;
    }

}
