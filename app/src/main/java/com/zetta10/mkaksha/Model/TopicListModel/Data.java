
package com.zetta10.mkaksha.Model.TopicListModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("std_id")
    @Expose
    private String stdId;
    @SerializedName("chapter_id")
    @Expose
    private String chapterId;
    @SerializedName("topic_name")
    @Expose
    private String topicName;
    @SerializedName("topic_name_en")
    @Expose
    private String topicNameEn;
    @SerializedName("read_path")
    @Expose
    private String readPath;
    @SerializedName("video_path")
    @Expose
    private String videoPath;
    @SerializedName("example_path")
    @Expose
    private String examplePath;
    @SerializedName("quiz_id")
    @Expose
    private Object quizId;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("practice_path")
    @Expose
    private String practicePath;
    @SerializedName("sr_id")
    @Expose
    private Integer srId;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("is_lock")
    @Expose
    private String isLock;
    @SerializedName("self_like")
    @Expose
    private String selfLike;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicNameEn() {
        return topicNameEn;
    }

    public void setTopicNameEn(String topicNameEn) {
        this.topicNameEn = topicNameEn;
    }

    public String getReadPath() {
        return readPath;
    }

    public void setReadPath(String readPath) {
        this.readPath = readPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getExamplePath() {
        return examplePath;
    }

    public void setExamplePath(String examplePath) {
        this.examplePath = examplePath;
    }

    public Object getQuizId() {
        return quizId;
    }

    public void setQuizId(Object quizId) {
        this.quizId = quizId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPracticePath() {
        return practicePath;
    }

    public void setPracticePath(String practicePath) {
        this.practicePath = practicePath;
    }

    public Integer getSrId() {
        return srId;
    }

    public void setSrId(Integer srId) {
        this.srId = srId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIsLock() {
        return isLock;
    }

    public void setIsLock(String isLock) {
        this.isLock = isLock;
    }

    public String getSelfLike() {
        return selfLike;
    }

    public void setSelfLike(String selfLike) {
        this.selfLike = selfLike;
    }

}
