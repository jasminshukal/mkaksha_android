package com.zetta10.mkaksha.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.pojoclass.Model_for_all_subject;
import com.zetta10.mkaksha.R;

import java.util.ArrayList;

public class Adapter_all_subject_result extends RecyclerView.Adapter<Adapter_all_subject_result.Holder> {

    ArrayList<Model_for_all_subject> allSubjectArrayList;

    public Adapter_all_subject_result(ArrayList<Model_for_all_subject> allSubjectArrayList) {
        this.allSubjectArrayList = allSubjectArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_allsubject_result,parent,false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.text_for_subname.setText(allSubjectArrayList.get(position).getSubject_name());
        holder.text_for_total_mark.setText(allSubjectArrayList.get(position).getTotal_mark());
        holder.text_for_obtain_mark.setText(allSubjectArrayList.get(position).getObtain_mark());
    }

    @Override
    public int getItemCount() {
        if (allSubjectArrayList != null){
            return allSubjectArrayList.size();
        }else {
            return 0;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView text_for_subname,text_for_total_mark,text_for_obtain_mark;
        public Holder(@NonNull View itemView) {
            super(itemView);
            text_for_subname = itemView.findViewById(R.id.text_for_subname);
            text_for_total_mark = itemView.findViewById(R.id.text_for_total_mark);
            text_for_obtain_mark = itemView.findViewById(R.id.text_for_obtain_mark);

        }
    }
}
