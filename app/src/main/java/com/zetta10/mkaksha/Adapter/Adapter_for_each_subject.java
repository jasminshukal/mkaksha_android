package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.pojoclass.Each_subject_pojo;
import com.zetta10.mkaksha.R;

import java.util.ArrayList;

public class Adapter_for_each_subject extends RecyclerView.Adapter<Adapter_for_each_subject.Holder> {

    Context context;
    ArrayList<Each_subject_pojo> eachSubjectPojos;

    public Adapter_for_each_subject(Context context, ArrayList<Each_subject_pojo> eachSubjectPojos) {
        this.context = context;
        this.eachSubjectPojos = eachSubjectPojos;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_each_subject, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.textview_for_test_sub_name.setText(eachSubjectPojos.get(position).getSub_name());
        holder.textview_total_marks.setText(eachSubjectPojos.get(position).getWrong());
        holder.textview_for_obtained_marks.setText(eachSubjectPojos.get(position).getCorrect());
        holder.textview_for_test_percentage.setText(eachSubjectPojos.get(position).getPercentage());
    }

    @Override
    public int getItemCount() {
        if (eachSubjectPojos != null){
            return eachSubjectPojos.size();
        }else {
            return 0;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView textview_for_test_sub_name, textview_total_marks, textview_for_obtained_marks, textview_for_test_percentage;

        public Holder(@NonNull View itemView) {
            super(itemView);

            textview_for_obtained_marks = itemView.findViewById(R.id.textview_for_obtained_marks);
            textview_for_test_sub_name = itemView.findViewById(R.id.textview_for_test_sub_name);
            textview_total_marks = itemView.findViewById(R.id.textview_total_marks);
            textview_for_test_percentage = itemView.findViewById(R.id.textview_for_test_percentage);
        }
    }
}
