package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.NotificationModel.Pojo_for_notification;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.Session_for_notification;

import java.util.ArrayList;

public class Adapter_for_notification extends RecyclerView.Adapter<Adapter_for_notification.Holder> {
    Context context;
    ArrayList<Pojo_for_notification> notificationArrayList;
    Session_for_notification session;

    public Adapter_for_notification(Context context, ArrayList<Pojo_for_notification> notificationArrayList) {
        this.context = context;
        this.notificationArrayList = notificationArrayList;
        session = new Session_for_notification(context);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_notification, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }
    @Override
    public long getItemId(int position) {
        //Object listItem = chapterList.get(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {

        holder.textview_for_not_title.setText(notificationArrayList.get(position).getTitle());
        holder.textview_for_not_desc.setText(notificationArrayList.get(position).getDescription());
        String title = notificationArrayList.get(position).getTitle();

        if (title.equals("")) {
            holder.linear_for_notification.setVisibility(View.GONE);
        }
        holder.image_for_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.remove_title(context, position);
                session.remove_desc(context, position);
                holder.linear_for_notification.setVisibility(View.GONE);
                // holder.linear_for_notification.removeViewAt(position);
                // remove(notificationArrayList.get(position));
                /*notificationArrayList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, notificationArrayList.size());*/
                // notifyDataSetChanged();
            }
        });
    }

    /*public void remove(Pojo_for_notification item) {
        int position = notificationArrayList.indexOf(item);
        notificationArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, notificationArrayList.size());
    }*/

    @Override
    public int getItemCount() {
        if (notificationArrayList != null) {
            return notificationArrayList.size();
        } else {
            return 0;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView textview_for_not_title, textview_for_not_desc;
        ImageView image_for_notification, image_for_close;
        LinearLayout linear_for_notification;

        public Holder(@NonNull View itemView) {
            super(itemView);
            textview_for_not_title = itemView.findViewById(R.id.textview_for_not_title);
            textview_for_not_desc = itemView.findViewById(R.id.textview_for_not_desc);
            image_for_close = itemView.findViewById(R.id.image_for_close);
            linear_for_notification = itemView.findViewById(R.id.linear_for_notification);
        }
    }
}
