package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.Name;
import com.zetta10.mkaksha.R;

import java.util.List;

public class Rvadapter extends RecyclerView.Adapter<Rvadapter.NameViewHolder> {

    List<Name> names;
    String activity_name, HelpActivity;
    Context context;

    public Rvadapter(Context context, List<Name> names) {
        this.context = context;
        this.names = names;
    }

    public Rvadapter(List<Name> names) {
        this.names = names;
    }

    @NonNull
    @Override
    public Rvadapter.NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        NameViewHolder nvh = new NameViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(@NonNull Rvadapter.NameViewHolder holder, int position) {
        holder.textView.setText(names.get(position).getTextView());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("e===>", "" + names.get(position).getTextView());

                String text = names.get(position).getTextView();
                if (text.equals("Manage Profile")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.Manage_Profile.class);
                    context.startActivity(intent);
                }
                if (text.equals("Subscription")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.SubscriptionActivity.class);
                    context.startActivity(intent);
                }
                if (text.equals("MyJourney")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.MyJourneyActivity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Wallet")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.Wallet_Activity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Test And Result")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.Available_Test_Result_Activity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Wishlist")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.WatchListActivity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Notification")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.Notification_display.class);
                    context.startActivity(intent);
                }
                if (text.equals("Invite Friend")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.ReferedActivity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Privacy Policy")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.PrivacyPolicyActivity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Terms AndCondition")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.TermsConditionActivity.class);
                    context.startActivity(intent);
                }
                if (text.equals("Help")) {
                    Intent intent = new Intent(context, com.zetta10.mkaksha.Activity.HelpActivity.class);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public class NameViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView textView;

        public NameViewHolder(@NonNull View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cardview);
            textView = (TextView) itemView.findViewById(R.id.textView);
        }
    }
}
