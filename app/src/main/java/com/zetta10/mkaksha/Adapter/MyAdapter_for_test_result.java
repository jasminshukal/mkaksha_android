package com.zetta10.mkaksha.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.zetta10.mkaksha.Fragment.AvailableTestFragment;
import com.zetta10.mkaksha.Fragment.ResultFragment;

public class MyAdapter_for_test_result extends FragmentPagerAdapter {

    Context context;
    int totaltabs;

    public MyAdapter_for_test_result( Context context,@NonNull FragmentManager fm, int totaltabs) {
        super(fm);
        this.context = context;
        this.totaltabs = totaltabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AvailableTestFragment testFragment = new AvailableTestFragment();
                return testFragment;
            case 1:
                ResultFragment resultFragment = new ResultFragment();
                return resultFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totaltabs;
    }
}
