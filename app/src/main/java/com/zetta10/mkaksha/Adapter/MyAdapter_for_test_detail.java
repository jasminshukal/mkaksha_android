package com.zetta10.mkaksha.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.zetta10.mkaksha.Fragment.Allsubject;
import com.zetta10.mkaksha.Fragment.Eachsubject;

public class MyAdapter_for_test_detail extends FragmentPagerAdapter {

    Context context;
    int totaltabs;

    public MyAdapter_for_test_detail(Context context, @NonNull FragmentManager fm, int totaltabs) {
        super(fm);
        this.context = context;
        this.totaltabs = totaltabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Eachsubject eachsubject = new Eachsubject();
                return eachsubject;
            case 1:
                Allsubject allsubject = new Allsubject();
                return allsubject;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totaltabs;
    }
}
