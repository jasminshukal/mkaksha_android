/*
package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.PromoModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<com.zetta10.mkaksha.Model.PromoModel.Data> promo_code_list;

    public PromoCodeAdapter(Context context, List<Data> promo_code_list) {
        this.promo_code_list = promo_code_list;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promocode_layout_design, parent, false);

        return new PromoCodeAdapter.MyviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
       */
/* holder.txt_topicList.setText(chapterList.get(position).getTopicName());
//        holder.txt_topic_id.setText(chapterList.get(position).getSrId());
        holder.txt_topic_id.setText(Integer.toString(chapterList.get(position).getSrId()));

        holder.ly_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Intent intent = new Intent(mcontext, VideoListActivity.class);
                intent.putExtra("v_topic_id", chapterList.get(position).getId());
                intent.putExtra("v_topic_name", chapterList.get(position).getTopicName());
                Log.e("v_topic_id==>", "" + chapterList.get(position).getId());
                mcontext.startActivity(intent);

            }
        });*//*

//        holder.txt_promo_value.setText();
        holder.txt_promo_details.setText(promo_code_list.get(position).getDetail());
        holder.txt_promo_value.setText(promo_code_list.get(position).getCode());

    }

    @Override
    public int getItemCount() {
        return promo_code_list.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView txt_promo_value, txt_promo_details;
        RelativeLayout ly_apply;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            txt_promo_details = itemView.findViewById(R.id.txt_promo_details);
            ly_apply = itemView.findViewById(R.id.ly_apply);
            txt_promo_value = itemView.findViewById(R.id.txt_promo_value);
        }
    }
}
*/
