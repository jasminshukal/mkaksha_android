package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.SubscriptionActivity;
import com.zetta10.mkaksha.Activity.TopicActivity;
import com.zetta10.mkaksha.Model.ChapaterListModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class ChapterListAdapter extends RecyclerView.Adapter<ChapterListAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<Data> chapterList;
    String payment_array_size;
    int check_lock = 0;

    public ChapterListAdapter(Context context, List<com.zetta10.mkaksha.Model.ChapaterListModel.Data> chapter_dataList, String payment_array_size) {
        this.chapterList = chapter_dataList;
        this.mcontext = context;
        this.payment_array_size = payment_array_size;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chapter_list_design, parent, false);

        return new ChapterListAdapter.MyviewHolder(itemView);
    }

    @Override
    public long getItemId(int position) {
        //Object listItem = chapterList.get(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
      /*  holder.txt_chapterList.setText(chapterList.get(position).getChapterName());
        holder.ly_chapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Log.e("click", "call");
                Intent intent = new Intent(mcontext, TopicActivity.class);
                intent.putExtra("topic_id", chapterList.get(position).getId());
                intent.putExtra("topic_s_id", chapterList.get(position).getSubjectId());
                intent.putExtra("topic", chapterList.get(position).getChapterName());
                Log.e("c_id==>", "" + chapterList.get(position).getId());
                mcontext.startActivity(intent);
            }
        });*/
        if (payment_array_size.equals("0")) {
            if (position == 0) {
                holder.txt_chapterList.setVisibility(View.VISIBLE);
                holder.txt_chapterList.setText(chapterList.get(0).getChapterName());
            } else if (position > 0) {
                check_lock = 1;
                holder.txt_chapterList.setText(chapterList.get(position).getChapterName());
// holder.txt_chapterList.setEnabled(false);
                holder.txt_chapterList.setClickable(false);
                Drawable img = mcontext.getResources().getDrawable(R.drawable.password);
                img.setBounds(0, 0, 50, 50);
                holder.txt_chapterList.setCompoundDrawables(img, null, null, null);
                holder.txt_chapterList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mcontext, SubscriptionActivity.class);
                        mcontext.startActivity(intent);
                    }
                });
//holder.txt_chapterList.setledrawable
// holder.txt_chapterList.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.txt_chapterList.setText(chapterList.get(position).getChapterName());
            holder.txt_chapterList.setVisibility(View.VISIBLE);
        }

        holder.txt_chapterList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_lock == 1) {
                    if (position == 0) {
                        Log.e("click", "click me");
                        Log.e("click", "call");
                        Intent intent = new Intent(mcontext, TopicActivity.class);
                        intent.putExtra("topic_id", chapterList.get(position).getId());
                        intent.putExtra("topic_s_id", chapterList.get(position).getSubjectId());
                        intent.putExtra("topic", chapterList.get(position).getChapterName());
                        Log.e("c_id==>", "" + chapterList.get(position).getId());
                        mcontext.startActivity(intent);
                    } else if (position > 0) {
                        Intent intent = new Intent(mcontext, SubscriptionActivity.class);
                        mcontext.startActivity(intent);
                    }
                } else if (check_lock == 0) {

                    Log.e("click", "click me");
                    Log.e("click", "call");
                    Intent intent = new Intent(mcontext, TopicActivity.class);
                    intent.putExtra("topic_id", chapterList.get(position).getId());
                    intent.putExtra("topic_s_id", chapterList.get(position).getSubjectId());
                    intent.putExtra("topic", chapterList.get(position).getChapterName());
                    Log.e("c_id==>", "" + chapterList.get(position).getId());
                    mcontext.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return chapterList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView txt_chapterList;
        LinearLayout ly_chapter;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            txt_chapterList = itemView.findViewById(R.id.txt_chapterList);
            ly_chapter = itemView.findViewById(R.id.ly_chapter);
        }
    }
}
