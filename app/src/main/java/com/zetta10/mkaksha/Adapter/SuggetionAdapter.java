package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zetta10.mkaksha.Activity.ChapterActivity;
import com.zetta10.mkaksha.Model.SubjectListModel.Data;
import com.zetta10.mkaksha.Model.SuggetionModel.SuggetionModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Utility.Session_for_search_history;

import java.util.List;


/*public class SuggetionAdapter extends RecyclerView.Adapter<SuggetionAdapter.MyViewHolder> {

    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<SuggetionModel> suggetionModelList;
    Session_for_search_history session_for_search_history;

    public SuggetionAdapter(Context context, List<SuggetionModel> suggetionModelList) {
        this.suggetionModelList = suggetionModelList;
        this.mcontext = context;
        session_for_search_history = new Session_for_search_history(mcontext);
    }


    @NonNull
    @Override
    public SuggetionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_last_request, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggetionAdapter.MyViewHolder holder, int position) {
//        Picasso.with(mcontext).load(subjectList.get(position).getSubjectImg()).priority(HIGH).into(holder.iv_thumb);

        holder.text.setText(suggetionModelList.get(position).getHistory());
        String history_text = suggetionModelList.get(position).getHistory();

        if (history_text.equals("")) {
            holder.ly_for_history_item.setVisibility(View.GONE);
        }
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "call");
                session_for_search_history.remove_search_text(mcontext, position);
                holder.ly_for_history_item.setVisibility(View.GONE);
               *//* Intent intent = new Intent(mcontext, ChapterActivity.class);
                intent.putExtra("chapter_id", subjectList.get(position).getId());
                intent.putExtra("subject", subjectList.get(position).getSubjectName());
                intent.putExtra("payment_data_size", payment_array_size);
                Log.e("c_id==>", "" + subjectList.get(position).getId());
                mcontext.startActivity(intent);*//*
            }
        });
    }

    @Override
    public int getItemCount() {
        return suggetionModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        ImageView iv_delete;
        LinearLayout ly_for_history_item;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            ly_for_history_item = itemView.findViewById(R.id.ly_for_history_item);
        }
    }
}*/
