package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zetta10.mkaksha.Activity.VideoDescriptionActivity;
import com.zetta10.mkaksha.Activity.WatchListActivity;
import com.zetta10.mkaksha.Model.WatchlistModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class WatchListAdapter extends RecyclerView.Adapter<WatchListAdapter.MyViewHolder> {

    private List<Data> watchlist_data;
    Context mcontext;
    String status;
    PrefManager prefManager;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_episode_title, txt_dot, ic_watch_remove;
        ImageView iv_thumb, iv_copy;
        CardView card_view;
        LinearLayout ly_click_watchList;

        public MyViewHolder(View view) {
            super(view);
            iv_thumb = (ImageView) view.findViewById(R.id.iv_thumb);
            txt_episode_title = (TextView) view.findViewById(R.id.txt_episode_title);
            txt_dot = (TextView) view.findViewById(R.id.txt_dot);
            ic_watch_remove = (TextView) view.findViewById(R.id.ic_watch_remove);
            card_view = (CardView) view.findViewById(R.id.card_view);
            ly_click_watchList = view.findViewById(R.id.ly_click_watchList);
        }
    }

    public WatchListAdapter(Context context, List<Data> watchlist_data) {
        this.watchlist_data = watchlist_data;
        this.mcontext = context;
        prefManager = new PrefManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.watch_item_land, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Glide.with(mcontext).load(watchlist_data.get(position).getThumbnail()).into(holder.iv_thumb);
        holder.txt_episode_title.setText(watchlist_data.get(position).getVideo_name());
        holder.iv_thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("click", "call" + position);
                Log.e("click", "click me");
                Intent intent = new Intent(mcontext, VideoDescriptionActivity.class);
                intent.putExtra("video_id", watchlist_data.get(position).getVideoId());
                intent.putExtra("video_data", watchlist_data.get(position).getVideo_name());
                Log.e("video_id2==>", "" + watchlist_data.get(position).getVideoId());
                mcontext.startActivity(intent);
            }
        });


        /*holder.ic_watch_remove.setLongClickable(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
            });*/


        holder.iv_thumb.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                      //  Toast.makeText(mcontext, "action_example", Toast.LENGTH_SHORT).show();

                        //Creating the instance of PopupMenu
                        PopupMenu popup = new PopupMenu(mcontext, holder.ic_watch_remove);
                        //Inflating the Popup using xml file
                        popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

                        //registering popup with OnMenuItemClickListener
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                              //  Toast.makeText(mcontext, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
//                                status = prefManager.getValue("fullstatus");
                                Log.e("==>data", "" + status);
                                holder.ly_click_watchList.setVisibility(View.GONE);
                                int video_id = watchlist_data.get(position).getVideoId();
                                WatchListActivity.watch_list(video_id);
/*
                                if (status.equals("1")) {
//                                    image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark_fill));
                                    Toast.makeText(mcontext, "" + "remove", Toast.LENGTH_SHORT).show();

//                                    prefManager.setValue("fullstatus", status);
                                } else {
//                                    image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark));
                                    Toast.makeText(mcontext, "" + "messge", Toast.LENGTH_SHORT).show();
                                    prefManager.setValue("halfstatus", status);
                                }*/
                                return true;
                            }
                        });
                        popup.show(); //showing popup menu
                        return true;
                    }
                }
        );


        holder.txt_dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        holder.ic_watch_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(mcontext, holder.ic_watch_remove);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.poupup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        /*Toast.makeText(
                                mcontext,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();*/
                        return true;
                    }
                });

                popup.show(); //showing popup menu

            }
        });
    }

    @Override
    public int getItemCount() {
        return watchlist_data.size();
    }
}
