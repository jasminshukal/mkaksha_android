package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.PdfViewer;
import com.zetta10.mkaksha.Model.PdfViewerModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class PdfViewAdapter extends RecyclerView.Adapter<PdfViewAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<com.zetta10.mkaksha.Model.PdfViewerModel.Data> pdf_view_List;

    public PdfViewAdapter(Context context, List<Data> pdf_view_List) {
        this.pdf_view_List = pdf_view_List;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pdf_view_design, parent, false);

        return new PdfViewAdapter.MyviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        holder.pdf_name.setText(pdf_view_List.get(position).getName());
        holder.linear_for_content_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Log.e("click", "call");
                Intent intent = new Intent(mcontext, PdfViewer.class);
                intent.putExtra("read", pdf_view_List.get(position).getPath());
                intent.putExtra("name", pdf_view_List.get(position).getName());
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pdf_view_List.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView pdf_name;
        LinearLayout linear_for_content_pdf;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            pdf_name = itemView.findViewById(R.id.pdf_name);
            linear_for_content_pdf = itemView.findViewById(R.id.linear_for_content_pdf);
        }
    }
}
