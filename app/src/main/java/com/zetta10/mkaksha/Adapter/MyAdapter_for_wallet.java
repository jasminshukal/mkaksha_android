package com.zetta10.mkaksha.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.zetta10.mkaksha.Fragment.CoinsFragment;
import com.zetta10.mkaksha.Fragment.RewardsFragment;

public class MyAdapter_for_wallet extends FragmentPagerAdapter {

    Context context;
    int totaltabs;

    public MyAdapter_for_wallet(Context context, @NonNull FragmentManager fm, int totaltabs) {
        super(fm);
        this.context = context;
        this.totaltabs = totaltabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                CoinsFragment coinFragment = new CoinsFragment();
                return coinFragment;
            case 1:
                RewardsFragment rewardFragment = new RewardsFragment();
                return rewardFragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return totaltabs;
    }
}
