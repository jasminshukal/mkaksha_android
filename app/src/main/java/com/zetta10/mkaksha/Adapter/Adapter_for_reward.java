package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zetta10.mkaksha.Model.RewardModel.Data_Reward_Model;
import com.zetta10.mkaksha.R;

import java.util.ArrayList;

public class Adapter_for_reward extends RecyclerView.Adapter<Adapter_for_reward.Holder> {

    Context context;
    ArrayList<Data_Reward_Model> reward_ArrayList;

    public Adapter_for_reward(Context context, ArrayList<Data_Reward_Model> reward_ArrayList) {
        this.context = context;
        this.reward_ArrayList = reward_ArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_reward,parent,false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.reward_date.setText(reward_ArrayList.get(position).getAdded_date());
        holder.textview_coursename.setText(reward_ArrayList.get(position).getGiftDescription());
        String img_url = reward_ArrayList.get(position).getImg();
        Glide.with(context).load(img_url).into(holder.image_for_reward);
    }

    @Override
    public int getItemCount() {
        if (reward_ArrayList != null){
            return reward_ArrayList.size();
        }else {
            return 0;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView reward_date,textview_coursename;
        ImageView image_for_reward;
        public Holder(@NonNull View itemView) {
            super(itemView);

            reward_date = itemView.findViewById(R.id.reward_date);
            image_for_reward = itemView.findViewById(R.id.image_for_reward);
            textview_coursename = itemView.findViewById(R.id.textview_coursename);
        }
    }
}
