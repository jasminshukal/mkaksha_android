package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.Megatest;
import com.zetta10.mkaksha.Model.pojoclass.Available_test_list_pojo;
import com.zetta10.mkaksha.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cn.iwgang.countdownview.CountdownView;

public class Adapter_for_ava_test extends RecyclerView.Adapter<Adapter_for_ava_test.Holder> {

    public Context context;
    public ArrayList<Available_test_list_pojo> arrayList_available_test;
    public Date date1, date2;
    public long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds, different, different1;
    public long secondsInMilli, minutesInMilli, hoursInMilli, daysInMilli;
    public String days, hours, minute, second;

    public Adapter_for_ava_test(Context context, ArrayList arrayList) {
        this.context = context;
        this.arrayList_available_test = arrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_avai_test, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.textview_for_test_name.setText(arrayList_available_test.get(position).getName());
        holder.textview_start_test_date.setText(arrayList_available_test.get(position).getStartDate());
        String current_date_time = arrayList_available_test.get(position).getCurrentDateTime();
        String current_date = arrayList_available_test.get(position).getCurrentDate();
        String current_time = arrayList_available_test.get(position).getCurrentTime();
        String start_date_time = arrayList_available_test.get(position).getStartTime();
        String start_time = arrayList_available_test.get(position).getStartTimeOnly();
        String start_date = arrayList_available_test.get(position).getStartDate();
        String duration = arrayList_available_test.get(position).getDuration();
        String test_id = String.valueOf(arrayList_available_test.get(position).getId());
        String is_gift = arrayList_available_test.get(position).getIsGift();
        String gift_desc = arrayList_available_test.get(position).getGiftDescription();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
        try {
            date1 = simpleDateFormat.parse(current_date_time);
            //  String new_time = simpleDateFormat.format(new Date(System.currentTimeMillis()+900000));
            date2 = simpleDateFormat.parse(start_date_time);
            printDifference(date1, date2);

            days = String.valueOf(elapsedDays);
            hours = String.valueOf(elapsedHours);
            minute = String.valueOf(elapsedMinutes);
            second = String.valueOf(elapsedSeconds);
            if (elapsedDays <= 7) {
                if (elapsedDays > 0) {
                    holder.text_for_test_rem_time.setVisibility(View.VISIBLE);
                    holder.text_for_test_rem_time.setText(days);
                    holder.text_for_test_rem_data.setText("Days");
                    holder.text_remaining.setVisibility(View.VISIBLE);
                    holder.countdownView.setVisibility(View.GONE);
                }

                if (elapsedDays <= 0) {
                    holder.countdownView.start(different1);
                    holder.countdownView.setVisibility(View.VISIBLE);

                    holder.text_for_test_rem_time.setVisibility(View.GONE);
                    // holder.text_for_test_rem_time.setText(hours + ":" + minute+":"+second);
                    holder.text_for_test_rem_data.setText("Time");
                    holder.text_remaining.setVisibility(View.VISIBLE);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
       /* if (elapsedDays <= 0) {

        }*/
       /* CountDownTimer countDownTimer = new CountDownTimer(different,1000) {
            @Override
            public void onTick(long l) {
                if (elapsedDays <= 0) {
                    long millisUntilFinished = l;
                    String hour = String.valueOf(millisUntilFinished / hoursInMilli);
                    String minute = String.valueOf(millisUntilFinished / minutesInMilli);
                    String sec = String.valueOf(millisUntilFinished / secondsInMilli);
                    holder.text_for_test_rem_time.setText(hour + ":" + minute + ":" + sec);
                }
            }

            @Override
            public void onFinish() {

            }
        };
        countDownTimer.start();*/
        holder.layout_for_avai_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String temp_current_date_time = "18-07-2020 03:18:53 PM";
                String temp_start_date_time = "18-07-2020 03:20:53 PM";

                if (date1.getTime() >= date2.getTime()) {
                    long temp_start_time = (date2.getTime() + 900000);
                    if (date1.getTime() <= temp_start_time) {

                        Intent intent = new Intent(context, Megatest.class);
                        intent.putExtra("duration", duration);
                        intent.putExtra("test_id", test_id);
                        intent.putExtra("is_gift", is_gift);
                        intent.putExtra("gift_desc", gift_desc);
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (arrayList_available_test != null) {
            return arrayList_available_test.size();
        } else {
            return 0;
        }

    }

    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        different = endDate.getTime() - startDate.getTime();
        different1 = endDate.getTime() - startDate.getTime();
        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        secondsInMilli = 1000;
        minutesInMilli = secondsInMilli * 60;
        hoursInMilli = minutesInMilli * 60;
        daysInMilli = hoursInMilli * 24;

        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;

       /* System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);*/
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView textview_for_test_name, textview_start_test_date, text_for_test_rem_time, text_for_test_rem_data, text_remaining;
        LinearLayout layout_for_avai_test;
        CountdownView countdownView;

        public Holder(@NonNull View itemView) {
            super(itemView);

            textview_for_test_name = itemView.findViewById(R.id.textview_for_test_name);
            textview_start_test_date = itemView.findViewById(R.id.textview_start_test_date);
            layout_for_avai_test = itemView.findViewById(R.id.layout_for_avail_test);
            text_for_test_rem_time = itemView.findViewById(R.id.text_for_test_rem_time);
            text_for_test_rem_data = itemView.findViewById(R.id.text_for_test_rem_data);
            text_remaining = itemView.findViewById(R.id.text_remaining);
            countdownView = itemView.findViewById(R.id.countdownview);
        }
    }
}
