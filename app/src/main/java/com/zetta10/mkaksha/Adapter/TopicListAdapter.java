package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.TopicActivity;
import com.zetta10.mkaksha.Activity.VideoListActivity;
import com.zetta10.mkaksha.Model.TopicListModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class TopicListAdapter extends RecyclerView.Adapter<TopicListAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<com.zetta10.mkaksha.Model.TopicListModel.Data> chapterList;

    public TopicListAdapter(Context context, List<Data> chapter_dataList) {
        this.chapterList = chapter_dataList;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.topic_list_design, parent, false);

        return new TopicListAdapter.MyviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        holder.txt_topicList.setText(chapterList.get(position).getTopicName());
//        holder.txt_topic_id.setText(chapterList.get(position).getSrId());
        holder.txt_topic_id.setText(Integer.toString(chapterList.get(position).getSrId()));

        holder.ly_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Intent intent = new Intent(mcontext, VideoListActivity.class);
                intent.putExtra("v_topic_id", chapterList.get(position).getId());
                intent.putExtra("v_topic_name", chapterList.get(position).getTopicName());
                Log.e("v_topic_id==>", "" + chapterList.get(position).getId());
                mcontext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return chapterList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView txt_topicList, txt_topic_id;
        LinearLayout ly_topic;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            txt_topicList = itemView.findViewById(R.id.txt_topicList);
            ly_topic = itemView.findViewById(R.id.ly_topic);
            txt_topic_id = itemView.findViewById(R.id.txt_topic_id);
        }
    }
}
