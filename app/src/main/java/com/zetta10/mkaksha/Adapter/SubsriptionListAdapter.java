package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Activity.Subsription_desc_Activity;
import com.zetta10.mkaksha.Model.PackagesListModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class SubsriptionListAdapter extends RecyclerView.Adapter<SubsriptionListAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<com.zetta10.mkaksha.Model.PackagesListModel.Data> subscription_List;

    public SubsriptionListAdapter(Context context, List<Data> subscription_List) {
        this.subscription_List = subscription_List;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription_design, parent, false);

        return new SubsriptionListAdapter.MyviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        holder.txt_rs_discount.setPaintFlags(holder.txt_rs_discount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        holder.txt_rs_discount.setBackground(mcontext.getResources().getDrawable(R.drawable.strike_through));
        holder.txt_rs.setText(subscription_List.get(position).getNewPrice());
        holder.txt_rs_discount.setText(subscription_List.get(position).getOld_price());
        holder.txt_month_plan.setText(subscription_List.get(position).getName());
//        holder.txt_std.setText(subscription_List.get(position).getName());
//        Picasso.with(mcontext).load(videoList.get(position).getThumbnail()).into(holder.iv_thumb);
        holder.linear_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Log.e("click", "call");
                Intent intent = new Intent(mcontext, Subsription_desc_Activity.class);
                intent.putExtra("subscription_id", subscription_List.get(position).getId());
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subscription_List.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView txt_std, txt_month_plan, txt_rs, txt_rs_discount, txt_all_subject;
        LinearLayout linear_login;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            txt_std = itemView.findViewById(R.id.txt_std);
            txt_month_plan = itemView.findViewById(R.id.txt_month_plan);
            txt_rs = itemView.findViewById(R.id.txt_rs);
            txt_rs_discount = itemView.findViewById(R.id.txt_rs_discount);
            txt_all_subject = itemView.findViewById(R.id.txt_all_subject);
            linear_login = itemView.findViewById(R.id.linear_login);
        }
    }
}
