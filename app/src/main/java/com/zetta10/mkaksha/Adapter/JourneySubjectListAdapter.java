package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zetta10.mkaksha.Activity.ChapterActivity;
import com.zetta10.mkaksha.Activity.Subject_wise_Journey_Activity;
import com.zetta10.mkaksha.Model.SubjectListModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;


public class JourneySubjectListAdapter extends RecyclerView.Adapter<JourneySubjectListAdapter.MyViewHolder> {

    Context mcontext;
    PrefManager prefManager;
    String from, payment_array_size;
    private List<Data> subjectList;

    public JourneySubjectListAdapter(Context context, List<Data> subject_dataList, String from) {
        this.subjectList = subject_dataList;
        this.from = from;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public JourneySubjectListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_journry_subject_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull JourneySubjectListAdapter.MyViewHolder holder, int position) {
//        Picasso.with(mcontext).load(subjectList.get(position).getSubjectImg()).priority(HIGH).into(holder.iv_thumb);

//        Glide.with(mcontext).load(subjectList.get(position).getSubjectImg()).into(holder.iv_thumb);
        holder.txt_sub_name.setText(subjectList.get(position).getSubjectName());
        holder.linear_for_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "call");
                Intent intent = new Intent(mcontext, Subject_wise_Journey_Activity.class);
                intent.putExtra("chapter_id", subjectList.get(position).getId());
                intent.putExtra("subject", subjectList.get(position).getSubjectName());
                intent.putExtra("payment_data_size", payment_array_size);
                Log.e("c_id==>", "" + subjectList.get(position).getId());
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjectList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_sub_name;
        ImageView iv_thumb;
        LinearLayout linear_for_subject;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_sub_name = itemView.findViewById(R.id.txt_sub_name);
            iv_thumb = itemView.findViewById(R.id.iv_thumb);
            linear_for_subject = itemView.findViewById(R.id.linear_for_subject);
        }
    }
}
