package com.zetta10.mkaksha.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.zetta10.mkaksha.Activity.SubscriptionActivity;
import com.zetta10.mkaksha.Activity.VideoDescriptionActivity;
import com.zetta10.mkaksha.Activity.YouTubeActivity;
import com.zetta10.mkaksha.Model.BannerListModel.Data;
import com.zetta10.mkaksha.R;

import java.util.List;

public class BannerAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Activity context;
    private List<Data> mBennerList;
    String video_All, action;

    public BannerAdapter(Activity context, List<Data> itemChannels) {
        this.context = context;
        this.mBennerList = itemChannels;
        inflater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return mBennerList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View imageLayout = inflater.inflate(R.layout.banner_item_row, container, false);
        assert imageLayout != null;
        video_All = mBennerList.get(position).getIntent();



        ImageView imageView = imageLayout.findViewById(R.id.image);

        Log.e("image==1", "" + mBennerList.get(position).getValue());
        /*Picasso.with(context).load(mBennerList.get(position).getValue()).resize(400, 400)
                .placeholder(R.drawable.logo).centerInside().into(imageView);*/
        Glide.with(context).load(mBennerList.get(position).getValue()).into(imageView);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action = mBennerList.get(position).getAction();
                Log.e("action==>", "" + action);

                if (action.equals("self-hosted")) {
                    Intent intent = new Intent(context, VideoDescriptionActivity.class);
                    intent.putExtra("video_id", mBennerList.get(position).getVideoId());
                    intent.putExtra("video_data", mBennerList.get(position).getVideoName());
                    /*intent.putExtra("video_id", mBennerList.get(position).getId());
                    intent.putExtra("video_data", mBennerList.get(position).getVideoName());
                    Log.e("video_id==>", "" + mBennerList.get(position).getId());*/
                    context.startActivity(intent);
                } else if (action.equals("youtube")) {
                    Intent intent = new Intent(context, YouTubeActivity.class);
                    intent.putExtra("you_url", mBennerList.get(position).getUrl());
                    context.startActivity(intent);

                } else if (action.equals("banner")) {
                    Intent intent = new Intent(context, SubscriptionActivity.class);
//                    intent.putExtra("video_id", 5);
//                    intent.putExtra("video_data", "a");
                    /*intent.putExtra("video_id", mBennerList.get(position).getId());
                    intent.putExtra("video_data", mBennerList.get(position).getVideoName());
                    Log.e("video_id==>", "" + mBennerList.get(position).getId());*/
                    context.startActivity(intent);
                }
            }
        });

        container.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        (container).removeView((View) object);
    }

}