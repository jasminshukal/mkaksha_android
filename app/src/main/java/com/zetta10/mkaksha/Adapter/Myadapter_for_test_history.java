package com.zetta10.mkaksha.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.zetta10.mkaksha.Fragment.Attempted_test_Fragment;
import com.zetta10.mkaksha.Fragment.NotAttempted_testFragment;

public class Myadapter_for_test_history extends FragmentPagerAdapter {

    Context context;
    int totaltabs;

    public Myadapter_for_test_history(Context context, @NonNull FragmentManager fm, int totaltabs) {
        super(fm);
        this.context = context;
        this.totaltabs = totaltabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Attempted_test_Fragment testFragment = new Attempted_test_Fragment();
                return testFragment;
            case 1:
                NotAttempted_testFragment resultFragment = new NotAttempted_testFragment();
                return resultFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totaltabs;
    }
}
