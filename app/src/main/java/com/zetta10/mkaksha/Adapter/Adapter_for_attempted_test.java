package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.pojoclass.Data_attempted_test_list;
import com.zetta10.mkaksha.R;

import java.util.ArrayList;

public class Adapter_for_attempted_test extends RecyclerView.Adapter<Adapter_for_attempted_test.DataViewHolder> {

    Context context;
    private ArrayList<Data_attempted_test_list> attemptedTestLists;

    public Adapter_for_attempted_test(Context context, ArrayList<Data_attempted_test_list> attemptedTestLists) {
        this.context = context;
        this.attemptedTestLists = attemptedTestLists;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_attempted_test, parent, false);
        DataViewHolder dataViewHolder = new DataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {


        holder.textview_for_test_name.setText(attemptedTestLists.get(position).getName());
        holder.text_for_test_date.setText(attemptedTestLists.get(position).getStart_date());


    }

    @Override
    public int getItemCount() {
        if (attemptedTestLists != null){
            return attemptedTestLists.size();
        }else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        TextView textview_for_test_name, text_for_test_date;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);

            textview_for_test_name = itemView.findViewById(R.id.textview_for_test_name);
            text_for_test_date = itemView.findViewById(R.id.text_for_test_date);

        }
    }
}
