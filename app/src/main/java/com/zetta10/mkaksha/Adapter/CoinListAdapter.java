package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.CoinModel.History;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;


public class CoinListAdapter extends RecyclerView.Adapter<CoinListAdapter.MyViewHolder> {

    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<History> coin_dataList;

    public CoinListAdapter(Context context, List<History> coin_dataList, String from) {
        this.coin_dataList = coin_dataList;
        this.from = from;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public CoinListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coin_desinge, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinListAdapter.MyViewHolder holder, int position) {
        holder.txt_date.setText("Date.  " + coin_dataList.get(position).getDate());
        Log.e("coin==>", "" + coin_dataList.get(position).getDate());
        Log.e("coin==>", "" + coin_dataList.get(position).getId());
        holder.txt_value.setText(coin_dataList.get(position).getValue());
        holder.txt_history.setText(coin_dataList.get(position).getMassage());
        coin_dataList.get(position).getOperation();
        Log.e("getOperation==>", "" + coin_dataList.get(position).getOperation());
        if (coin_dataList.get(position).getOperation().equals("Cr")) {
            holder.image_for_plus.setVisibility(View.VISIBLE);
            holder.image_for_minus.setVisibility(View.GONE);
        } else if (coin_dataList.get(position).getOperation().equals("Dr")) {
            holder.image_for_plus.setVisibility(View.GONE);
            holder.image_for_minus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return coin_dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_value, txt_date, image_for_plus, image_for_minus, txt_history;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_value = itemView.findViewById(R.id.txt_value);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_history = itemView.findViewById(R.id.txt_history);
            image_for_plus = itemView.findViewById(R.id.image_for_plus);
            image_for_minus = itemView.findViewById(R.id.image_for_minus);
        }
    }
}
