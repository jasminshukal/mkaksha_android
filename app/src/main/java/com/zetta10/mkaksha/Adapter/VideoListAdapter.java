package com.zetta10.mkaksha.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.zetta10.mkaksha.Activity.VideoDescriptionActivity;
import com.zetta10.mkaksha.Model.VideoListModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<com.zetta10.mkaksha.Model.VideoListModel.Data> videoList;

    public VideoListAdapter(Context context, List<Data> video_dataList) {
        this.videoList = video_dataList;
        this.mcontext = context;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_design, parent, false);

        return new VideoListAdapter.MyviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        holder.txt_title.setText(videoList.get(position).getVideoName());
//        Picasso.with(mcontext).load(videoList.get(position).getThumbnail()).into(holder.iv_thumb);
        Glide.with(mcontext).load(videoList.get(position).getThumbnail()).into(holder.iv_thumb);
        holder.iv_thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click me");
                Log.e("click", "call");
                Intent intent = new Intent(mcontext, VideoDescriptionActivity.class);
                intent.putExtra("video_id", videoList.get(position).getId());
                intent.putExtra("video_data", videoList.get(position).getVideoName());
                Log.e("video_id==>", "" + videoList.get(position).getId());

                prefManager.setValue_resume("resume_video_id",  videoList.get(position).getId());
                prefManager.setValue("resume_video_data", "" + videoList.get(position).getVideoName());
                prefManager.setValue("resume_image", "" + videoList.get(position).getThumbnail());
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView txt_title;
        ImageView iv_thumb;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            txt_title = itemView.findViewById(R.id.txt_title);
            iv_thumb = itemView.findViewById(R.id.iv_thumb);
        }
    }
}
