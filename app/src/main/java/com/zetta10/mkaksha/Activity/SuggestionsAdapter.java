package com.zetta10.mkaksha.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

abstract class SuggestionsAdapter<S, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> implements Filterable {
    protected List<S> suggestions = new ArrayList<>();
    protected List<S> suggestions_clone = new ArrayList<>();
    private final LayoutInflater inflater;
    protected int maxSuggestionsCount = 5;


    public SuggestionsAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public interface OnItemViewClickListener {
        void OnItemClickListener(int position, View v);

        void OnItemDeleteListener(int position, View v);
    }

    public abstract int getSingleViewHeight();

    public abstract void onBindSuggestionHolder(S suggestion, V holder, int position);

    @Override
    public int getItemCount() {
        return suggestions.size();
    }

    public int getListHeight() {
        return getItemCount() * getSingleViewHeight();
    }

    public void setMaxSuggestionsCount(int maxSuggestionsCount) {
        this.maxSuggestionsCount = maxSuggestionsCount;
    }

    public void addSuggestion(S r) {
        if (maxSuggestionsCount <= 0)
            return;

        if (r == null)
            return;
        if (!suggestions.contains(r)) {
            if (suggestions.size() >= maxSuggestionsCount) {
                suggestions.remove(maxSuggestionsCount - 1);
            }
            suggestions.add(0, r);
        } else {
            suggestions.remove(r);
            suggestions.add(0, r);
        }
        suggestions_clone = suggestions;
        notifyDataSetChanged();
    }

    public void setSuggestions(List<S> suggestions) {
        this.suggestions = suggestions;
        suggestions_clone = suggestions;
        notifyDataSetChanged();
    }

    public void clearSuggestions() {
        suggestions.clear();
        suggestions_clone = suggestions;
        notifyDataSetChanged();
    }

    public void deleteSuggestion(int position, S r) {
        if (r == null)
            return;
        //delete item with animation
        if (suggestions.contains(r)) {
            this.notifyItemRemoved(position);
            suggestions.remove(r);
            suggestions_clone = suggestions;
        }
    }

    public List<S> getSuggestions() {
        return suggestions;
    }

    public int getMaxSuggestionsCount() {
        return maxSuggestionsCount;
    }

    protected LayoutInflater getLayoutInflater() {
        return this.inflater;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @NonNull
    @Override
    public V onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {
        onBindSuggestionHolder(suggestions.get(position), holder, position);
    }


}
