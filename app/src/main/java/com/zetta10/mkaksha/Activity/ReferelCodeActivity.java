package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferelCodeActivity extends AppCompatActivity {

    EditText ed_refer;
    LinearLayout linear_apply_refer, finish_ch_list;
    String refere_code;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    String referral_code, referral_code_second;
    String fname, lname, email, mobile, pass, con_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.referel_code_activity);
        init();
        prefManager = new PrefManager(ReferelCodeActivity.this);
        progressDialog = new ProgressDialog(ReferelCodeActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        try {
            Log.e("u_fname", "" + prefManager.getValue("u_fname"));
            Log.e("u_lname", "" + prefManager.getValue("u_lname"));
            Log.e("u_email", "" + prefManager.getValue("u_email"));
            Log.e("u_phone", "" + prefManager.getValue("u_phone"));
            Log.e("u_pass", "" + prefManager.getValue("u_pass"));
            Log.e("u_con_pass", "" + prefManager.getValue("u_con_pass"));

            fname = prefManager.getValue("u_fname");
            lname = prefManager.getValue("u_lname");
            email = prefManager.getValue("u_email");
            mobile = prefManager.getValue("u_phone");
            pass = prefManager.getValue("u_pass");
            con_pass = prefManager.getValue("u_con_pass");
        } catch (Exception e) {
            Log.e("detail_user==>", "" + e.getMessage());
        }

        linear_apply_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refere_code = ed_refer.getText().toString().trim();
                if (TextUtils.isEmpty(refere_code)) {
                    Toast.makeText(getApplicationContext(), "Enter Refere_code", Toast.LENGTH_LONG).show();
                    return;
                }
                generate_code_check(refere_code);

            }
        });
    }

    private void generate_code_check(String refere_code) {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.CheckReferralCode(refere_code);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");
                    if (status.equals("1")) {
                        referral_code_second = jsonObject.getJSONObject("data").getString("referral_code");
                        String agent_id = jsonObject.getJSONObject("data").getString("agent_id");
                        String agent_name = jsonObject.getJSONObject("data").getString("agent_name");
                        String occupation = jsonObject.getJSONObject("data").getString("occupation");
                        Log.e("msg==>", "" + msg);
                        Toast.makeText(ReferelCodeActivity.this, msg, Toast.LENGTH_LONG).show();
                        /*if (referral_code_second.equals(refere_code)) {
                            Toast.makeText(ReferelCodeActivity.this, "" + msg, Toast.LENGTH_LONG).show();
                        }*/
                        Intent intent = new Intent(ReferelCodeActivity.this, Registration.class);
                        Log.e("refer_code==>", "" + referral_code_second);
                        intent.putExtra("referral_code_second", referral_code_second);
                        intent.putExtra("s_fname", fname);
                        intent.putExtra("s_lname", lname);
                        intent.putExtra("s_email", email);
                        intent.putExtra("s_mobile", mobile);
                        intent.putExtra("s_pass", pass);
                        intent.putExtra("s_con_pass", con_pass);
                        startActivity(intent);
                        finish();
                    } else if (status.equals("0")) {
                        Toast.makeText(ReferelCodeActivity.this, "" + msg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("==>", "" + e.getMessage());
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init() {
        ed_refer = findViewById(R.id.ed_refer);
        linear_apply_refer = findViewById(R.id.linear_apply_refer);
        finish_ch_list = findViewById(R.id.finish_ch_list);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
