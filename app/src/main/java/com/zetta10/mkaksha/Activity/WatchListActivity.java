package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Adapter.WatchListAdapter;
import com.zetta10.mkaksha.Model.WatchlistModel.Data;
import com.zetta10.mkaksha.Model.WatchlistModel.WatchlistModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WatchListActivity extends AppCompatActivity {

    LinearLayout finish_video_list, ly_dataNotFound;
    TextView toolbar_title;
    RecyclerView rv_watchlist;
    static ProgressDialog progressDialog;
    static PrefManager prefManager;
    WatchListAdapter downloadAdapter;
    List<Data> WatchListList;
    static String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.watch_list_activity);
        init();
        prefManager = new PrefManager(WatchListActivity.this);
        progressDialog = new ProgressDialog(WatchListActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);


        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getwatchlist();
    }


    private void getwatchlist() {
        try {
            progressDialog.show();
            AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
            Call<WatchlistModel> call = bookNPlayAPI.FevList(prefManager.gettokenId(), prefManager.getLoginId());
            call.enqueue(new Callback<WatchlistModel>() {
                @Override
                public void onResponse(Call<WatchlistModel> call, Response<WatchlistModel> response) {
                    progressDialog.dismiss();
                    try {
                        WatchListList = new ArrayList<>();
                        WatchListList = response.body().getData();
                        Log.e("WatchListList", "" + WatchListList.size());

                        if (WatchListList.size() > 0) {
                            downloadAdapter = new WatchListAdapter(WatchListActivity.this, WatchListList);
                            rv_watchlist.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(WatchListActivity.this,
                                    LinearLayoutManager.VERTICAL, false);
//                            rv_watchlist.setLayoutManager(new GridLayoutManager(WatchListActivity.this, 2));
                            rv_watchlist.setLayoutManager(mLayoutManager3);
                            rv_watchlist.setItemAnimator(new DefaultItemAnimator());
                            rv_watchlist.setAdapter(downloadAdapter);
                            downloadAdapter.notifyDataSetChanged();
                            rv_watchlist.setVisibility(View.VISIBLE);
//                            ly_no_record.setVisibility(View.GONE);
                        } else {
                            rv_watchlist.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        Log.e("===>", "" + e.getMessage());
                    }

                }

                @Override
                public void onFailure(Call<WatchlistModel> call, Throwable t) {
                    progressDialog.dismiss();
                    rv_watchlist.setVisibility(View.GONE);
//                    ly_no_record.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception e) {
            Log.e("===>", "" + e.getMessage());
        }
    }


    public static void watch_list(int video_id) {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.VideoFev(prefManager.gettokenId(), prefManager.getLoginId(), video_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.e("output==>", "" + jsonObject);
                        String messge = jsonObject.getString("msg");
                        status = jsonObject.getString("status");
                        Log.e("status_video==>", "" + status);
                        prefManager.setValue("fullstatus", status);
                        if (status.equals("1")) {
//                            image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark_fill));
//                            Toast.makeText(WatchListActivity.this, "" + messge, Toast.LENGTH_SHORT).show();

                        } else {
//                            image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark));
                        }
                    } else {
                        Log.e("error==>", "error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                progressDialog.dismiss();
                Log.e("errors==>", "" + t.getMessage());
            }
        });
    }

    private void init() {
        finish_video_list = findViewById(R.id.finish_video_list);
        toolbar_title = findViewById(R.id.toolbar_title);
        rv_watchlist = findViewById(R.id.rv_watchlist);
        ly_dataNotFound = findViewById(R.id.ly_dataNotFound);
    }
}
