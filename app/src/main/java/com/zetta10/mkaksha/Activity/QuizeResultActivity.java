package com.zetta10.mkaksha.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizeResultActivity extends AppCompatActivity {

    TextView quizResult, txt_coin;
    Button finish_quiz;
    String quizId, chapId, subId;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    LinearLayout finish_topic_list;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quize_result_activity);
        init();
        progressDialog = new ProgressDialog(QuizeResultActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(QuizeResultActivity.this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            quizId = bundle.getString("quizId");
            int correct = getIntent().getIntExtra("correct", 0);
            int skip = getIntent().getIntExtra("skip", 0);
            int wrong = getIntent().getIntExtra("wrong", 0);
            quizResult.setText("Correct Answers: " + correct + "\n" + "Wrong Answers: " + wrong + "\n" + "skip Question: " + skip);
            submitQuiz(correct, wrong, skip);
        }


        finish_quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuizeResultActivity.this.finish();
            }
        });

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuizeResultActivity.this.finish();
            }
        });
    }

    private void submitQuiz(Integer correct, Integer wrong, Integer skip) {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.SubmitQuiz(prefManager.gettokenId(), prefManager.getLoginId(), "1", correct, wrong);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
             /*   try {
                    progressDialog.dismiss();
                    if (response.code() == 200) {
                        quize_list = new ArrayList<>();
                        quize_list = response.body().getData();
                        Log.e("quize==>", "" + quize_list);
                        for (int i = 0; i < quize_list.size(); i++) {
                            question = quize_list.get(i).getQuestion();
                            txt_question.setText(question);
                            option1.setText(quize_list.get(i).getA());
                            option2.setText(quize_list.get(i).getB());
                            option3.setText(quize_list.get(i).getC());
                            option4.setText(quize_list.get(i).getD());
                            correct = quize_list.get(i).getRightAns();

//                            setquestion(i);
                        }
                    }
                } catch (Exception e) {
                }*/
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String success = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");
                    Log.e("msg==>", "" + msg);

                    Toast.makeText(QuizeResultActivity.this, msg, Toast.LENGTH_LONG).show();

                    String coin = jsonObject.getJSONObject("data").getString("coin");
                    Log.e("coins==>", "" + coin);

                    if (wrong == 0 && skip == 0) {
                        popup_coin(coin);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }

    private void init() {
        quizResult = findViewById(R.id.quizResult);
        finish_quiz = findViewById(R.id.finish_quiz);
        finish_topic_list = findViewById(R.id.finish_topic_list);
    }

    private void popup_coin(String coin) {
        dialog = new Dialog(QuizeResultActivity.this);
        dialog.setContentView(R.layout.coin_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_coin = dialog.findViewById(R.id.txt_coin);
        txt_coin.setText("You Earn " + coin + " Coins");

        ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
        dialog.show();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
