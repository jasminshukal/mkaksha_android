package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Adapter.TopicListAdapter;
import com.zetta10.mkaksha.Model.TopicListModel.Data;
import com.zetta10.mkaksha.Model.TopicListModel.TopicListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopicActivity extends AppCompatActivity {
    LinearLayout finish_topic_list, ly_dataNotFound;
    TextView toolbar_title;
    RecyclerView rv_topic_list;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    Integer c_id, subject_id;
    String chapter_name;

    List<Data> topic_dataList;
    TopicListAdapter topicListAdapter;
    String api_token, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.topic_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        init();
        prefManager = new PrefManager(TopicActivity.this);
        progressDialog = new ProgressDialog(TopicActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            c_id = bundle.getInt("topic_id");
            subject_id = bundle.getInt("topic_s_id");
            chapter_name = bundle.getString("topic");
            Log.e("chapter_id", "" + c_id);

            Topic_list(subject_id, c_id);
            toolbar_title.setText(chapter_name);
        }

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        finish_topic_list = findViewById(R.id.finish_topic_list);
        ly_dataNotFound = findViewById(R.id.ly_dataNotFound);
        toolbar_title = findViewById(R.id.toolbar_title);
        rv_topic_list = findViewById(R.id.rv_topic_list);
    }

    private void Topic_list(Integer subject_id, Integer chapter_id) {
        progressDialog.show();
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;

            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<TopicListModel> call = appAPI.topic_list_call(api_token, user_id, subject_id, chapter_id);
            call.enqueue(new Callback<TopicListModel>() {
                @Override
                public void onResponse(Call<TopicListModel> call, Response<TopicListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        topic_dataList = new ArrayList<>();
                        topic_dataList = response.body().getData();
                        Log.e("dataList==>", "" + topic_dataList.size());
                        if (topic_dataList.size() > 0) {
                            topicListAdapter = new TopicListAdapter(TopicActivity.this, topic_dataList);
                            rv_topic_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(TopicActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_topic_list.setLayoutManager(mLayoutManager3);
                            rv_topic_list.setItemAnimator(new DefaultItemAnimator());
                            rv_topic_list.setAdapter(topicListAdapter);
                            topicListAdapter.notifyDataSetChanged();
                            rv_topic_list.setVisibility(View.VISIBLE);
                            rv_topic_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_topic_list.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                    progressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<TopicListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog != null)
            progressDialog.dismiss();
    }

}
