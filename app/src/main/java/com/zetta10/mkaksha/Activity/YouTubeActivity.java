package com.zetta10.mkaksha.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.YouTube.YouTubeConfig;

public class YouTubeActivity extends YouTubeBaseActivity {

    YouTubePlayerView myouTubePlayerView;
    YouTubePlayer.OnInitializedListener monInitializedListener;
    LinearLayout finish_video_list;
    String you_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.you_tube_activity);
        finish_video_list = findViewById(R.id.finish_video_list);
        myouTubePlayerView = findViewById(R.id.youtubeplay);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            you_url = bundle.getString("you_url");
        }


        Log.e("==>On_create_start", "");

        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        monInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                Log.e("==>On_init_done", "");
                youTubePlayer.loadVideo(you_url);  //single Video Added
                /*List<String> video_List = new ArrayList<>();
                video_List.add("rCKcNVlPUGw");
                video_List.add("pB_xLbpCgXM");
//                video_List.add("http://mkaksha.in/public/video/mp4/10/social_science/ch1/2.m4v");
                youTubePlayer.loadVideos(video_List);*/

//                youTubePlayer.loadPlaylist("UCHf05Uxs9Bfg54emFC1GkHw");
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Log.e("==>On_init_fail", "");
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("==>On_init_start", "");
        myouTubePlayerView.initialize(YouTubeConfig.getApiKey(), monInitializedListener);
    }
}