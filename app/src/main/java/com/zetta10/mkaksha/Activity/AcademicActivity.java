package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Utility.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcademicActivity extends AppCompatActivity {

    Spinner spinner_board, spinner_standard, spinner_medium;
    PrefManager prefManager;
    String fname, lname, email, phone, pass, con_pass, latitude, longitude, str_board,
            str_standard, str_medium, udid, pass_refere;


    String status;
    Session session;
    LinearLayout linearLayout_for_signup;
    String device_name;
    String refreshedToken;
    ArrayList<String> array_std_value;
    ArrayList<String> array_med_value;
    ArrayList<String> array_board_value;

    ArrayList<String> array_std_key;
    ArrayList<String> array_med_key;
    ArrayList<String> array_board_key;

    String url_std = "http://mkaksha.in/api/v1/GetRegisterDetail/STD";
    String url_medium = "http://mkaksha.in/api/v1/GetRegisterDetail/MEDIUM";
    String url_board = "http://mkaksha.in/api/v1/GetRegisterDetail/BORD";
    String key, value;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.academic_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {
            }

            @Override
            public void onNetDisConnected() {

            }
        });
        progressDialog = new ProgressDialog(AcademicActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        spinner_board = findViewById(R.id.spinner_board);
        spinner_standard = findViewById(R.id.spinner_standard);
        spinner_medium = findViewById(R.id.spinner_medium);
        linearLayout_for_signup = findViewById(R.id.linear_for_reg);
        session = new Session(getApplicationContext());
        prefManager = new PrefManager(getApplicationContext());
        fname = prefManager.getValue("fname");
        lname = prefManager.getValue("lname");
        email = prefManager.getValue("email");
        phone = prefManager.getValue("phone");
        pass = prefManager.getValue("pass");
        con_pass = prefManager.getValue("con_pass");
        latitude = prefManager.getValue("latitude");
        longitude = prefManager.getValue("longitude");
        pass_refere = prefManager.getValue("pass_refere");

        Log.e("dataget_pass==>", "" + pass_refere);
        device_name = Build.MODEL;
        //  latitude = prefManager.getValue("latitude");
        //  longitude = prefManager.getValue("longitude");
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("token==>", "" + refreshedToken);
        udid = android.provider.Settings.System.getString(super.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);


        array_std_value = new ArrayList<String>();
        array_board_value = new ArrayList<String>();
        array_med_value = new ArrayList<String>();

        array_std_key = new ArrayList<String>();
        array_board_key = new ArrayList<String>();
        array_med_key = new ArrayList<String>();

        spinner_medium.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_medium = array_med_key.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_standard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_standard = array_std_key.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_board.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_board = array_board_key.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        linearLayout_for_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_registered_data();
            }
        });


        get_standard_data();
        get_board_data();
        get_medium_data();
    }

    /*public void get_standard_data() {
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> registercall = apiinterface.bord_call();

        registercall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject json_data = jsonObject.getJSONObject("data");
                    for (int i = 0; i < json_data.length(); i++) {
                        String bord = json_data.getString("1");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }*/

    public void get_registered_data() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> responseCall = apiinterface.registercall(pass, device_name, lname, fname, phone, email, str_board, str_medium, latitude, longitude, udid, str_standard, refreshedToken, pass_refere);

        responseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = String.valueOf(jsonObject.get("msg"));
                    //  JSONArray jsonArray_phon = json_msg.getJSONArray("phno");
                    /* String str_phone = String.valueOf(jsonArray_phon);
                    String rep_str = str_phone.replaceAll("]", "");
                    JSONArray jsonArray_email = json_msg.getJSONArray("email");
                    JSONArray jsonArray_password = json_msg.getJSONArray("password");
                    JSONArray jsonArray_lname = json_msg.getJSONArray("lname");
                    JSONArray jsonArray_fname = json_msg.getJSONArray("fname");
                    JSONArray jsonArray_board = json_msg.getJSONArray("board");
                    JSONArray jsonArray_medium = json_msg.getJSONArray("medium");
                    JSONArray jsonArray_latitude = json_msg.getJSONArray("latitude");
                    JSONArray jsonArray_longitude = json_msg.getJSONArray("longitude");
                    JSONArray jsonArray_udid = json_msg.getJSONArray("udid");
                    JSONArray jsonArray_fcm_token = json_msg.getJSONArray("fcm_token");*/
                    Toast.makeText(AcademicActivity.this, msg, Toast.LENGTH_SHORT).show();
                    prefManager.setLoginId(jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("id"));
                    prefManager.settoken(jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("api_token"));
                    prefManager.setcoin(jsonObject.getJSONObject("data").getString("coin"));
                    Log.e("token==>", "" + jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("api_token"));
                    Intent intent = new Intent(AcademicActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }

    public void get_standard_data() {

        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> standard_call = apiinterface.getstandard();

        standard_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_std_value.add(value);
                        array_std_key.add(key);
                    }


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                setdata_std();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        /*RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_std, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_std_value.add(value);
                        array_std_key.add(key);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setdata_std();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);*/

    }

    public void get_board_data() {

        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> board_call = apiinterface.getboard();

        board_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_board_value.add(value);
                        array_board_key.add(key);
                    }


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                setdata_board();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
       /* RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_board, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_board_value.add(value);
                        array_board_key.add(key);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setdata_board();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);
*/
    }

    public void get_medium_data() {

        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> medium_call = apiinterface.getmedium();

        medium_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_med_value.add(value);
                        array_med_key.add(key);
                    }


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                setdata_med();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
       /* RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_medium, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_med_value.add(value);
                        array_med_key.add(key);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setdata_med();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);*/

    }


    public void setdata_std() {

        ArrayAdapter adpter_std = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, array_std_value);
        //  adpter_std.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_standard.setAdapter(adpter_std);
    }

    public void setdata_med() {

        ArrayAdapter adpter_med = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, array_med_value);
        spinner_medium.setAdapter(adpter_med);
    }

    public void setdata_board() {

        ArrayAdapter adpter_board = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, array_board_value);
        spinner_board.setAdapter(adpter_board);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }
}
