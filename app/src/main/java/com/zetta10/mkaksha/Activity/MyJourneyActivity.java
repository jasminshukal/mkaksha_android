package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.zetta10.mkaksha.Adapter.JourneySubjectListAdapter;
import com.zetta10.mkaksha.Model.MyJourneyModel.MyJourneyModel;
import com.zetta10.mkaksha.Model.MyJourneyModel.Subject;
import com.zetta10.mkaksha.Model.SubjectListModel.Data;
import com.zetta10.mkaksha.Model.SubjectListModel.SubjectListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyJourneyActivity extends AppCompatActivity {

    private BarChart chart;
    //    private SeekBar seekBarX, seekBarY;
    private TextView tvX, tvY;
    BarDataSet set1;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    List<Subject> subject_list;
    float percentage, all_percentage;
    List<Data> subject_dataList;
    String sub_name, status, std_ids;
    List<MyJourneyModel> journeyModelList;
    List<SubjectItem> subjectItems;
    //    String[] xAxisLables ;
    ArrayList<String> xAxisLables;
    List<BarEntry> entries;
    SeekBar seekBar;
    RecyclerView rv_all_subject;
    TextView txt_all_subject_percentage;
    JourneySubjectListAdapter subjectListAdapter;
    LinearLayout finish_ch_list;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     /*   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.my_journey_activity);
        prefManager = new PrefManager(MyJourneyActivity.this);
        progressDialog = new ProgressDialog(MyJourneyActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        chart = findViewById(R.id.chart1);
        finish_ch_list = findViewById(R.id.finish_ch_list);
        txt_all_subject_percentage = findViewById(R.id.txt_all_subject_percentage);
        rv_all_subject = findViewById(R.id.rv_all_subject);
        seekBar = findViewById(R.id.seekBar);
        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);
        chart.getAxisRight().setEnabled(false);
        chart.setTouchEnabled(false);
        chart.setPinchZoom(false);
        chart.setDoubleTapToZoomEnabled(false);
        entries = new ArrayList<>();
        xAxisLables = new ArrayList<String>();
        chart.getDescription().setEnabled(false);
        chart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawGridBackground(false);
        // chart.setDrawYLabels(false);

        YAxis yAxis = chart.getAxis(YAxis.AxisDependency.LEFT);
//        yAxis.mAxisMinimum = 1;
        yAxis.setStartAtZero(true);
        subject_list_Data();
        getdata();

        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        xAxisLables = new ArrayList<String>(){"Guj", "hindi", "social  science", "science &  technology"};
      /*  xAxisLables.add("Mon");
        xAxisLables.add("Tue");
        xAxisLables.add("Wed");
        xAxisLables.add("Thufadfadfas");
        xAxisLables.add("Frisafdafd");
        xAxisLables.add("Sat");
        xAxisLables.add("Sun");
        chart.setExtraBottomOffset(50);
*/


      /*  XAxis xAxis = chart.getXAxis();
//        chart.getXAxis().setCenterAxisLabels(true);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.mLabelRotatedWidth = 10;
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLables));
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLables));*/

/*        chart.setExtraBottomOffset(50);
        entries = new ArrayList<>();
        entries.add(new BarEntry(0f, 30f));
        entries.add(new BarEntry(1f, 80f));
        entries.add(new BarEntry(2f, 60f));
        entries.add(new BarEntry(3f, 50f));
        // gap of 2f
        entries.add(new BarEntry(4f, 70f));
        entries.add(new BarEntry(5f, 60f));*/

//        BarDataSet set = new BarDataSet(entries, "");

//        chart.setVisibleXRangeMaximum(5);
//        data.setBarWidth(0.9f); // set custom bar width
//        chart.getXAxis().setAxisMinValue(0f);     // Better remove setAxisMinValue(), as it deprecated
//        chart.groupBars(0, 0.5f, 0.5f);
        // refresh
    }

    private void getdata() {
        try {
            progressDialog.show();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<MyJourneyModel> call = appAPI.MyJourney(prefManager.gettokenId(), prefManager.getLoginId());
            call.enqueue(new Callback<MyJourneyModel>() {
                @Override
                public void onResponse(Call<MyJourneyModel> call, Response<MyJourneyModel> response) {
                    subject_list = new ArrayList<>();
                    if (response.isSuccessful()) {
                        subject_list = response.body().getData().getSubject();
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        Log.e("data3", "" + response.body().getData().getYourLaval());
                        Log.e("data4", "" + response.body().getData().getSubject().get(0).getPercentage());
                        Log.e("data5", "" + subject_list.size());
                        seekBar.getThumb().mutate().setAlpha(0);
                        all_percentage = response.body().getData().getYourLaval();
                        txt_all_subject_percentage.setText("" + all_percentage);
                        seekBar.setProgress((int) all_percentage);
                        for (int i = 0; i < subject_list.size(); i++) {
                            percentage = response.body().getData().getSubject().get(i).getPercentage();
                            sub_name = response.body().getData().getSubject().get(i).getName();
                            /*================= 11th std =================*/

                            std_ids = prefManager.getValue("std_ids");
                            Log.e("==>std_ids", "" + std_ids);
                            if (std_ids.equals("11")) {
                                if (sub_name.equals("અર્થશાસ્ત્ર")) {
                                    sub_name = "Eco.";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("નામનાં મૂળતત્વો ભાગ - 1")) {
                                    sub_name = "Account-1";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("વાણિજ્ય વ્યવસ્થા અને સંચાલન")) {
                                    sub_name = "B.O";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("વાણિજ્ય પત્રવ્યવહાર અને સેક્રેટરિયલ પ્રેક્ટિસ")) {
                                    sub_name = "S.P";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("અંગ્રેજી")) {
                                    sub_name = "English";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("આંકડાશાસ્ત્ર")) {
                                    sub_name = "State";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("નામાં ના મૂળતત્વો ભાગ-2")) {
                                    sub_name = "Account-2";
                                    xAxisLables.add(sub_name);
                                }

                            }
                            /*================= 10th std =================*/
                            if (std_ids.equals("10")) {
                                if (sub_name.equals("Mathematics")) {
                                    sub_name = "Mathematics";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("Science & Technology")) {
                                    sub_name = "Science & Technology";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("Social Science")) {
                                    sub_name = "Social Science";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("English")) {
                                    sub_name = "English";
                                    xAxisLables.add(sub_name);
                                }

                            }
                            /*================= 12th std =================*/
                            if (std_ids.equals("12")) {
                                if (sub_name.equals("અર્થશાસ્ત્ર")) {
                                    sub_name = "Eco.";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("નામનાં મૂળતત્વો ભાગ - ૧")) {
                                    sub_name = "Account-1";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("વાણિજ્ય વ્યવસ્થા અને સંચાલન")) {
                                    sub_name = "B.O";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("વાણિજ્ય પત્રવ્યવહાર અને સેક્રેટરિયલ પ્રેક્ટિસ")) {
                                    sub_name = "S.P";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("આંકડાશાસ્ત્ર ભાગ - ૧")) {
                                    sub_name = "State-1";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("અંગ્રેજી")) {
                                    sub_name = "English";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("આંકડાશાસ્ત્ર ભાગ - ૨")) {
                                    sub_name = "State-2";
                                    xAxisLables.add(sub_name);
                                }
                                if (sub_name.equals("નામાનાં મૂળતત્વો ભાગ - ૨")) {
                                    sub_name = "Account-2";
                                    xAxisLables.add(sub_name);
                                }
                            }

                            /*if (percentage == 0.0) {
                                Log.e("==>percentage", "" + percentage);
                                entries.add(new BarEntry(i,0.0f ));
                            } else {
                            }*/
                            entries.add(new BarEntry(i, percentage));
//                            xAxisLables = new String[]{sub_name};
//                            subjectItems.add(new SubjectItem(percentage, sub_name));
                        }
                        Log.e("==>sub_name", "" + xAxisLables);
//                        subjectItems.get(i)
                    /*    XAxis xAxis = chart.getXAxis();
//                        xAxis.setGranularity(1f);
//                        xAxis.setLabelRotationAngle(-90);
                        xAxis.setCenterAxisLabels(true);
//                        xAxis.setEnabled(true);
//                        xAxis.setDrawGridLines(false);
                        xAxis.setTextSize(10f);
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//                        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
//                        chart.getXAxis().setCenterAxisLabels(true);
//                        xAxis.setGranularity(1f);
//                        xAxis.setGranularityEnabled(true);*/
                        chart.setExtraBottomOffset(50);
                        XAxis xAxis = chart.getXAxis();
                        xAxis.setTextSize(10f);
//        chart.getXAxis().setCenterAxisLabels(true);
                        xAxis.setGranularity(1f);
//                        xAxis.setLabelRotationAngle(-80);
                        xAxis.setGranularityEnabled(true);
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.mLabelRotatedWidth = 10;
                        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLables));
                        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLables));

                     /*   chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLables));
                        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLables));*/

                        BarDataSet set = new BarDataSet(entries, "Video parentage");
                        BarData data = new BarData(set);
                        data.setBarWidth(0.4f);
                        xAxis.setAxisMinimum(data.getXMin() - .2f);
                        xAxis.setAxisMaximum(data.getXMax() + .2f);
                        xAxis.setSpaceMax(0.1f);
                        xAxis.setSpaceMin(0.1f);
                        chart.setData(data);
                       /* float groupSpace = 0.06f;
                        float barSpace = 0.02f;
                        chart.groupBars(0, groupSpace, barSpace);*/
                        chart.setFitBars(true); // make the x-axis fit e1actly all bars
                        chart.notifyDataSetChanged();
                        chart.invalidate();// refresh
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<MyJourneyModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }
    }

    private void subject_list_Data() {
        try {
            progressDialog.show();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<SubjectListModel> call = appAPI.subject_list_call(prefManager.gettokenId(), prefManager.getLoginId());
            call.enqueue(new Callback<SubjectListModel>() {
                @Override
                public void onResponse(Call<SubjectListModel> call, Response<SubjectListModel> response) {
                    subject_dataList = new ArrayList<>();
                    subject_dataList = response.body().getData();

                    status = response.body().getStatus();
                    Log.e("status==>", "" + status);
                    if (status.equals("1")) {
                        if (response.isSuccessful()) {
                            Log.e("data1", "" + response.body());
                            Log.e("data2", "" + response.body().getData());
                            Log.e("data3", "" + response.body().getData().get(0).getSubjectImg());

                            if (subject_dataList.size() > 0) {
                                subjectListAdapter = new JourneySubjectListAdapter(MyJourneyActivity.this, subject_dataList, "myjourney");
                                rv_all_subject.setHasFixedSize(true);
                                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(MyJourneyActivity.this,
                                        LinearLayoutManager.VERTICAL, false);
                                GridLayoutManager gridLayoutManager = new GridLayoutManager(MyJourneyActivity.this, 2,
                                        LinearLayoutManager.VERTICAL, false);

                                rv_all_subject.setLayoutManager(mLayoutManager3);
                                rv_all_subject.setItemAnimator(new DefaultItemAnimator());
                                rv_all_subject.setAdapter(subjectListAdapter);
                                subjectListAdapter.notifyDataSetChanged();
                                rv_all_subject.setVisibility(View.VISIBLE);
                                rv_all_subject.setVisibility(View.VISIBLE);
                            } else {
                                rv_all_subject.setVisibility(View.GONE);
//                            ly_paid_book.setVisibility(View.GONE);
                            }
//                            progressDialog.dismiss();
                        }
                    } else if (status.equals("2")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(MyJourneyActivity.this, "InValid Token", Toast.LENGTH_SHORT).show();
                            prefManager.setLoginId("0");
                            prefManager.settoken("0");
                            String refer_code = String.valueOf(0);
                            prefManager.setValue("referral_code", refer_code);
                            Intent intent = new Intent(MyJourneyActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            MyJourneyActivity.this.finish();
                        } catch (Exception e) {
                            Log.e("token_error==>", "" + e.getMessage());
                        }
                    } else if (status.equals("3")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(MyJourneyActivity.this, "maintenance", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MyJourneyActivity.this, PopupActivity.class);
                            intent.putExtra("status_data", status);
                            startActivity(intent);
                            MyJourneyActivity.this.finish();
                        } catch (Exception e) {
                            Log.e("error3==>", "" + e.getMessage());
                        }
                    } else if (status.equals("4")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(MyJourneyActivity.this, "Update", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MyJourneyActivity.this, PopupActivity.class);
                            intent.putExtra("status_data", status);
                            startActivity(intent);
                            MyJourneyActivity.this.finish();
                        } catch (Exception e) {
                            Log.e("error4==>", "" + e.getMessage());
                        }
                    }
//                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<SubjectListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }

    }
}
