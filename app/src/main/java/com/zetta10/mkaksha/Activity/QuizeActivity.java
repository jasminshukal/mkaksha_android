package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.zetta10.mkaksha.Model.QuizModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizeActivity extends AppCompatActivity {

    TextView option1, option2, option3, option4;
    LinearLayout linear_for_back, linear_for_skip, finish_topic_list, ly_option1, ly_option2, ly_option3, ly_option4;
    TextView txt_question, toolbar_title;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    List<Data> quize_list;
    List<QuizItem> quizItems;
    String quize_data, question, corrects,video_id_data;
    int correct = 0, wrong = 0, skip = 0;
    int currentQuestion = 0;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.quize_activity);
        init();
        prefManager = new PrefManager(QuizeActivity.this);
        progressDialog = new ProgressDialog(QuizeActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        quizItems = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            quize_data = bundle.getString("quize_data");
            video_id_data = bundle.getString("video_id_data");
            Log.e("quize_data", "" + quize_data);
        }

        linear_for_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                QuizeActivity.this.finish();
                skip++;
                if (currentQuestion < quizItems.size() + 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            progressDialog.dismiss();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds
                }

                int arr = quizItems.size();
                int minus = arr - 1;

                if (currentQuestion == minus) {
//                    linear_for_skip.setVisibility(View.GONE);
                    linear_for_skip.setEnabled(false);
                }
            }
        });

        linear_for_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuizeActivity.this.finish();
            }
        });

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ly_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                //check if answer is correct
                if (quizItems.get(currentQuestion).getAnswer1()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
//                    Toast.makeText(QuizeActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
//                    option1.setBackgroundColor(R.drawable.question_rounded_corner);
//                    ly_option1.setBackground(getResources().getDrawable(R.drawable.question_rounded_corner));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                } else {
                    //wrong
                    wrong++;
//                    Toast.makeText(QuizeActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
//                    option1.setBackgroundColor(R.drawable.question_red);
                    ly_option1.setBackground(getResources().getDrawable(R.drawable.question_red));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                            // yourMethod();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds

                } else {
                    //end result
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            Intent i = new Intent(getApplicationContext(), QuizeResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("skip", skip);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds

                }

                int arr = quizItems.size();
                int minus = arr - 1;

                if (currentQuestion == minus) {
//                    linear_for_skip.setVisibility(View.GONE);
                    linear_for_skip.setEnabled(false);
                }
                /*option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));*/
            }
        });

        ly_option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if answer is correct
                progressBar.setVisibility(View.VISIBLE);
                /*progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);*/
                if (quizItems.get(currentQuestion).getAnswer2()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
//                    Toast.makeText(QuizeActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
                } else {
                    //wrong
                    wrong++;
//                    Toast.makeText(QuizeActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
//                    option2.setBackgroundColor(R.drawable.question_red);
//                    ly_option2.setBackground(getResources().getDrawable(R.drawable.question_red));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_red));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                            // yourMethod();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds


                } else {
                    //end result

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            Intent i = new Intent(getApplicationContext(), QuizeResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("skip", skip);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds
                }

                int arr = quizItems.size();
                int minus = arr - 1;

                if (currentQuestion == minus) {
//                    linear_for_skip.setVisibility(View.GONE);
                    linear_for_skip.setEnabled(false);
                }
            }
        });

        ly_option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if answer is correct
                progressBar.setVisibility(View.VISIBLE);
                if (quizItems.get(currentQuestion).getAnswer3()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
//                    Toast.makeText(QuizeActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                } else {
                    //wrong
                    wrong++;
//                    Toast.makeText(QuizeActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_red));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            progressDialog.dismiss();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds

                } else {
                    //end result
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            Intent i = new Intent(getApplicationContext(), QuizeResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("skip", skip);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds
                }
                int arr = quizItems.size();
                int minus = arr - 1;

                if (currentQuestion == minus) {
//                    linear_for_skip.setVisibility(View.GONE);
                    linear_for_skip.setEnabled(false);
                }
            }
        });

        ly_option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if answer is correct
                progressBar.setVisibility(View.VISIBLE);
                if (quizItems.get(currentQuestion).getAnswer4()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
//                    Toast.makeText(QuizeActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                } else {
                    //wrong
                    wrong++;
//                    Toast.makeText(QuizeActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
                    ly_option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_red));
                    ly_option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                    ly_option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            progressDialog.dismiss();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds

                } else {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            Intent i = new Intent(getApplicationContext(), QuizeResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("skip", skip);
//                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds
                }
                int arr = quizItems.size();
                int minus = arr - 1;

                if (currentQuestion == minus) {
//                    linear_for_skip.setVisibility(View.GONE);
                    linear_for_skip.setEnabled(false);
                }
            }
        });

        get_mcq_data();
      /*  option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
            }
        });*/


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // yourMethod();
                Collections.shuffle(quizItems);
                //load first Question
                setquestion(currentQuestion);
                progressBar.setVisibility(View.INVISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }, 3000);   //2 seconds


    }

    private void get_mcq_data() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.QuizModel(prefManager.gettokenId(), prefManager.getLoginId(), quize_data);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
             /*   try {
                    progressDialog.dismiss();
                    if (response.code() == 200) {
                        quize_list = new ArrayList<>();
                        quize_list = response.body().getData();
                        Log.e("quize==>", "" + quize_list);
                        for (int i = 0; i < quize_list.size(); i++) {
                            question = quize_list.get(i).getQuestion();
                            txt_question.setText(question);
                            option1.setText(quize_list.get(i).getA());
                            option2.setText(quize_list.get(i).getB());
                            option3.setText(quize_list.get(i).getC());
                            option4.setText(quize_list.get(i).getD());
                            correct = quize_list.get(i).getRightAns();

//                            setquestion(i);
                        }
                    }
                } catch (Exception e) {
                }*/
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String success = jsonObject.getString("status");

                    if (success.equals("1")) {
                        //  Toast.makeText(QuizActivity.this, response, Toast.LENGTH_SHORT).show();
                        JSONArray arr = jsonObject.getJSONArray("data");
                        JSONObject listObj = null;
                        for (int i = 0; i < arr.length(); i++) {
                            listObj = arr.getJSONObject(i);
                            String question = listObj.getString("question");
                            String a = listObj.getString("a");
                            String b = listObj.getString("b");
                            String c = listObj.getString("c");
                            String d = listObj.getString("d");
                            String correct = listObj.getString("right_ans");
                            quizItems.add(new QuizItem(question, a, b, c, d, correct));
                            //Toast.makeText(QuizActivity.this, question+a+b+c+d+correct, Toast.LENGTH_SHORT).show();
                        }

                    } else if (success.equals("0")) {

                    }
                } catch (JSONException e) {

                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }

    private void setquestion(int i) {
        try {
            if (quizItems.size() > 0) {
                question = quizItems.get(i).getQuestion();
                txt_question.setText(question);
                option1.setText(quizItems.get(i).getAnswer1());
                option2.setText(quizItems.get(i).getAnswer2());
                option3.setText(quizItems.get(i).getAnswer3());
                option4.setText(quizItems.get(i).getAnswer4());
            } else {
//                Toast.makeText("")
            }
        } catch (Exception e) {
            Log.e("question==>", "" + e.getMessage());
        }
//        corrects = quizItems.get(i).getRightAns();
    }

    private void init() {
        txt_question = findViewById(R.id.txt_question);
        toolbar_title = findViewById(R.id.toolbar_title);

        option1 = findViewById(R.id.option1);
        option2 = findViewById(R.id.option2);
        option3 = findViewById(R.id.option3);
        option4 = findViewById(R.id.option4);

        linear_for_back = findViewById(R.id.linear_for_back);
        linear_for_skip = findViewById(R.id.linear_for_skip);
        finish_topic_list = findViewById(R.id.finish_topic_list);

        ly_option1 = findViewById(R.id.ly_option1);
        ly_option2 = findViewById(R.id.ly_option2);
        ly_option3 = findViewById(R.id.ly_option3);
        ly_option4 = findViewById(R.id.ly_option4);

        progressBar = findViewById(R.id.quiz_progress);
    }

    public void btnBg() {
        ly_option1.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
        ly_option2.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
        ly_option3.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
        ly_option4.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
    }


}
