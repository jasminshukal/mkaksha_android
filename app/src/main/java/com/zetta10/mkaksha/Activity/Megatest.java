package com.zetta10.mkaksha.Activity;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nishant.math.MathView;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.iwgang.countdownview.CountdownView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Megatest extends AppCompatActivity {

    TextView option1, option2, option3, option4;
    LinearLayout linear_for_back, linear_for_skip, finish_topic_list, ly_option1, ly_option2, ly_option3, ly_option4;
    TextView txt_question, toolbar_title;
    PrefManager prefManager;
    public static ProgressDialog progressDialog;
    public static List<QuizItem> quizItems;
    String quize_data, question, corrects;
    public static String duration_test;
    public static int correct = 0, wrong = 0, attempt_question = 0;
    int currentQuestion = 0, skip;
    ProgressBar progressBar;
    static String user_id, login_token;
    public static CountdownView countdownView;
    String duration;
    public static int milisecond_duration;
    public static String test_id;
    public static int hour, minute, sec;
    public static long remain_time, remain_minute;
    public static long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds, different, different1;
    public static long secondsInMilli, minutesInMilli, hoursInMilli, daysInMilli;
    public static int test_time;
    public static String temp_day, temp_hour = "0", temp_minute = "0", temp_sec = "0", is_gift, gift_desc;
    public static Dialog dialog;
    RecyclerView recyclerview_for_megatest;
    public static Context context;
    Application application;
    public static Megatest megatest;
    static Resources res;
    static float fontSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_megatest);
        res = getResources();
        countdownView = findViewById(R.id.countdownview);
        recyclerview_for_megatest = findViewById(R.id.recyclerview_for_megatest);
        megatest = this;
        context = Megatest.this;
        correct = 0;
        attempt_question = 0;
        wrong = 0;
        elapsedDays = 0;
        elapsedHours = 0;
        elapsedMinutes = 0;
        elapsedSeconds = 0;
        temp_hour = "0";
        temp_minute = "0";
        temp_sec = "0";
        test_time = 0;
        secondsInMilli = 0;
        minutesInMilli = 0;
        hoursInMilli = 0;
        daysInMilli = 0;
        different = 0;
        different1 = 0;
        remain_time = 0;
        remain_minute = 0;
        milisecond_duration = 0;
        init();
        Intent intent = getIntent();
        duration = intent.getStringExtra("duration");
        test_id = intent.getStringExtra("test_id");
        is_gift = intent.getStringExtra("is_gift");
        gift_desc = intent.getStringExtra("gift_desc");

        if (is_gift.equals("1")) {

            Dialog dialog = new Dialog(Megatest.this);
            dialog.setContentView(R.layout.layout_for_reaward_des);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
            WebView webView = dialog.findViewById(R.id.webview_for_gift);
            webView.loadData(gift_desc, "text/html", "UTF-8");
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    test_time = Integer.parseInt(duration);
                    milisecond_duration = test_time * 60000;
                    countdownView.start(milisecond_duration);
                }
            });
            dialog.show();
        }
        if (is_gift.equals("0")) {

            test_time = Integer.parseInt(duration);
            milisecond_duration = test_time * 60000;
            countdownView.start(milisecond_duration);
        }
        /* duration_test = (05 + ":" + 00 + ":" + 00);*/
        countdownView.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
            @Override
            public void onEnd(CountdownView cv) {

                elapsedDays = 0;
                elapsedSeconds = 0;
                elapsedMinutes = 0;
                elapsedHours = 0;
                temp_hour = "0";
                temp_minute = "0";
                temp_sec = "0";

                different = milisecond_duration;
                secondsInMilli = 1000;
                minutesInMilli = secondsInMilli * 60;
                hoursInMilli = minutesInMilli * 60;
                daysInMilli = hoursInMilli * 24;

                elapsedDays = different / daysInMilli;
                different = different % daysInMilli;

                elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;

                elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;

                elapsedSeconds = different / secondsInMilli;
                if (elapsedDays != 0) {
                    temp_day = String.valueOf(elapsedDays);
                }
                if (elapsedHours != 0) {
                    temp_hour = String.valueOf(elapsedHours);
                }
                if (elapsedMinutes != 0) {
                    temp_minute = String.valueOf(elapsedMinutes);
                }
                if (elapsedSeconds != 0) {
                    temp_sec = String.valueOf(elapsedSeconds);
                }
                duration_test = (temp_hour + ":" + temp_minute + ":" + temp_sec);
                //  Toast.makeText(getApplicationContext(), ""+duration_test, Toast.LENGTH_SHORT).show();
                submit_test(/*getApplication(),*/ context, megatest);
            }
        });

        prefManager = new PrefManager(Megatest.this);
        login_token = prefManager.gettokenId();
        user_id = prefManager.getLoginId();
        progressDialog = new ProgressDialog(Megatest.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        quizItems = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            quize_data = bundle.getString("quize_data");
            Log.e("quize_data", "" + quize_data);
        }

       /* linear_for_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Megatest.this.finish();
            }
        });*/

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Megatest.this)
                        .setMessage("Are you sure you want to exit This Test")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Megatest.super.onBackPressed();
                            }
                        }).setNegativeButton("No", null)
                        .show();
            }
        });
        get_test_data();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit This Test")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Megatest.super.onBackPressed();
                    }
                }).setNegativeButton("No", null)
                .show();
    }

    private void init() {
        txt_question = findViewById(R.id.txt_question);
        toolbar_title = findViewById(R.id.toolbar_title);

        option1 = findViewById(R.id.option1);
        option2 = findViewById(R.id.option2);
        option3 = findViewById(R.id.option3);
        option4 = findViewById(R.id.option4);

        linear_for_back = findViewById(R.id.linear_for_back);
        linear_for_skip = findViewById(R.id.linear_for_skip);
        finish_topic_list = findViewById(R.id.finish_topic_list);

        ly_option1 = findViewById(R.id.ly_option1);
        ly_option2 = findViewById(R.id.ly_option2);
        ly_option3 = findViewById(R.id.ly_option3);
        ly_option4 = findViewById(R.id.ly_option4);

        progressBar = findViewById(R.id.quiz_progress);
    }

    private void get_test_data() {
        try {
            progressDialog.show();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<ResponseBody> call_test_detail = apiinterface.get_test_detail(prefManager.gettokenId(), prefManager.getLoginId(), test_id);
            call_test_detail.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        // progressDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String success = jsonObject.getString("status");
                        if (success.equals("1")) {
                            JSONArray arr = jsonObject.getJSONArray("data");
                            JSONObject listObj = null;
                            for (int i = 0; i < arr.length(); i++) {
                                listObj = arr.getJSONObject(i);
                                String question = listObj.getString("question");
                                String a = listObj.getString("a");
                                String b = listObj.getString("b");
                                String c = listObj.getString("c");
                                String d = listObj.getString("d");
                                String correct = listObj.getString("right_ans");
                                quizItems.add(new QuizItem(question, a, b, c, d, correct));
                            }
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerview_for_megatest.setLayoutManager(layoutManager);

                            Adapter_for_mega_test adapter_for_mega_test = new Adapter_for_mega_test(/*getApplication(),*/ context, megatest);
                            recyclerview_for_megatest.setAdapter(adapter_for_mega_test);
                            progressDialog.dismiss();

                        } else if (success.equals("0")) {

                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
        }

    }

    public static void printDifference(long test_dur, long test_time, Context context) {

        //milliseconds
        elapsedDays = 0;
        elapsedSeconds = 0;
        elapsedMinutes = 0;
        elapsedHours = 0;
        temp_hour = "0";
        temp_minute = "0";
        temp_sec = "0";

        different = test_dur - test_time;
        different1 = test_dur - test_dur;


        secondsInMilli = 1000;
        minutesInMilli = secondsInMilli * 60;
        hoursInMilli = minutesInMilli * 60;
        daysInMilli = hoursInMilli * 24;

        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;

        if (elapsedDays != 0) {
            temp_day = String.valueOf(elapsedDays);
        }
        if (elapsedHours != 0) {
            temp_hour = String.valueOf(elapsedHours);
        }
        if (elapsedMinutes != 0) {
            temp_minute = String.valueOf(elapsedMinutes);
        }
        if (elapsedSeconds != 0) {
            temp_sec = String.valueOf(elapsedSeconds);
        }
        duration_test = (temp_hour + ":" + temp_minute + ":" + temp_sec);
        //  Toast.makeText(context, ""+duration_test, Toast.LENGTH_SHORT).show();
    }

    public static void submit_test(/*Application application,*/ Context context, Megatest megatest) {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> call_submit_test = apiinterface.submit_test(login_token, user_id, test_id, duration_test, correct, wrong, attempt_question);

        call_submit_test.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(context, "" + msg, Toast.LENGTH_LONG).show();
                        JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                        String coin = jsonObject_data.getString("coin");
                        if (!coin.equals("0")) {
                            dialog = new Dialog(context);
                            dialog.setContentView(R.layout.coin_dialog);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            TextView txt_coin = dialog.findViewById(R.id.txt_coin);
                            txt_coin.setText("You Earn" + coin + "Coins");

                            ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
                            dialog.show();
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                        }
                        progressDialog.dismiss();
                        megatest.finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public static void getremaintime(Context context) {

        sec = countdownView.getSecond();
        remain_time = countdownView.getRemainTime();
        remain_minute = remain_time / 60000;
        milisecond_duration = test_time * 60000;
        printDifference(milisecond_duration, remain_time, context);
    }

}

class Adapter_for_mega_test extends RecyclerView.Adapter<Adapter_for_mega_test.Holder> {


    String quize_data, question, corrects;
    public static String duration_test;
    public static int correct = 0, wrong = 0, attempt_question = 0;
    // CountdownView countdownView;
    // String duration;
    int milisecond_duration;
    int hour, minute, sec;
    long remain_time, remain_minute;
    public long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds, different, different1;
    public long secondsInMilli, minutesInMilli, hoursInMilli, daysInMilli;
    int test_time;
    String temp_day, temp_hour = "0", temp_minute = "0", temp_sec = "0", is_gift, gift_desc;
    public Application context;
    Megatest megatest;
    Context context_activity;
    String question_url, opt1_url, opt2_url, opt3_url, opt4_url;

    public Adapter_for_mega_test(/*Application context,*/ Context context_activity, Megatest megatest) {
        this.context = context;
        this.context_activity = context_activity;
        this.megatest = megatest;
    }

    @NonNull
    @Override
    public Adapter_for_mega_test.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_megatest, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_for_mega_test.Holder holder, int position) {
        int q_no = position + 1;

        String question = "Q." + q_no + "] " + Megatest.quizItems.get(position).getQuestion();
        String question_data = question.replaceAll("(?<=\\{).*?(?=\\})", "");
        String final_question = question_data.replaceAll("\\{\\}\\}", "");
        holder.txt_question.setText(final_question);

        String op1 = Megatest.quizItems.get(position).getAnswer1();
        String op1_data = op1.replaceAll("(?<=\\{).*?(?=\\})", "");
        String final_op1 = op1_data.replaceAll("\\{\\}\\}", "");

        String op2 = Megatest.quizItems.get(position).getAnswer2();
        String op2_data = op2.replaceAll("(?<=\\{).*?(?=\\})", "");
        String final_op2 = op2_data.replaceAll("\\{\\}\\}", "");

        String op3 = Megatest.quizItems.get(position).getAnswer3();
        String op3_data = op3.replaceAll("(?<=\\{).*?(?=\\})", "");
        String final_op3 = op3_data.replaceAll("\\{\\}\\}", "");

        String op4 = Megatest.quizItems.get(position).getAnswer4();
        String op4_data = op4.replaceAll("(?<=\\{).*?(?=\\})", "");
        String final_op4 = op4_data.replaceAll("\\{\\}\\}", "");

        holder.text_op1.setText("A. ");
        holder.text_op2.setText("B. ");
        holder.text_op3.setText("C. ");
        holder.text_op4.setText("D. ");

        holder.option1.setText(final_op1);
        holder.option2.setText(final_op2);
        holder.option3.setText(final_op3);
        holder.option4.setText(final_op4);

        String answer1 = Megatest.quizItems.get(position).getAnswer1();
        String answer2 = Megatest.quizItems.get(position).getAnswer2();
        String answer3 = Megatest.quizItems.get(position).getAnswer3();
        String answer4 = Megatest.quizItems.get(position).getAnswer4();

        String correct_ans = Megatest.quizItems.get(position).getCorrect();

        final WebSettings webSettings = holder.txt_question.getSettings();
        Megatest.fontSize = Megatest.res.getDimension(R.dimen.fontsize);
        webSettings.setDefaultFontSize((int) Megatest.fontSize);

        String exp = "\\{\\{(.*?)\\}\\}";
        String value = question;

        Pattern pattern = Pattern.compile(exp);
        Matcher matcher = pattern.matcher(value);

        List<String> question_matches = new ArrayList<String>();
        question_url = "";
        while (matcher.find()) {
            String group = matcher.group();
            question_url = group.substring(2, group.length() - 2);
            question_matches.add(question_url);
        }
        String op1_value = answer1;

        Matcher matcher1 = pattern.matcher(op1_value);

        List<String> op1_matches = new ArrayList<String>();
        opt1_url = "";
        while (matcher1.find()) {
            String group = matcher1.group();
            opt1_url = group.substring(2, group.length() - 2);
            op1_matches.add(opt1_url);
        }
        String op2_value = answer2;

        Matcher matcher2 = pattern.matcher(op2_value);

        List<String> op2_matches = new ArrayList<String>();
        opt2_url = "";
        while (matcher2.find()) {
            String group = matcher2.group();
            opt2_url = group.substring(2, group.length() - 2);
            op2_matches.add(opt2_url);
        }
        String op3_value = answer3;

        Matcher matcher3 = pattern.matcher(op3_value);

        List<String> op3_matches = new ArrayList<String>();
        opt3_url = "";
        while (matcher3.find()) {
            String group = matcher3.group();
            opt3_url = group.substring(2, group.length() - 2);
            op3_matches.add(opt3_url);
        }
        String op4_value = answer4;

        Matcher matcher4 = pattern.matcher(op4_value);

        List<String> op4_matches = new ArrayList<String>();
        opt4_url = "";
        while (matcher4.find()) {
            String group = matcher4.group();
            opt4_url = group.substring(2, group.length() - 2);
            op4_matches.add(opt4_url);
        }

        if (!question_url.equals("")) {
            holder.image_for_question_figure.setVisibility(View.VISIBLE);
            Glide.with(context_activity).load(question_url).into(holder.image_for_question_figure);
        }
        if (!opt1_url.equals("")) {
            holder.image_for_op1_figure.setVisibility(View.VISIBLE);
            Glide.with(context_activity).load(opt1_url).into(holder.image_for_op1_figure);
        }
        if (!opt2_url.equals("")) {
            holder.image_for_op2_figure.setVisibility(View.VISIBLE);
            Glide.with(context_activity).load(opt2_url).into(holder.image_for_op2_figure);
        }
        if (!opt3_url.equals("")) {
            holder.image_for_op3_figure.setVisibility(View.VISIBLE);
            Glide.with(context_activity).load(opt3_url).into(holder.image_for_op3_figure);
        }
        if (!opt4_url.equals("")) {
            holder.image_for_op4_figure.setVisibility(View.VISIBLE);
            Glide.with(context_activity).load(opt4_url).into(holder.image_for_op4_figure);
        }
        // String[] groups = matches.toArray(new String[matches.size()]);
        // System.out.println(Arrays.toString(groups));
        // holder.txt_question.loadDataWithBaseURL();
       /* String message ="<font color='white'>"+"<u>"+"text in white"+ "<br>" +"<font color='blue'>"+"<font size='8'>"+question+"</font>";
        holder.txt_question.loadData(message,"text/html", "utf8");*/
        if (Megatest.quizItems.size() == position + 1) {
            holder.textview_for_submit.setVisibility(View.VISIBLE);
        } else {
            holder.textview_for_submit.setVisibility(View.GONE);
        }
        holder.textview_for_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Megatest.getremaintime(context_activity);
                Megatest.submit_test(/*context,*/ context_activity, megatest);
                //   Toast.makeText(context_activity, "correct :"+Megatest.correct+"Wrong :"+Megatest.wrong+"Attempt :"+Megatest.attempt_question, Toast.LENGTH_SHORT).show();
            }
        });

        holder.ly_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ly_option1.setBackgroundResource(R.drawable.test_option_bg);
                holder.ly_option2.setBackgroundResource(0);
                holder.ly_option3.setBackgroundResource(0);
                holder.ly_option4.setBackgroundResource(0);
                holder.ly_option1.setEnabled(false);
                holder.ly_option2.setEnabled(false);
                holder.ly_option3.setEnabled(false);
                holder.ly_option4.setEnabled(false);

                Megatest.attempt_question++;
                if (Megatest.quizItems.get(position).getAnswer1().equals(correct_ans)) {
                    Megatest.correct++;
                } else {
                    Megatest.wrong++;
                }
            }
        });
        holder.ly_option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ly_option2.setBackgroundResource(R.drawable.test_option_bg);
                holder.ly_option3.setBackgroundResource(0);
                holder.ly_option4.setBackgroundResource(0);
                holder.ly_option1.setBackgroundResource(0);
                holder.ly_option1.setEnabled(false);
                holder.ly_option2.setEnabled(false);
                holder.ly_option3.setEnabled(false);
                holder.ly_option4.setEnabled(false);

                Megatest.attempt_question++;
                if (Megatest.quizItems.get(position).getAnswer2().equals(correct_ans)) {
                    Megatest.correct++;
                } else {
                    Megatest.wrong++;
                }
            }
        });
        holder.ly_option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ly_option3.setBackgroundResource(R.drawable.test_option_bg);
                holder.ly_option1.setBackgroundResource(0);
                holder.ly_option2.setBackgroundResource(0);
                holder.ly_option4.setBackgroundResource(0);
                holder.ly_option1.setEnabled(false);
                holder.ly_option2.setEnabled(false);
                holder.ly_option3.setEnabled(false);
                holder.ly_option4.setEnabled(false);

                Megatest.attempt_question++;
                if (Megatest.quizItems.get(position).getAnswer3().equals(correct_ans)) {
                    Megatest.correct++;
                } else {
                    Megatest.wrong++;
                }
            }
        });
        holder.ly_option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ly_option4.setBackgroundResource(R.drawable.test_option_bg);
                holder.ly_option3.setBackgroundResource(0);
                holder.ly_option2.setBackgroundResource(0);
                holder.ly_option1.setBackgroundResource(0);
                holder.ly_option1.setEnabled(false);
                holder.ly_option2.setEnabled(false);
                holder.ly_option3.setEnabled(false);
                holder.ly_option4.setEnabled(false);

                Megatest.attempt_question++;
                if (Megatest.quizItems.get(position).getAnswer4().equals(correct_ans)) {
                    Megatest.correct++;
                } else {
                    Megatest.wrong++;
                }
            }
        });
    }
    /* countdownView.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener()
    {
        @Override
        public void onEnd (CountdownView cv){
        different = milisecond_duration;
        secondsInMilli = 1000;
        minutesInMilli = secondsInMilli * 60;
        hoursInMilli = minutesInMilli * 60;
        daysInMilli = hoursInMilli * 24;

        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;
        if (elapsedDays != 0) {
            temp_day = String.valueOf(elapsedDays);
        }
        if (elapsedHours != 0) {
            temp_hour = String.valueOf(elapsedHours);
        }
        if (elapsedMinutes != 0) {
            temp_minute = String.valueOf(elapsedMinutes);
        }
        if (elapsedSeconds != 0) {
            temp_sec = String.valueOf(elapsedSeconds);
        }
        duration_test = (temp_hour + ":" + temp_minute + ":" + temp_sec);

        submit_test();
    }
    });*/
    /*public static void submit_test() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> call_submit_test = apiinterface.submit_test(login_token, user_id, test_id, duration_test, correct, wrong, attempt_question);

        call_submit_test.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(Megatest.this, "" + msg, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                        String coin = jsonObject_data.getString("coin");
                        if (!coin.equals("0")) {
                            dialog = new Dialog(Megatest.this);
                            dialog.setContentView(R.layout.coin_dialog);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            TextView txt_coin = dialog.findViewById(R.id.txt_coin);
                            txt_coin.setText("you Earn" + coin + "Coins");

                            ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
                            dialog.show();
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                        }
                        // finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }*/

    @Override
    public int getItemCount() {

        if (Megatest.quizItems != null) {
            return Megatest.quizItems.size();
        } else {
            return 0;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        MathView txt_question;
        TextView option1, option2, option3, option4, textview_for_submit, text_op1, text_op2, text_op3, text_op4;
        LinearLayout ly_option1,ly_option2,ly_option3,ly_option4;
        ImageView image_for_question_figure, image_for_op1_figure, image_for_op2_figure, image_for_op3_figure, image_for_op4_figure;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_question = itemView.findViewById(R.id.txt_question);

            option1 = itemView.findViewById(R.id.option1);
            option2 = itemView.findViewById(R.id.option2);
            option3 = itemView.findViewById(R.id.option3);
            option4 = itemView.findViewById(R.id.option4);

            text_op1 = itemView.findViewById(R.id.text_op1);
            text_op2 = itemView.findViewById(R.id.text_op2);
            text_op3 = itemView.findViewById(R.id.text_op3);
            text_op4 = itemView.findViewById(R.id.text_op4);

            ly_option1 = itemView.findViewById(R.id.ly_option1);
            ly_option2 = itemView.findViewById(R.id.ly_option2);
            ly_option3 = itemView.findViewById(R.id.ly_option3);
            ly_option4 = itemView.findViewById(R.id.ly_option4);

            textview_for_submit = itemView.findViewById(R.id.textview_for_submit);

            image_for_question_figure = itemView.findViewById(R.id.image_for_figure);
            image_for_op1_figure = itemView.findViewById(R.id.image_for_op1_figure);
            image_for_op2_figure = itemView.findViewById(R.id.image_for_op2_figure);
            image_for_op3_figure = itemView.findViewById(R.id.image_for_op3_figure);
            image_for_op4_figure = itemView.findViewById(R.id.image_for_op4_figure);
        }
    }
}
