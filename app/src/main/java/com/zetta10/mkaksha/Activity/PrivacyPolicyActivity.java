package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Model.PrivacyModel.PrivacyModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyPolicyActivity extends AppCompatActivity {

    LinearLayout finish_topic_list;
    TextView privacy_policy;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    WebView web_data;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.privacy_policy_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        finish_topic_list = findViewById(R.id.finish_topic_list);
//        privacy_policy = findViewById(R.id.privacy_policy);
        web_data = findViewById(R.id.web_data);
        prefManager = new PrefManager(PrivacyPolicyActivity.this);
        progressDialog = new ProgressDialog(PrivacyPolicyActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        privacy_policy_data();
    }

    private void privacy_policy_data() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<PrivacyModel> call = bookNPlayAPI.GetPolicy(prefManager.gettokenId(), prefManager.getLoginId());
        call.enqueue(new Callback<PrivacyModel>() {
            @Override
            public void onResponse(Call<PrivacyModel> call, Response<PrivacyModel> response) {
//                Toast.makeText(PrivacyPolicyActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                view_comments();
                progressDialog.dismiss();
//                web_data.setBackgroundResource(R.drawable.chapterlist_bg);
                web_data.setBackgroundColor(Color.TRANSPARENT);
                final WebSettings webSettings = web_data.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.SMALLEST);
                webSettings.setDefaultFontSize(30);//Larger number means larger font size

                String data = response.body().getData();
                Log.e("web_data==>", "" + data);
                web_data.loadData(data, "text/html", "UTF-8");
            }

            @Override
            public void onFailure(Call<PrivacyModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
