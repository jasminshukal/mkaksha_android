package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.zetta10.mkaksha.Model.MySubjectWiseModel.Chapter;
import com.zetta10.mkaksha.Model.MySubjectWiseModel.MySubjectWiseModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Subject_wise_Journey_Activity extends AppCompatActivity {
    private BarChart chart;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    List<BarEntry> entries;
    ArrayList<String> xAxisLables;
    List<Chapter> subject_list;
    String sub_name, status;
    SeekBar seekBar;
    float percentage, all_percentage;
    Integer c_id;
    String subject_name;
    TextView txt_all_subject_percentage;
    LinearLayout finish_ch_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.subject_wise__journey_activity);
        prefManager = new PrefManager(Subject_wise_Journey_Activity.this);
        progressDialog = new ProgressDialog(Subject_wise_Journey_Activity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        finish_ch_list = findViewById(R.id.finish_ch_list);
        chart = findViewById(R.id.chart2);
        txt_all_subject_percentage = findViewById(R.id.txt_all_subject_percentage);
        seekBar = findViewById(R.id.seekBar2);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            c_id = bundle.getInt("chapter_id");
            subject_name = bundle.getString("subject");
            //  payment_array_size = bundle.getString("payment_data_size");
            Log.e("chapter_id", "" + c_id);
            getdata();
//            toolbar_title.setText(subject_name);
        }

        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);
        chart.setTouchEnabled(false);
        chart.setPinchZoom(false);
        chart.setDoubleTapToZoomEnabled(false);
        chart.getAxisRight().setEnabled(false);
        entries = new ArrayList<>();
        xAxisLables = new ArrayList<String>();
        chart.getDescription().setEnabled(false);
        chart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawGridBackground(false);
        // chart.setDrawYLabels(false);

        YAxis yAxis = chart.getAxis(YAxis.AxisDependency.LEFT);
//        yAxis.mAxisMinimum = 1;
        yAxis.setStartAtZero(true);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getdata() {
        try {
            progressDialog.show();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<MySubjectWiseModel> call = appAPI.MySubjectWise(prefManager.gettokenId(), prefManager.getLoginId(), c_id);
            call.enqueue(new Callback<MySubjectWiseModel>() {
                @Override
                public void onResponse(Call<MySubjectWiseModel> call, Response<MySubjectWiseModel> response) {
                    subject_list = new ArrayList<>();
                    if (response.isSuccessful()) {
                        subject_list = response.body().getData().getChapters();
                        Log.e("data1-", "" + response.body());
                        Log.e("data2-", "" + response.body().getData());
                        Log.e("data3-", "" + response.body().getData().getYourLaval());
//                        Log.e("data4-", "" + response.body().getData().getChapters().get(0).getPersontage());
//                        Log.e("data5-", "" + subject_list.size());
                        seekBar.getThumb().mutate().setAlpha(0);
                        all_percentage = response.body().getData().getYourLaval();
                        txt_all_subject_percentage.setText("" + all_percentage);
                        seekBar.setProgress((int) all_percentage);
                        for (int i = 0; i < subject_list.size(); i++) {
                            percentage = response.body().getData().getChapters().get(i).getPersontage();

                            sub_name = response.body().getData().getChapters().get(i).getName();
//                            String upToNCharacters = sub_name.substring(0, Math.min(sub_name.length(), 4));
//                            xAxisLables.add(upToNCharacters);
                            xAxisLables.add(sub_name);


                            entries.add(new BarEntry(i, percentage));
                        }
                        Log.e("==>sub_name", "" + xAxisLables);
                        chart.setExtraBottomOffset(50);
                        XAxis xAxis = chart.getXAxis();
//                        xAxis.setLabelRotationAngle(-80);
                        xAxis.setGranularity(1f);
                        xAxis.setGranularityEnabled(true);
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.mLabelRotatedWidth = 10;
                        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLables));
                        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLables));

                        BarDataSet set = new BarDataSet(entries, "Video parentage");
                        BarData data = new BarData(set);
                        data.setBarWidth(0.2f);
                        xAxis.setAxisMinimum(data.getXMin() - .2f);
                        xAxis.setAxisMaximum(data.getXMax() + .2f);
                        xAxis.setSpaceMax(0.1f);
                        xAxis.setSpaceMin(0.1f);
//                        chart.setVisibleXRangeMaximum(1);
                        chart.setVisibleXRangeMinimum(entries.size());
                        chart.moveViewToX(1);
                        chart.setData(data);
                        chart.setFitBars(true); // make the x-axis fit exactly all bars
                        chart.notifyDataSetChanged();
                        chart.invalidate();// refresh
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<MySubjectWiseModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }
    }
}
