package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    EditText editText_newpassword, editText_con_pass;
    LinearLayout linearLayout_submit;
    String login_token, user_id, new_password, new_con_pass;
    PrefManager prefManager;
    String status;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.change_password_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        prefManager = new PrefManager(getApplicationContext());
        login_token = prefManager.getValue("api_key");
        user_id = prefManager.getValue("user_id");
        editText_newpassword = findViewById(R.id.edit_new_password);
        editText_con_pass = findViewById(R.id.edit_con_pass);
        linearLayout_submit = findViewById(R.id.linear_submit);
        progressDialog = new ProgressDialog(ChangePasswordActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        linearLayout_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new_password = editText_newpassword.getText().toString().trim();
                new_con_pass = editText_con_pass.getText().toString().trim();

                if (TextUtils.isEmpty(new_password)) {
                    Toast.makeText(ChangePasswordActivity.this, "You Must Enter The New Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(new_con_pass)) {
                    Toast.makeText(ChangePasswordActivity.this, "You Must Enter The Confirm Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!new_password.equals(new_con_pass)) {
                    Toast.makeText(ChangePasswordActivity.this, "Password does not match", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (new_password.length() < 8) {
                    Toast.makeText(ChangePasswordActivity.this, "Password must be 8 Character", Toast.LENGTH_SHORT).show();
                    return;
                }
                change_password_api();
            }
        });

    }

    public void change_password_api() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> change_password_call = apiinterface.changepassword(login_token, user_id, new_con_pass);

        change_password_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
                    status = jsonObject.getString("status");
                    Toast.makeText(ChangePasswordActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                    if (status.equals("3")) {
                        Intent intent = new Intent(getApplicationContext(), PopupActivity.class);
                        intent.putExtra("status", status);
                        startActivity(intent);
                        //   Toast.makeText(LoginActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                    }
                    if (status.equals("4")) {
                        Intent intent = new Intent(getApplicationContext(), PopupActivity.class);
                        intent.putExtra("status", status);
                        startActivity(intent);
                        //   Toast.makeText(LoginActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                    }

                    if (status.equals("1")) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
