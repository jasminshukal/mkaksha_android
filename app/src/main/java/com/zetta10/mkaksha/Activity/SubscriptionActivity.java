package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Adapter.SubsriptionListAdapter;
import com.zetta10.mkaksha.Model.PackagesListModel.Data;
import com.zetta10.mkaksha.Model.PackagesListModel.PackagesListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionActivity extends AppCompatActivity {

    RecyclerView rv_premium_list;
    String api_token, user_id;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    List<Data> subscription_dataList;
    SubsriptionListAdapter subsriptionListAdapter;
    LinearLayout finish_ch_list;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.subscription_activity);
        prefManager = new PrefManager(SubscriptionActivity.this);
        progressDialog = new ProgressDialog(SubscriptionActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });


        init();
        subscription_list();

        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init() {
        rv_premium_list = findViewById(R.id.rv_premium_list);
        finish_ch_list = findViewById(R.id.finish_ch_list);

    }

    private void subscription_list() {
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<PackagesListModel> call = appAPI.PackagesList_list_call(api_token, user_id);
            call.enqueue(new Callback<PackagesListModel>() {
                @Override
                public void onResponse(Call<PackagesListModel> call, Response<PackagesListModel> response) {
                    if (response.isSuccessful()) {

                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        subscription_dataList = new ArrayList<>();
                        subscription_dataList = response.body().getData();
                        Log.e("dataList==>", "" + subscription_dataList.size());
                        if (subscription_dataList.size() > 0) {
                            subsriptionListAdapter = new SubsriptionListAdapter(SubscriptionActivity.this, subscription_dataList);
                            rv_premium_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SubscriptionActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_premium_list.setLayoutManager(mLayoutManager3);
                            rv_premium_list.setItemAnimator(new DefaultItemAnimator());
                            rv_premium_list.setAdapter(subsriptionListAdapter);
                            subsriptionListAdapter.notifyDataSetChanged();
                            rv_premium_list.setVisibility(View.VISIBLE);
                            rv_premium_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_premium_list.setVisibility(View.GONE);
//                            ly_paid_book.setVisibility(View.GONE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<PackagesListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if ((progressDialog != null) && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
