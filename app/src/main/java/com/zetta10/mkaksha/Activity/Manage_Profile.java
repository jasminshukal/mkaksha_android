package com.zetta10.mkaksha.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Manage_Profile extends AppCompatActivity {

    CircleImageView image_for_profile;
    LinearLayout linear_for_basic_info, linear_for_per_info, linear_for_guard_info, linear_for_edu_det, linear_for_add;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    String login_token, user_id, image_url;
    private String downloadImageUrl;
    final static int Gallery_pic = 1;
    File file_front;
    LinearLayout finish_ch_list;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_manage__profile);

        progressDialog = new ProgressDialog(Manage_Profile.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getApplicationContext());
        login_token = prefManager.gettokenId();
        user_id = prefManager.getLoginId();

        linear_for_basic_info = findViewById(R.id.linear_for_basic_info);
        finish_ch_list = findViewById(R.id.finish_ch_list);
        linear_for_per_info = findViewById(R.id.linear_for_per_info);
        linear_for_guard_info = findViewById(R.id.linear_for_guard_info);
        linear_for_edu_det = findViewById(R.id.linear_for_edu_det);
        linear_for_add = findViewById(R.id.linear_for_add);
        image_for_profile = findViewById(R.id.image_for_profile);
        image_for_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //  galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                // galleryIntent.setType("image/*");
                //   startActivityForResult(galleryIntent, Gallery_pic);
                startActivity(galleryIntent);*/
//                selectImage();
/*
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent,Gallery_pic);*/
//                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(Manage_Profile.this);
//                CropImage.activity().setAspectRatio(1, 1);
//                CropImage.activity().setFixAspectRatio(true);

                CropImage.activity().setAspectRatio(1, 1)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(Manage_Profile.this);
            }
        });
        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        linear_for_basic_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BasicInformation.class);
                startActivity(intent);
            }
        });

        linear_for_per_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PersonalInformation.class);
                startActivity(intent);
            }
        });

        linear_for_guard_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GuardianInformation.class);
                startActivity(intent);
            }
        });

        linear_for_edu_det.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EducationInformation.class);
                startActivity(intent);
            }
        });

        linear_for_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddressInformation.class);
                startActivity(intent);
            }
        });
        get_profile_detail();

        isStoragePermissionGranted();
    }

    private void selectImage() {
//        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        final CharSequence[] options = {"Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Manage_Profile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("TAG", "Permission is granted");
                return true;
            } else {

                Log.e("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.e("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      /*  if (resultCode == RESULT_OK) {

            if (requestCode == 1) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = Manage_Profile.this.getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                thumbnail = getResizedBitmap(thumbnail, 300, 300);
                String param_img = encodeImage(thumbnail);
                Log.e("imgs", "" + thumbnail);
                Log.e("ten_param_img", "" + param_img);

//                image_for_profile.setImageBitmap(param_img);
//                image_for_profile.setImageURI(selectedImage);
                Picasso.get().load(selectedImage).resize(300, 300).into(image_for_profile);

                File file = new File(getRealPathFromURI(selectedImage));
                Log.e("file", file + "");

                try {
                    file_front = savebitmap(thumbnail);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                AddProfileImg();
            }
        }*/


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            CropImage.activity().setAspectRatio(1, 1).start(Manage_Profile.this);
            if (resultCode == RESULT_OK) {
                Uri selectedImage = result.getUri();
             /*   String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = Manage_Profile.this.getContentResolver().query(selectedImage, filePath, null, null, null);
//                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();*/
                try {
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
//                    storeImage(thumbnail, selectedImage);
                    thumbnail = getResizedBitmap(thumbnail, 300, 300);
                    String param_img = encodeImage(thumbnail);
                    Log.e("imgs", "" + thumbnail);
                    Log.e("ten_param_img", "" + param_img);
                    Picasso.get().load(selectedImage).resize(300, 300).into(image_for_profile);

                    File file = new File(getRealPathFromURI(selectedImage));
                    Log.e("file", file + "");


//                        file = savebitmap(thumbnail);

               /*     try {
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        image.compress(Bitmap.CompressFormat.PNG, 90, fos);
                        fos.close();
                    } catch (FileNotFoundException e) {
                        Log.e("", "File not found: " + e.getMessage());
                    } catch (IOException e) {
                        Log.e("TAG", "Error accessing file: " + e.getMessage());
                    }*/


                    file_front = savebitmap(thumbnail);
//                    file_front = bitmapToFile(Manage_Profile.this, thumbnail, "test");
                    Log.e("file==>", "" + file_front);

//                        file_front = imagefile;
                    try {
                        AddProfileImg();
                    } catch (Exception e) {
                        Log.e("error==>", "" + e.getMessage());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("error==>", "" + error);
            }
        }


    }

    public static File savebitmap(Bitmap bmp) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "testimage.jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }


    private String encodeImage(Bitmap selectedImage) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = Manage_Profile.this.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void AddProfileImg() {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file_front);
        Log.e("===>", "" + requestFile);

//        RequestBody requestFile = RequestBody.create(file_front, MediaType.parse("image/*"));
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", file_front.getName(), requestFile);

        RequestBody id =
                RequestBody.create(MediaType.parse("text/plain"), "" + prefManager.getLoginId());

        RequestBody token =
                RequestBody.create(MediaType.parse("text/plain"), "" + prefManager.gettokenId());

        Log.e("p_id", "" + prefManager.getLoginId());
        Log.e("token_id", "" + prefManager.gettokenId());
        Log.e("token_id==>", "" + login_token);
        Log.e("id==>", "" + id);
        Log.e("body==>", "" + body);
        Log.e("p_id", "" + prefManager.gettokenId());
        progressDialog.show();

        Apiinterface bookNPlayAPI = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> call = bookNPlayAPI.student_prof_info(token, id, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                try {
                    Log.e("response==>", "" + response.body());
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
                    Log.e("response", "" + jsonObject);
                    if (response.code() == 200) {
                        Toast.makeText(Manage_Profile.this, "" + msg, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject_data = jsonObject.getJSONObject("data");
//                        JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                        image_url = jsonObject_data.getString("profile_img");
                        Log.e("set_server==>", "" + image_url);
                        prefManager.setValue("image_urls", "" + image_url);
                        finish(); //finish Activity.
                    } else {
                        // Toast.makeText(Manage_Profile.this, "", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Throwable", "" + t.getMessage());
                progressDialog.dismiss();
            }
        });
    }


    public void get_profile_detail() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(user_id, login_token);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(AddressInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                    image_url = jsonObject_stud.getString("profile_img");
                    Log.e("urls===>", "" + image_url);
                    Picasso.get().load(image_url).placeholder(R.drawable.demo).fit().into(image_for_profile);
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
