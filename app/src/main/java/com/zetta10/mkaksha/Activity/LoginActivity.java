package com.zetta10.mkaksha.Activity;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Utility.Session;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements LocationListener {

    private static final int REQUEST_LOCATION = 1;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private static final long MIN_TIME_BW_UPDATES = 0;
    String udid;
    String version = "1";
    String fcm_token;
    String status;
    String connected;
    String email_or_phone;
    String password;
    EditText edittext_email_or_phone, edittext_password;
    boolean isConnected = true;
    ConnectivityManager.NetworkCallback connectivityCallback;
    ConnectivityManager connectivityManager;
    TextView textView_signup;
    LinearLayout linearlayout_login;
    int inputTypeValue;
    String num;
    Session session;
    LocationManager locationManager;
    String latitude, longitude;
    Location location;
    PrefManager prefManager;
    LocationListener locationListener;
    String latitude_ststic = String.valueOf(7854.251454);
    String longitude_static = String.valueOf(744554114);
    String device_name;
    boolean isGPSEnabled = false;
    Boolean isNetworkEnabled;
    private boolean monitoringConnectivity = false;
    ProgressDialog progressDialog;
    String refreshedToken;
    TextView txt_forget_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        MultiDex.install(this);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.loginactivity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        PrefManager.forceRTLIfSupported(getWindow(), LoginActivity.this);
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        textView_signup = findViewById(R.id.textview_signup);
        txt_forget_pass = findViewById(R.id.txt_forget_pass);
        linearlayout_login = findViewById(R.id.linear_login);
        edittext_email_or_phone = findViewById(R.id.edit_username);
        edittext_password = findViewById(R.id.edit_password);
        prefManager = new PrefManager(getApplicationContext());

        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("token==>", "" + refreshedToken);
        connectivityCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                isConnected = true;
                //    Toast.makeText(LoginActivity.this, "connected", Toast.LENGTH_SHORT).show();
                /* LogUtility.LOGD(TAG, "INTERNET CONNECTED");*/
            }

            @Override
            public void onLost(Network network) {
                isConnected = false;
                //  Toast.makeText(LoginActivity.this, "disconnected", Toast.LENGTH_SHORT).show();
                /* LogUtility.LOGD(TAG, "INTERNET LOST");*/
            }
        };
        checkConnectivity();

//        fcm_token = FirebaseInstanceId.getInstance().getToken();
//        Log.e("token==>", "" + fcm_token);
        udid = android.provider.Settings.System.getString(super.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        Log.e("udid==>", "" + udid);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getLocation();
        }
        txt_forget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });

        linearlayout_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_or_phone = edittext_email_or_phone.getText().toString().trim();
                password = edittext_password.getText().toString().trim();
                String digit_check = String.valueOf(TextUtils.isDigitsOnly(email_or_phone));
                if (TextUtils.isEmpty(email_or_phone)) {
                    Toast.makeText(getApplicationContext(), "Enter Username", Toast.LENGTH_LONG).show();
                    return;
                } else {

                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email_or_phone).matches()) {
                    if (digit_check.equals("false")) {
                        Toast.makeText(LoginActivity.this, "Enter valid Email Address", Toast.LENGTH_SHORT).show();
                        return;
                    }

                }
                if (digit_check.equals("true")) {
                    if (email_or_phone.length() < 10) {
                        Toast.makeText(LoginActivity.this, "Phone number not valid", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                } else {

                }
                if (password.length() < 8) {
                    Toast.makeText(LoginActivity.this, "Enter Password must be minimum 8 character", Toast.LENGTH_SHORT).show();
                    return;
                }
                getdata();
            }
        });
        textView_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), Registration.class);
                startActivity(intent);
            }
        });

        session = new Session(getApplicationContext());
        device_name = Build.MODEL;
        connected = session.getconnected();
    }


    private void getLocation() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (!isGPSEnabled) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    builder.show();
                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        else {
            if (location == null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                    return;
                }
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());

                        prefManager.setValue("latitude", latitude);
                        prefManager.setValue("longitude", longitude);

//                        Toast.makeText(this, "" + latitude + longitude, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        if (isNetworkEnabled) {
            if (locationManager != null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

                    return;
                }
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    prefManager.setValue("latitude", latitude);
                    prefManager.setValue("longitude", longitude);

//                    Toast.makeText(this, latitude + longitude, Toast.LENGTH_SHORT).show();
                }
            }
        }


    }

    protected void onResume() {
        super.onResume();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (!isGPSEnabled) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {
            if (location == null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

                    return;
                }
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());

                        prefManager.setValue("latitude", latitude);
                        prefManager.setValue("longitude", longitude);

//                        Toast.makeText(this, "" + latitude + longitude, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        if (isNetworkEnabled) {
            if (locationManager != null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

                    return;
                }
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    prefManager.setValue("latitude", latitude);
                    prefManager.setValue("longitude", longitude);

//                    Toast.makeText(this, latitude + longitude, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void checkConnectivity() {
        // here we are getting the connectivity service from connectivity manager
        connectivityManager = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        connectivityManager.registerNetworkCallback(
                new NetworkRequest.Builder()
                        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                        .build(), connectivityCallback);
        // Getting network Info
        // give Network Access Permission in Manifest
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        // isConnected is a boolean variable
        // here we check if network is connected or is getting connected
        isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();

        if (!isConnected) {
            // SHOW ANY ACTION YOU WANT TO SHOW
            // WHEN WE ARE NOT CONNECTED TO INTERNET/NETWORK
            /* LogUtility.LOGD(TAG, " NO NETWORK!");*/
            //  Toast.makeText(this, "disconnected", Toast.LENGTH_SHORT).show();
// if Network is not connected we will register a network callback to  monitor network
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                connectivityManager.registerNetworkCallback(
                        new NetworkRequest.Builder()
                                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                                .build(), connectivityCallback);
            }
            monitoringConnectivity = true;
        }

    }

    public void getdata() {

//         AppAPI appAPI = Apiclient.getRetrofit().create(Apiinterface.class);
//        Call<ResponseBody> logincall = apiinterface.logincall(password, device_name, longitude_static, latitude_ststic, email_or_phone, udid, version, fcm_token);
        progressDialog.show();
        AppAPI appAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> logincall = appAPI.logincall(password, device_name, longitude, latitude, email_or_phone, udid, version, refreshedToken);
        logincall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = String.valueOf(jsonObject.get("msg"));
                    Toast.makeText(LoginActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                    Log.e("log_id==>", "" + jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("id"));
                    prefManager.setLoginId(jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("id"));
                    prefManager.settoken(jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("api_token"));

                    String fname = jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("fname");
                    prefManager.setValue("fname", "" + fname);
                    String lname = jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("lname");
                    prefManager.setValue("lname", "" + lname);

                    String img_url = jsonObject.getJSONObject("data").getJSONObject("student_detail").getJSONObject("detail").getString("profile_img");
                    Log.e("img==>", "" + img_url);
                    prefManager.setValue("image_urls", "" + img_url);
//                    prefManager.setfname(jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("fname"));
//                    prefManager.setsname(jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("lname"));

                  /*  String fname = jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("fname");
                    Log.e("firstname==>", "" + fname);
                    String seocnd = jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("lname");
                    Log.e("firstname==>", "" + seocnd);*/

                    Log.e("token_ids==>", "" + jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("api_token"));
                    Log.e("ids==>", "" + jsonObject.getJSONObject("data").getJSONObject("student_detail").getString("id"));
                    //  Toast.makeText(LoginActivity.this, ""+jsonObject, Toast.LENGTH_SHORT).show();
                    status = String.valueOf(jsonObject.get("status"));
                    Log.e("status==>", "" + status);
                    progressDialog.dismiss();
//                        Toast.makeText(LoginActivity.this, "" + status, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    if (status.equals("1")) {
                    } /*else if (status.equals("2")) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, "InValid Token" + status, Toast.LENGTH_SHORT).show();
                    } else if (status.equals("3")) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, "Maintenance" + status, Toast.LENGTH_SHORT).show();
                    } else if (status.equals("4")) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, "Update" + status, Toast.LENGTH_SHORT).show();
                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
