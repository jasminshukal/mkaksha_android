package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Adapter.ChapterListAdapter;
import com.zetta10.mkaksha.Adapter.Rvadapter;
import com.zetta10.mkaksha.Adapter.SubjectListAdapter;
import com.zetta10.mkaksha.Adapter.TopicListAdapter;
import com.zetta10.mkaksha.Adapter.VideoListAdapter;
import com.zetta10.mkaksha.Model.ChapaterListModel.ChapaterListModel;
import com.zetta10.mkaksha.Model.SubjectListModel.Data;
import com.zetta10.mkaksha.Model.SubjectListModel.SubjectListModel;
import com.zetta10.mkaksha.Model.SuggetionModel.SuggetionModel;
import com.zetta10.mkaksha.Model.TopicListModel.TopicListModel;
import com.zetta10.mkaksha.Model.VideoListModel.VideoListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Utility.Session_for_search_history;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.mancj.materialsearchbar.MaterialSearchBar;

public class SearchActivity extends AppCompatActivity implements MaterialSearchBar.OnSearchActionListener {

    public static EditText et_search;
    private ImageButton bt_clear;
    NestedScrollView scrollbar;
    int payment_data_check;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    LinearLayout ly_subject, ly_chapter, ly_topic, ly_video_list, ly_dataNotFound, ly_activity;
    TextView txt_subject, txt_chapter, txt_topic, txt_video_list;
    RecyclerView rv_subject_list, rv_chapter_list, rv_topic_list, rv_video_list, rv_activity_list;
    String user_id, status, api_token, payment_data_size;
    //Subject data
    List<Data> subject_dataList;
    List<Data> subject_dataList_tmp;
    Session_for_search_history session_for_search_history;
    SubjectListAdapter subjectListAdapter;
    int search_count = 0;
    boolean flag = true;
    boolean data_search = true;
    //chapter data
    ChapterListAdapter chapterListAdapter;
    List<com.zetta10.mkaksha.Model.ChapaterListModel.Data> chapter_dataList;
    List<com.zetta10.mkaksha.Model.ChapaterListModel.Data> chapter_dataList_tmp;

    //VideoList
    VideoListAdapter videoListAdapter;
    List<com.zetta10.mkaksha.Model.VideoListModel.Data> video_dataList;
    List<com.zetta10.mkaksha.Model.VideoListModel.Data> video_dataList_tmp;

    //TopicList
    List<com.zetta10.mkaksha.Model.TopicListModel.Data> topic_dataList;
    List<com.zetta10.mkaksha.Model.TopicListModel.Data> topic_dataList_tmp;
    TopicListAdapter topicListAdapter;
    private List<Name> names;
    List<Name> names_tmp;
    String search_name;
    String[] array1 = {"AcademicActivity", "AddressInformation", "Available_Test_Result_Activity",
            "BasicInformation", "ChapterActivity", "EducationInformation", "GuardianInformation",
            "HelpActivity", "Manage_Profile", "Megatest", "MegatestActivity", "MegaTestResultActivity",
            "MyJourneyActivity", "Notification_display", "PersonalInformation", "PrivacyPolicyActivity",
            "QuizeActivity", "QuizeResultActivity", "ReferedActivity", "ReferelCodeActivity", "SubscriptionActivity",
            "TermsConditionActivity", "Test_history_Activity", "TopicActivity", "Wallet_Activity", "WatchListActivity"};

    List<SuggetionModel> suggetionModelList;

    private static final String TAG = MainActivity.class.getSimpleName();
    private LinearLayout mRootLayout;
//    private MaterialSearchView mMaterialSearchView;

    private MaterialSearchBar searchBar;

    private List<String> lastSearches;
    String suggestion;
    String state;
    Button btn_clear;
    SuggetionAdapter suggetionAdapter;
    RecyclerView rv_suggestion;

    //    SuggestionsAdapter adapter;
//    private SuggestionsAdapter adapter;
    //    MaterialSearchView searchHolder;
//    private SearchRecyclerAdapter adapterSearch;
    private int maxSuggestionCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.search_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });

        mRootLayout = findViewById(R.id.root_view);
//        mMaterialSearchView = findViewById(R.id.material_search_view);

        /*Button clearSearchHistory = findViewById(R.id.clear_search_history_button);
        clearSearchHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialSearchView.clearSearchHistory();
            }
        });*/
        session_for_search_history = new Session_for_search_history(getApplicationContext());
        prefManager = new PrefManager(SearchActivity.this);
        progressDialog = new ProgressDialog(SearchActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        lastSearches = new ArrayList<>();
        suggetionModelList = new ArrayList<>();
        init();
        searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setHint("Search hint");
        Intent intents = getIntent();

//        searchBar.OnItemClickListener();

/*
        if (Intent.ACTION_SEARCH.equals(intents.getAction())) {
            String query = intents.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
        }*/

        searchBar.setSpeechMode(false);
        searchBar.setOnSearchActionListener(this);
//        searchBar.OnItemClickListener();
//        searchBar.OnItemDeleteListener();

        //restore last queries from disk
        /*lastSearches = loadSearchSuggestionFromDisk();
        searchBar.setLastSuggestions(lastSearches);*/

      /*  AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> aAdapter, View aView, int arg2, long arg3) {
                TextView textViewItem = (TextView) aView;
                String filtr = textViewItem.getText().toString();
//                makeList(mCurrentLocation, filtr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Log.e("==>", "" + arg0);
            }
        };
        searchBar.setOnSearchActionListener((MaterialSearchBar.OnSearchActionListener) onItemSelectedListener);*/

     /*   if (adapter == null) {
            adapter = new SuggestionsAdapter(LayoutInflater.from(SearchActivity.this));
            adapter.getItemCount();
            adapter.getSuggestions();
            Log.e("==>1", "" + adapter.getItemCount());
            Log.e("==>2", "" + adapter.getSuggestions());
        }*/




        /*searchBar.OnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                String value = (String) adapter.getItemAtPosition(position);
                session_for_search_history.remove_search_text(getApplicationContext(), position);


            }
        });*/


        //restore last queries from disk
//        lastSearches = loadSearchSuggestionFromDisk();
//        lastSearches = load;
//        searchBar.setLastSuggestions(list);

        initializeData();
        initializeAdapter();
        user_id = prefManager.getLoginId();
        api_token = prefManager.gettokenId();

        Intent intent = getIntent();
        payment_data_size = intent.getStringExtra("payment_check");

        payment_data_check = Integer.parseInt(payment_data_size);

        if (payment_data_check > 0) {
            subject_list_Data();
            chapter_list();
            Topic_list();
            Video_list();
        } else {
            Toast.makeText(SearchActivity.this, "Please First Subscription...", Toast.LENGTH_LONG).show();
        }

        bt_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_search.setText("");
                scrollbar.setVisibility(View.GONE);
                rv_suggestion.setVisibility(View.GONE);
            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard();
                    if (payment_data_check > 0) {
                        searchAction();
                    }
                    return true;
                }
                return false;
            }
        });


        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_count = session_for_search_history.getSearch_count();
                suggetionModelList.clear();
                rv_suggestion.setVisibility(View.VISIBLE);
                for (int i = -1; i < search_count; i++) {
                    String search_text = session_for_search_history.getSearch_text(i);
                    SuggetionModel suggetionModel = new SuggetionModel();
                    suggetionModel.setHistory(search_text);
                    suggetionModelList.add(suggetionModel);
                }
                if (suggetionModelList.size() > 0) {
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this);
                    rv_suggestion.setLayoutManager(layoutManager);
                    suggetionAdapter = new SuggetionAdapter(SearchActivity.this, suggetionModelList);
                    rv_suggestion.setAdapter(suggetionAdapter);
                }

            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String search_str = et_search.getText().toString();
                if (search_str.equals("")) {
                    rv_suggestion.setVisibility(View.GONE);
                }
            }
        });


       /* btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session_for_search_history.session_destroy();
                lastSearches = new ArrayList<>();
            }
        });*/

    }

   /* private List<String> loadSearchSuggestionFromDisk() {
        return lastSearches;
    }*/

    private void initializeAdapter() {
//        Rvadapter adapter=new Rvadapter(names);
//        rv_activity_list.setAdapter(adapter);
        Rvadapter adapter = new Rvadapter(names);
        rv_activity_list.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                LinearLayoutManager.VERTICAL, false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(SearchActivity.this, 2,
                LinearLayoutManager.VERTICAL, false);

        rv_activity_list.setLayoutManager(mLayoutManager3);
        rv_activity_list.setItemAnimator(new DefaultItemAnimator());
        rv_activity_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
//        ly_subject.setVisibility(View.VISIBLE);
//        rv_subject_list.setVisibility(View.VISIBLE);
    }

    public void initializeData() {
        names = new ArrayList<>();
//        names.add(new Name("Home"));
        names.add(new Name("Manage Profile"));
        names.add(new Name("Subscription"));
        names.add(new Name("MyJourney"));
        names.add(new Name("Wallet"));
        names.add(new Name("Test And Result"));
        names.add(new Name("Wishlist"));
        names.add(new Name("Notification"));
        names.add(new Name("Invite Friend"));
        names.add(new Name("Privacy Policy"));
        names.add(new Name("Terms AndCondition"));
        names.add(new Name("Help"));
//        names.add(new Name("Logout"));

        /*names.add(new Name("AddressInformation"));
        names.add(new Name("BasicInformation"));
        names.add(new Name("ChapterActivity"));
        names.add(new Name("EducationInformation"));
        names.add(new Name("GuardianInformation"));
        names.add(new Name("HelpActivity"));
        names.add(new Name("Megatest"));
        names.add(new Name("MegatestActivity"));
        names.add(new Name("MegaTestResultActivity"));
        names.add(new Name("PersonalInformation"));
        names.add(new Name("QuizeActivity"));
        names.add(new Name("QuizeResultActivity"));
        names.add(new Name("ReferelCodeActivity"));
        names.add(new Name("Test_history_Activity"));
        names.add(new Name("TopicActivity"));
        names.add(new Name("WatchListActivity"));*/
    }

    private void searchAction() {
        scrollbar.setVisibility(View.GONE);
        final String query = et_search.getText().toString().trim();
        search_count = session_for_search_history.getSearch_count();
        session_for_search_history.setSearch_text(query);
        session_for_search_history.setSearch_count();
        rv_suggestion.setVisibility(View.GONE);

       /* for (int i = -1; i < search_count; i++) {

            String search_text = session_for_search_history.getSearch_text(i);
            SuggetionModel suggetionModel = new SuggetionModel();
            suggetionModel.setHistory(search_text);
        }*/

//        String query = String.valueOf(charSequence);
//        searchBar.getLastSuggestions().add(query);
//        prefManager.setValue("query", query);
//        searchBar.getLastSuggestions().add(prefManager.getValue("query"));





        /*SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
        suggestions.saveRecentQuery(query, null);*/
        Log.e("search_name==>", "" + query);
        if (!query.equals("")) {
            names_tmp = new ArrayList<>();
            /*Activity Search*/
            for (int i = 0; i < names.size(); i++) {
                if (names.get(i).getTextView().toLowerCase().contains(query.toLowerCase())) {
                    names_tmp.add(names.get(i));
                    Log.e("CategoryList_tmp", "" + names_tmp.size());
//                    Log.e("CategoryList_tmp==>", "" + subject_dataList_tmp.get(i).getSubjectName());
                } else {
                    Log.e("else", "" + names.get(i).getTextView());
                    ly_subject.setVisibility(View.GONE);
                    rv_subject_list.setVisibility(View.GONE);
                    scrollbar.setVisibility(View.GONE);
                    ly_dataNotFound.setVisibility(View.VISIBLE);
                }
            }
            if (names_tmp.size() > 0) {
                Rvadapter adapter = new Rvadapter(SearchActivity.this, names_tmp);
                rv_activity_list.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                        LinearLayoutManager.VERTICAL, false);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(SearchActivity.this, 2,
                        LinearLayoutManager.VERTICAL, false);

                rv_activity_list.setLayoutManager(mLayoutManager3);
                rv_activity_list.setItemAnimator(new DefaultItemAnimator());
                rv_activity_list.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                ly_activity.setVisibility(View.GONE);
                rv_activity_list.setVisibility(View.GONE);
                scrollbar.setVisibility(View.GONE);
                ly_dataNotFound.setVisibility(View.VISIBLE);
            }


            /*SubjectList All*/
            subject_dataList_tmp = new ArrayList<>();
            for (int i = 0; i < subject_dataList.size(); i++) {
                if (subject_dataList.get(i).getSubjectName().toLowerCase().contains(query.toLowerCase())) {
                    subject_dataList_tmp.add(subject_dataList.get(i));
                    Log.e("CategoryList_tmp", "" + subject_dataList_tmp.size());
//                    Log.e("CategoryList_tmp==>", "" + subject_dataList_tmp.get(i).getSubjectName());
                } else {
                    Log.e("else", "" + subject_dataList.get(i).getSubjectName());
                    ly_subject.setVisibility(View.GONE);
                    rv_subject_list.setVisibility(View.GONE);
                    scrollbar.setVisibility(View.GONE);
                    ly_dataNotFound.setVisibility(View.VISIBLE);
                }
            }
            if (subject_dataList_tmp.size() > 0) {

                subjectListAdapter = new SubjectListAdapter(SearchActivity.this, subject_dataList_tmp, "Home", payment_data_size);
                rv_subject_list.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                        LinearLayoutManager.HORIZONTAL, false);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(SearchActivity.this, 2,
                        LinearLayoutManager.VERTICAL, false);

                rv_subject_list.setLayoutManager(gridLayoutManager);
                rv_subject_list.setItemAnimator(new DefaultItemAnimator());
                rv_subject_list.setAdapter(subjectListAdapter);
                subjectListAdapter.notifyDataSetChanged();
                ly_subject.setVisibility(View.VISIBLE);
                rv_subject_list.setVisibility(View.VISIBLE);
            } else {
                ly_subject.setVisibility(View.GONE);
                rv_subject_list.setVisibility(View.GONE);
                scrollbar.setVisibility(View.GONE);
                ly_dataNotFound.setVisibility(View.VISIBLE);
            }

            /*Chapter LIst*/
            try {
                chapter_dataList_tmp = new ArrayList<>();
                for (int i = 0; i < chapter_dataList.size(); i++) {
                    if (chapter_dataList.get(i).getChapterName().toLowerCase().contains(query.toLowerCase())) {
                        chapter_dataList_tmp.add(chapter_dataList.get(i));
                        Log.e("chapter_dataList_tmp", "" + chapter_dataList_tmp.size());
//                    Log.e("CategoryList_tmp==>", "" + subject_dataList_tmp.get(i).getSubjectName());
                    } else {
                        Log.e("else", "" + chapter_dataList.get(i).getChapterName());
                        ly_subject.setVisibility(View.GONE);
                        rv_subject_list.setVisibility(View.GONE);
                        scrollbar.setVisibility(View.GONE);
                        ly_dataNotFound.setVisibility(View.VISIBLE);
                    }
                }
                if (chapter_dataList_tmp.size() > 0) {
                    chapterListAdapter = new ChapterListAdapter(SearchActivity.this, chapter_dataList_tmp, payment_data_size);
                    rv_chapter_list.setHasFixedSize(true);
                    RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                            LinearLayoutManager.VERTICAL, false);
                    rv_chapter_list.setLayoutManager(mLayoutManager3);
                    rv_chapter_list.setItemAnimator(new DefaultItemAnimator());
                    rv_chapter_list.setAdapter(chapterListAdapter);
                    chapterListAdapter.notifyDataSetChanged();
                    ly_chapter.setVisibility(View.VISIBLE);
                    rv_chapter_list.setVisibility(View.VISIBLE);
                } else {
                    ly_chapter.setVisibility(View.GONE);
                    rv_chapter_list.setVisibility(View.GONE);
                    scrollbar.setVisibility(View.GONE);
                    ly_dataNotFound.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                Log.e("e_tmp==>", "" + e.getMessage());
            }

            /*TopicList*/
            topic_dataList_tmp = new ArrayList<>();
            for (int i = 0; i < topic_dataList.size(); i++) {
                if (topic_dataList.get(i).getTopicName().toLowerCase().contains(query.toLowerCase())) {
                    topic_dataList_tmp.add(topic_dataList.get(i));
                    Log.e("topic_dataList_tmp", "" + topic_dataList_tmp.size());
//                    Log.e("CategoryList_tmp==>", "" + subject_dataList_tmp.get(i).getSubjectName());
                } else {
                    Log.e("else", "" + topic_dataList.get(i).getTopicName());
                    ly_subject.setVisibility(View.GONE);
                    rv_subject_list.setVisibility(View.GONE);
                    scrollbar.setVisibility(View.GONE);
                    ly_dataNotFound.setVisibility(View.VISIBLE);
                }
            }
            if (topic_dataList_tmp.size() > 0) {

                topicListAdapter = new TopicListAdapter(SearchActivity.this, topic_dataList_tmp);
                rv_topic_list.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                        LinearLayoutManager.VERTICAL, false);
                rv_topic_list.setLayoutManager(mLayoutManager3);
                rv_topic_list.setItemAnimator(new DefaultItemAnimator());
                rv_topic_list.setAdapter(topicListAdapter);
                topicListAdapter.notifyDataSetChanged();
                ly_topic.setVisibility(View.VISIBLE);
                rv_topic_list.setVisibility(View.VISIBLE);
            } else {
                ly_topic.setVisibility(View.GONE);
                rv_topic_list.setVisibility(View.GONE);
                scrollbar.setVisibility(View.GONE);
                ly_dataNotFound.setVisibility(View.VISIBLE);
            }



            /*Video List*/

            video_dataList_tmp = new ArrayList<>();
            for (int i = 0; i < video_dataList.size(); i++) {
                if (video_dataList.get(i).getVideoName().toLowerCase().contains(query.toLowerCase())) {
                    video_dataList_tmp.add(video_dataList.get(i));
                    Log.e("video_dataList_tmp", "" + video_dataList_tmp.size());
//                    Log.e("CategoryList_tmp==>", "" + subject_dataList_tmp.get(i).getSubjectName());
                } else {
                    Log.e("else", "" + video_dataList.get(i).getVideoName());
                    ly_subject.setVisibility(View.GONE);
                    rv_subject_list.setVisibility(View.GONE);
                    scrollbar.setVisibility(View.GONE);
                    ly_dataNotFound.setVisibility(View.VISIBLE);
                }
            }
            if (video_dataList_tmp.size() > 0) {

                videoListAdapter = new VideoListAdapter(SearchActivity.this, video_dataList_tmp);
                rv_video_list.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                        LinearLayoutManager.VERTICAL, false);
                rv_video_list.setLayoutManager(mLayoutManager3);
                rv_video_list.setItemAnimator(new DefaultItemAnimator());
                rv_video_list.setAdapter(videoListAdapter);
                videoListAdapter.notifyDataSetChanged();
                ly_video_list.setVisibility(View.VISIBLE);
                rv_video_list.setVisibility(View.VISIBLE);
            } else {
                ly_video_list.setVisibility(View.GONE);
                rv_video_list.setVisibility(View.GONE);
                scrollbar.setVisibility(View.GONE);
                ly_dataNotFound.setVisibility(View.VISIBLE);
            }


            scrollbar.setVisibility(View.VISIBLE);

          /*  if (subject_dataList_tmp.size() > 0 && chapter_dataList_tmp.size() > 0 && video_dataList_tmp.size() > 0 &&
                    topic_dataList_tmp.size() > 0 && names_tmp.size() > 0) {
                scrollbar.setVisibility(View.VISIBLE);
                ly_dataNotFound.setVisibility(View.GONE);
            } else {
                scrollbar.setVisibility(View.GONE);
                ly_dataNotFound.setVisibility(View.VISIBLE);
            }*/

        } else {
            Toast.makeText(SearchActivity.this, "Please fill search input", Toast.LENGTH_SHORT).show();
            scrollbar.setVisibility(View.GONE);
            ly_dataNotFound.setVisibility(View.VISIBLE);
        }
        /*chapterList All*/
    }

    private void hideKeyboard() {
        View view = SearchActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void init() {
        ly_subject = findViewById(R.id.ly_subject);
        ly_chapter = findViewById(R.id.ly_chapter);
        ly_topic = findViewById(R.id.ly_topic);
        ly_video_list = findViewById(R.id.ly_video_list);
        ly_dataNotFound = findViewById(R.id.ly_dataNotFound);
        ly_activity = findViewById(R.id.ly_activity);

        txt_subject = findViewById(R.id.txt_subject);
        txt_chapter = findViewById(R.id.txt_chapter);
        txt_topic = findViewById(R.id.txt_topic);
        txt_video_list = findViewById(R.id.txt_video_list);

        rv_subject_list = findViewById(R.id.rv_subject_list);
        rv_chapter_list = findViewById(R.id.rv_chapter_list);
        rv_topic_list = findViewById(R.id.rv_topic_list);
        rv_video_list = findViewById(R.id.rv_video_list);
        rv_activity_list = findViewById(R.id.rv_activity_list);

        scrollbar = findViewById(R.id.scrollbar);
        bt_clear = findViewById(R.id.bt_clear);
        et_search = findViewById(R.id.et_search);
        btn_clear = findViewById(R.id.btn_clear);
        rv_suggestion = findViewById(R.id.rv_suggestion);
//        searchHolder = findViewById(R.id.searchHolder);
    }

    private void subject_list_Data() {
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            Log.e("login_id", "" + user_id);
            Log.e("login_token", "" + api_token);
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<SubjectListModel> call = appAPI.subject_list_call(api_token, user_id);
            call.enqueue(new Callback<SubjectListModel>() {
                @Override
                public void onResponse(Call<SubjectListModel> call, Response<SubjectListModel> response) {
                    subject_dataList = new ArrayList<>();
                    subject_dataList = response.body().getData();
                    status = response.body().getStatus();
                    Log.e("status==>", "" + status);
                    if (status.equals("1")) {
                        if (response.isSuccessful()) {
                            Log.e("data1", "" + response.body());
                            Log.e("data2", "" + response.body().getData());
                            Log.e("data3", "" + response.body().getData().get(0).getSubjectImg());
                            if (subject_dataList.size() > 0) {
                                subjectListAdapter = new SubjectListAdapter(SearchActivity.this, subject_dataList, "Home", payment_data_size);
                                rv_subject_list.setHasFixedSize(true);
                                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                                        LinearLayoutManager.HORIZONTAL, false);
                                GridLayoutManager gridLayoutManager = new GridLayoutManager(SearchActivity.this, 2,
                                        LinearLayoutManager.VERTICAL, false);

                                rv_subject_list.setLayoutManager(gridLayoutManager);
                                rv_subject_list.setItemAnimator(new DefaultItemAnimator());
                                rv_subject_list.setAdapter(subjectListAdapter);
                                subjectListAdapter.notifyDataSetChanged();
                                rv_subject_list.setVisibility(View.VISIBLE);
                                rv_subject_list.setVisibility(View.VISIBLE);
                            } else {
                                rv_subject_list.setVisibility(View.GONE);
                                ly_dataNotFound.setVisibility(View.VISIBLE);
                            }
                            progressDialog.dismiss();
                        }
                    } else if (status.equals("2")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(SearchActivity.this, "InValid Token", Toast.LENGTH_SHORT).show();
                            prefManager.setLoginId("0");
                            prefManager.settoken("0");
                            Intent intent = new Intent(SearchActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            SearchActivity.this.finish();
                        } catch (Exception e) {
                            Log.e("token_error==>", "" + e.getMessage());
                        }
                    } else if (status.equals("3")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(SearchActivity.this, "maintenance", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SearchActivity.this, PopupActivity.class);
                            intent.putExtra("status_data", status);
                            startActivity(intent);
                            SearchActivity.this.finish();
                        } catch (Exception e) {
                            Log.e("error3==>", "" + e.getMessage());
                        }
                    } else if (status.equals("4")) {
                        try {
                            progressDialog.dismiss();
                            Toast.makeText(SearchActivity.this, "Update", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SearchActivity.this, PopupActivity.class);
                            intent.putExtra("status_data", status);
                            startActivity(intent);
                            SearchActivity.this.finish();
                        } catch (Exception e) {
                            Log.e("error4==>", "" + e.getMessage());
                        }
                    }
//                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<SubjectListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }

    }

    private void chapter_list() {
        progressDialog.show();
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<ChapaterListModel> call = appAPI.Search_Chapter(api_token, user_id);
            call.enqueue(new Callback<ChapaterListModel>() {
                @Override
                public void onResponse(Call<ChapaterListModel> call, Response<ChapaterListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        chapter_dataList = new ArrayList<>();
                        chapter_dataList = response.body().getData();
                        Log.e("dataList==>", "" + chapter_dataList.size());
                        if (chapter_dataList.size() > 0) {
                            chapterListAdapter = new ChapterListAdapter(SearchActivity.this, chapter_dataList, payment_data_size);
                            rv_chapter_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_chapter_list.setLayoutManager(mLayoutManager3);
                            rv_chapter_list.setItemAnimator(new DefaultItemAnimator());
                            rv_chapter_list.setAdapter(chapterListAdapter);
                            chapterListAdapter.notifyDataSetChanged();
                            rv_chapter_list.setVisibility(View.VISIBLE);
                            rv_chapter_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_chapter_list.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ChapaterListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }

    }


    private void Topic_list() {
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;

            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<TopicListModel> call = appAPI.Search_Topic(api_token, user_id);
            call.enqueue(new Callback<TopicListModel>() {
                @Override
                public void onResponse(Call<TopicListModel> call, Response<TopicListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        topic_dataList = new ArrayList<>();
                        topic_dataList = response.body().getData();
                        Log.e("dataList==>", "" + topic_dataList.size());
                        if (topic_dataList.size() > 0) {
                            topicListAdapter = new TopicListAdapter(SearchActivity.this, topic_dataList);
                            rv_topic_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_topic_list.setLayoutManager(mLayoutManager3);
                            rv_topic_list.setItemAnimator(new DefaultItemAnimator());
                            rv_topic_list.setAdapter(topicListAdapter);
                            topicListAdapter.notifyDataSetChanged();
                            rv_topic_list.setVisibility(View.VISIBLE);
                            rv_topic_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_topic_list.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<TopicListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }

    }

    private void Video_list() {
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();

            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<VideoListModel> call = appAPI.Search_Video(api_token, user_id);
            call.enqueue(new Callback<VideoListModel>() {
                @Override
                public void onResponse(Call<VideoListModel> call, Response<VideoListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        video_dataList = new ArrayList<>();
                        video_dataList = response.body().getData();
                        Log.e("dataList==>", "" + video_dataList.size());
                        if (video_dataList.size() > 0) {
                            videoListAdapter = new VideoListAdapter(SearchActivity.this, video_dataList);
                            rv_video_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(SearchActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_video_list.setLayoutManager(mLayoutManager3);
                            rv_video_list.setItemAnimator(new DefaultItemAnimator());
                            rv_video_list.setAdapter(videoListAdapter);
                            videoListAdapter.notifyDataSetChanged();
                            rv_video_list.setVisibility(View.VISIBLE);
                            rv_video_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_video_list.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<VideoListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        searchBar.getLastSuggestions();
//        searchBar.setMaxSuggestionCount(5);
//        flag = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
//        searchBar.getLastSuggestions();
//        searchBar.setMaxSuggestionCount(5);
//        flag = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
//        searchBar.getLastSuggestions();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        searchBar.getLastSuggestions();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //save last queries to disk
//        saveSearchSuggestionToDisk(searchBar.getLastSuggestions());
    }

    @Override
    public void onSearchStateChanged(boolean b) {

        state = b ? "enabled" : "disabled";
//        Toast.makeText(SearchActivity.this, "Search " + state, Toast.LENGTH_SHORT).show();
//        lastSearches.add(search_name);
//        search_name = prefManager.getValue("" + suggestion);
//        Log.e("search_name===>", "" + search_name);
//        lastSearches.add(search_name);
//        searchBar.setLastSuggestions(lastSearches);

        if (state.equals("enabled")) {
            if (flag == true) {
                flag = false;
                search_count = session_for_search_history.getSearch_count();
                for (int i = -1; i < search_count; i++) {
                    search_name = session_for_search_history.getSearch_text(i);
                    lastSearches.add(search_name);
                }
            }
            searchBar.setLastSuggestions(lastSearches);
        }
    }


    @Override
    public void onSearchConfirmed(CharSequence charSequence) {
//        Toast.makeText(this, "Searching " + charSequence.toString() + " ......", Toast.LENGTH_SHORT).show();
//        search_name = String.valueOf(charSequence);
//        lastSearches.add(String.valueOf(charSequence));
//        searchBar.setLastSuggestions(lastSearches);


        if (state.equals("enabled")) {
            if (data_search == true) {
                data_search = false;
                String test_data = charSequence.toString().trim();
                search_count = session_for_search_history.getSearch_count();
                if (!TextUtils.isEmpty(test_data)) {
                    session_for_search_history.setSearch_text(charSequence.toString().trim());
                    session_for_search_history.setSearch_count();

                }
            }
        }
//        suggestion = String.valueOf(charSequence);
//        prefManager.setValue("" + suggestion, "" + charSequence);

//        searchBar.setLastSuggestions(lastSearches);
//        searchAction(charSequence);
    }

    @Override
    public void onButtonClicked(int i) {
        Log.e("==>position", "" + i);
        switch (i) {
            case MaterialSearchBar.BUTTON_NAVIGATION:
                Toast.makeText(SearchActivity.this, "Button Nav ", Toast.LENGTH_SHORT).show();
                break;
            case MaterialSearchBar.BUTTON_SPEECH:
                Toast.makeText(SearchActivity.this, "Speech ", Toast.LENGTH_SHORT).show();
        }
//        adapter.deleteSuggestion(i,search_name);
    }


    /*private void saveSearchSuggestionToDisk(List<String> lastSuggestions) {
        Log.e("==>In", "" + lastSuggestions);
        lastSearches = lastSuggestions;
    }*/

    /* @Override
    public void onSearch(@NonNull String searchTerm) {
        searchAction(searchTerm);
    }

    @Override
    public void onVoiceSearchError(int error) {

    }

    @Override
    public void onVoiceSearch(@NonNull String searchTerm) {

    }*/
}

class SuggetionAdapter extends RecyclerView.Adapter<com.zetta10.mkaksha.Activity.SuggetionAdapter.MyViewHolder> {

    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<SuggetionModel> suggetionModelList;
    Session_for_search_history session_for_search_history;

    public SuggetionAdapter(Context context, List<SuggetionModel> suggetionModelList) {
        this.suggetionModelList = suggetionModelList;
        this.mcontext = context;
        session_for_search_history = new Session_for_search_history(mcontext);
    }


    @NonNull
    @Override
    public com.zetta10.mkaksha.Activity.SuggetionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_last_request, parent, false);

        return new com.zetta10.mkaksha.Activity.SuggetionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull com.zetta10.mkaksha.Activity.SuggetionAdapter.MyViewHolder holder, int position) {
//        Picasso.with(mcontext).load(subjectList.get(position).getSubjectImg()).priority(HIGH).into(holder.iv_thumb);

        holder.text.setText(suggetionModelList.get(position).getHistory());
        String history_text = suggetionModelList.get(position).getHistory();
        // SearchActivity searchActivity = new SearchActivity();

        holder.ly_for_history_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String history = suggetionModelList.get(position).getHistory();
                SearchActivity.et_search.setText("" + history);
            }
        });
        if (history_text.equals("")) {
            holder.ly_for_history_item.setVisibility(View.GONE);
        }
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "call");
                session_for_search_history.remove_search_text(mcontext, position);
                holder.ly_for_history_item.setVisibility(View.GONE);
               /* Intent intent = new Intent(mcontext, ChapterActivity.class);
                intent.putExtra("chapter_id", subjectList.get(position).getId());
                intent.putExtra("subject", subjectList.get(position).getSubjectName());
                intent.putExtra("payment_data_size", payment_array_size);
                Log.e("c_id==>", "" + subjectList.get(position).getId());
                mcontext.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return suggetionModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        ImageView iv_delete;
        LinearLayout ly_for_history_item;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            ly_for_history_item = itemView.findViewById(R.id.ly_for_history_item);
        }
    }
}
