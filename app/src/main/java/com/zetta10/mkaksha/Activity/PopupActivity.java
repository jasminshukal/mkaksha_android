package com.zetta10.mkaksha.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.Session;

public class PopupActivity extends AppCompatActivity {

    ImageView img_maintanance, img_update;
    TextView textView;
    String status;
    Session session;
    String connected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.popup_activity);
        session = new Session(getApplicationContext());
        img_maintanance = findViewById(R.id.img_maintanance);
        img_update = findViewById(R.id.img_update);
        textView = findViewById(R.id.textview_popup);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            status = bundle.getString("status_data");
            Log.e("chapter_id", "" + status);
            if (status.equals("3")) {
                img_maintanance.setVisibility(View.VISIBLE);
                img_update.setVisibility(View.GONE);
                textView.setText(R.string.maintanance);
            }
            if (status.equals("4")) {
                img_update.setVisibility(View.VISIBLE);
                img_maintanance.setVisibility(View.GONE);
                textView.setText(R.string.update);
            }
        }


    }
}
