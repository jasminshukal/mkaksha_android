package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressInformation extends AppCompatActivity {

    EditText edit_perment_add, edit_corres_add;
    Spinner spinner_for_city;
    LinearLayout linearLayout_for_save;
    String[] cities;
    String district;
    String permenent_add, corres_ad;
    ProgressDialog progressDialog;
    String login_token, user_id;
    PrefManager prefManager;
    String str_permanent_address, str_correspondence_address, str_district;
    ArrayAdapter adapter;
    LinearLayout finish_video_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_address_information);

        finish_video_list = findViewById(R.id.finish_video_list);
        edit_perment_add = findViewById(R.id.edit_perment_add);
        edit_corres_add = findViewById(R.id.edit_corres_add);
        spinner_for_city = findViewById(R.id.spinner_for_district);
        linearLayout_for_save = findViewById(R.id.linear_for_save);

        progressDialog = new ProgressDialog(AddressInformation.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getApplicationContext());
        login_token = prefManager.gettokenId();
        user_id = prefManager.getLoginId();

        linearLayout_for_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permenent_add = edit_perment_add.getText().toString().trim();
                corres_ad = edit_corres_add.getText().toString().trim();

                if (TextUtils.isEmpty(permenent_add)) {

                    Toast.makeText(AddressInformation.this, "Enter Your Permanent Address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(corres_ad)) {

                    Toast.makeText(AddressInformation.this, "Enter Your Correspondence Address", Toast.LENGTH_SHORT).show();
                    return;
                }
                stud_address_info();
            }
        });
       /* ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,cities);
        spinner_for_city.setAdapter(arrayAdapter);*/

        adapter = ArrayAdapter.createFromResource(this, R.array.district, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_for_city.setAdapter(adapter);
        cities = getResources().getStringArray(R.array.district);
        spinner_for_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                district = cities[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        get_profile_detail();

        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    public void stud_address_info() {

        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> stud_add_info_call = apiinterface.student_address_info(login_token, user_id, district, permenent_add, corres_ad);

        stud_add_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    //  JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    String msg = jsonObject.getString("msg");
                    Toast.makeText(AddressInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void get_profile_detail() {

        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(user_id, login_token);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(AddressInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                    str_permanent_address = jsonObject_stud.getString("permanent_address");
                    str_correspondence_address = jsonObject_stud.getString("correspondence_address");
                    str_district = jsonObject_stud.getString("city");
                    set_stud_data();
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void set_stud_data() {

        edit_corres_add.setText(str_correspondence_address);
        edit_perment_add.setText(str_permanent_address);
        String compare_value = str_district;
        if (compare_value != null) {
            int spinnerposition = adapter.getPosition(compare_value);
            spinner_for_city.setSelection(spinnerposition);
        }
    }
}
