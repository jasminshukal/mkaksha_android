package com.zetta10.mkaksha.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;
import com.zetta10.mkaksha.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PdfViewer extends AppCompatActivity {

    PDFView pdfView;
    String read, fileUrl, fileName, name;
    ImageView download;
    LinearLayout finish_video_list;
    TextView toolbar_title;
    private static final String PDF_DIRECTORY = "/MKaksha";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.pdf_viewer_activity);

        pdfView = findViewById(R.id.pdfView);
        toolbar_title = findViewById(R.id.toolbar_title);
        finish_video_list = findViewById(R.id.finish_video_list);
        download = findViewById(R.id.btn_download);

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                read = bundle.getString("read");
                name = bundle.getString("name");
                Log.e("video_id", "" + read);
                Log.e("name===>", "" + name);
                toolbar_title.setText(name);
            }
           /* Intent i = getIntent();
            read = i.getStringExtra("read");
            name = i.getStringExtra("name");
            toolbar_title.setText(name);*/
        } catch (Exception e) {
        }

        if (!read.equals(null)) {
            new pdfStream().execute(read);
        }
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Toast.makeText(PdfViewer.this, "Downloading.... ", Toast.LENGTH_LONG).show();
                new DownloadFile().execute(read, name);
                Toast.makeText(PdfViewer.this, "Downloading Completed.... ", Toast.LENGTH_LONG).show();*/

                DownloadBook();
            }
        });
        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class pdfStream extends AsyncTask<String, Void, InputStream> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PdfViewer.this, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.wait));
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            pdfView.fromStream(inputStream).load();
            pDialog.dismiss();
        }
    }


    //Download
    /*private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
//            fileName = strings[1];  // -> maven.pdf
//            myFileUrl = new URL(args[0]);
//            String path = myFileUrl.getPath();
            String path = fileUrl;
            fileName = path.substring(path.lastIndexOf('/') + 1);
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "Mkaksha");
            folder.mkdir();

            Log.e("url==>", "" + fileUrl);
            Log.e("fileName==>", "" + fileName);

            File pdfFile = new File(folder, fileName);

            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader_r.downloadFile(fileUrl, pdfFile);
            return null;
        }
    }*/


    public static class FileDownloader_r {
        private static final int MEGABYTE = 1024 * 1024;

        public static void downloadFile(String fileUrl, File directory) {
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);
                int totalSize = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutputStream.write(buffer, 0, bufferLength);
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void DownloadBook() {
        Log.e("book_url==>", "" + read);
        new PdfViewer.DownloadBook("save").execute(read);
    }


    public class DownloadBook extends AsyncTask<String, String, String> {
        private ProgressDialog pDialog;
        URL myFileUrl;
        String option;
        File file;

        DownloadBook(String option) {
            this.option = option;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(PdfViewer.this, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.downloading));
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            try {
                myFileUrl = new URL(args[0]);
                String path = myFileUrl.getPath();
                String fileName = path.substring(path.lastIndexOf('/') + 1);
                //   File dir = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name) + "/");
                File dir = new File(Environment.getExternalStorageDirectory() + "" + PDF_DIRECTORY);
//                dir.mkdirs();
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                file = new File(dir, fileName);

                if (!file.exists()) {
                    HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    FileOutputStream fos = new FileOutputStream(file);
                    byte data[] = new byte[4096];
                    int count;
                    while ((count = is.read(data)) != -1) {
                        if (isCancelled()) {
                            is.close();
                            return null;
                        }
                        fos.write(data, 0, count);
                    }
                    fos.flush();
                    fos.close();

                    if (option.equals("save")) {
                        MediaScannerConnection.scanFile(PdfViewer.this, new String[]{file.getAbsolutePath()},
                                null,
                                new MediaScannerConnection.OnScanCompletedListener() {
                                    @Override
                                    public void onScanCompleted(String path, Uri uri) {

                                    }
                                });
                    }
                    return "1";
                } else {
                    return "2";
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", "" + e.getMessage());
                return "0";
            }
        }

        @Override
        protected void onPostExecute(String args) {
            if (args.equals("1") || args.equals("2")) {
                switch (option) {
                    case "save":
                        if (args.equals("2")) {
                            Toast.makeText(PdfViewer.this, "Already Download", Toast.LENGTH_SHORT).show();
                        } else {
//                            AddDownload();
                            Toast.makeText(PdfViewer.this, "Download success", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } else {
                Toast.makeText(PdfViewer.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();
        }
    }
}
