package com.zetta10.mkaksha.Activity;

public class Name {

    String textView;

    Name(String textView) {
        this.textView = textView;
    }

    public String getTextView() {
        return textView;
    }

    public void setTextView(String textView) {
        this.textView = textView;
    }
}
