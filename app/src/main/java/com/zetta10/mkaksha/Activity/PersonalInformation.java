package com.zetta10.mkaksha.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalInformation extends AppCompatActivity {

    EditText edit_date_of_birth,edit_nationality,edit_blood_group,edit_about;
    LinearLayout linear_for_submit,finish_video_list;
    String date_of_birth,nationality,blood_group,about;
    Calendar myCalendar;
    Spinner spinner_for_gender;
    String [] gender;
    String gender_selected;
    ArrayList arrayList;
    PrefManager prefManager;
    String user_id;
    String login_token;
    ProgressDialog progressDialog;
    String str_dob,str_nationality,str_blood,str_about,str_gender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_personal_information);

        prefManager = new PrefManager(getApplicationContext());
        progressDialog = new ProgressDialog(PersonalInformation.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        edit_date_of_birth = findViewById(R.id.edit_date_of_birth);
        edit_nationality = findViewById(R.id.edit_nationality);
        edit_blood_group = findViewById(R.id.edit_blood_group);
        edit_about = findViewById(R.id.edit_about);
        finish_video_list = findViewById(R.id.finish_video_list);
        linear_for_submit = findViewById(R.id.linear_for_submit);
        spinner_for_gender = findViewById(R.id.spinner_for_gender);
        arrayList = new ArrayList();
        gender = getResources().getStringArray(R.array.gender);
        user_id = prefManager.getLoginId();
        login_token = prefManager.gettokenId();
      //  arrayList.add(R.array.gender);
       /* ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,gender);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_for_gender.setAdapter(arrayAdapter);*/
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
               R.array.gender, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_for_gender.setAdapter(adapter);
        spinner_for_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender_selected = gender[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        linear_for_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_of_birth = edit_date_of_birth.getText().toString().trim();
                nationality = edit_nationality.getText().toString().trim();
                blood_group = edit_blood_group.getText().toString().trim();
                about = edit_about.getText().toString().trim();

                if (TextUtils.isEmpty(date_of_birth)) {
                    Toast.makeText(PersonalInformation.this, "Enter Your Birth Date", Toast.LENGTH_SHORT).show();
                    return;

                }
                if (TextUtils.isEmpty(nationality)) {
                    Toast.makeText(PersonalInformation.this, "Enter a Nationality", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(blood_group)) {
                    Toast.makeText(PersonalInformation.this, "Enter a Blood Group", Toast.LENGTH_SHORT).show();
                    return;
                } /*else if (blood_group.length() < 10) {
                    Toast.makeText(PersonalInformation.this, "Enter a Valid Mobile Number", Toast.LENGTH_SHORT).show();
                    return;
                }*/

               if (TextUtils.isEmpty(about)) {
                    Toast.makeText(PersonalInformation.this, "Enter About section", Toast.LENGTH_SHORT).show();
                    return;
                }
               student_personal_info();
            }
        });
        myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        edit_date_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(PersonalInformation.this,onDateSetListener,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        get_profile_detail();
    }
    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edit_date_of_birth.setText(sdf.format(myCalendar.getTime()));
    }

    public void student_personal_info(){
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> stud_per_info_call = apiinterface.student_personal_info(login_token,user_id,date_of_birth,gender_selected,nationality,blood_group,about);

        stud_per_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
                    Toast.makeText(PersonalInformation.this, ""+msg, Toast.LENGTH_SHORT).show();
                   /* JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    str_dob = jsonObject_data.getString("dob");
                    str_nationality = jsonObject_data.getString("nationality");
                    str_blood = jsonObject_data.getString("blood_group");
                    str_about = jsonObject_data.getString("about");
                    str_gender = jsonObject_data.getString("gender");*/

                 //  set_stud_data();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    public void get_profile_detail() {

        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(user_id, login_token);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(PersonalInformation.this, ""+msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                    str_dob = jsonObject_stud.getString("dob");
                    str_gender = jsonObject_stud.getString("gender");
                    str_nationality = jsonObject_stud.getString("nationality");
                    str_blood = jsonObject_stud.getString("blood_group");
                    str_about = jsonObject_stud.getString("about");
                    set_stud_data();
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    public void set_stud_data() {

        edit_date_of_birth.setText(str_dob);
        edit_about.setText(str_about);
        edit_blood_group.setText(str_blood);
        edit_nationality.setText(str_nationality);

        if (str_gender.equals("Male")) {
            spinner_for_gender.setSelection(0);
        }else {
            spinner_for_gender.setSelection(1);
        }
    }
}
