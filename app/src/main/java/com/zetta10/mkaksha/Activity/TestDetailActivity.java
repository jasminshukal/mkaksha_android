package com.zetta10.mkaksha.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.zetta10.mkaksha.Adapter.MyAdapter_for_test_detail;
import com.zetta10.mkaksha.R;

public class TestDetailActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    LinearLayout finish_ch_list;
    TextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_detail);

        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        finish_ch_list = findViewById(R.id.finish_ch_list);
        toolbar_title = findViewById(R.id.toolbar_title);
        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        MyAdapter_for_test_detail myAdapter_for_test_detail = new MyAdapter_for_test_detail(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(myAdapter_for_test_detail);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
