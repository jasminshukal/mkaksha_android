package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Model.PrivacyModel.PrivacyModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditionActivity extends AppCompatActivity {

    LinearLayout finish_topic_list;
    //    TextView help;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    WebView web_data_load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.terms_condition_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        finish_topic_list = findViewById(R.id.finish_topic_list);
        web_data_load = findViewById(R.id.web_data_load);
//        help = findViewById(R.id.help);
        prefManager = new PrefManager(TermsConditionActivity.this);
        progressDialog = new ProgressDialog(TermsConditionActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        help_data();
        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    private void help_data() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<PrivacyModel> call = bookNPlayAPI.GetTerms(prefManager.gettokenId(), prefManager.getLoginId());
        call.enqueue(new Callback<PrivacyModel>() {
            @Override
            public void onResponse(Call<PrivacyModel> call, Response<PrivacyModel> response) {
//                Toast.makeText(PrivacyPolicyActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                view_comments();
                progressDialog.dismiss();
                web_data_load.setBackgroundColor(Color.TRANSPARENT);
                final WebSettings webSettings = web_data_load.getSettings();
                webSettings.setTextSize(WebSettings.TextSize.SMALLEST);
                webSettings.setDefaultFontSize(30);//Larger number means larger font size
                String data = response.body().getData();
                Log.e("web_data==>", "" + data);
                web_data_load.loadData(data, "text/html", "UTF-8");

            }

            @Override
            public void onFailure(Call<PrivacyModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
