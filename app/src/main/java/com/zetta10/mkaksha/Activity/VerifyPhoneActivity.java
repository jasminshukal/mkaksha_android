package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import java.util.concurrent.TimeUnit;

public class VerifyPhoneActivity extends AppCompatActivity {
    LinearLayout linear_login, ly_change_number, ly_resend_otp;
    TextView txt_number, txt_number_2;
    String mobile, fname, lname, email, phone, pass, con_pass, key;
    private String mVerificationId;
    //The edittext to input the code
    PrefManager prefManager;
    ProgressDialog progressDialog;
    private EditText editTextCode;
    //firebase auth object
    private FirebaseAuth mAuth;
    View view_number_line;
    //the callback to detect the verification status
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(VerifyPhoneActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
            mResendToken = forceResendingToken;
        }
    };
    TextView txt_change_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        //initializing objects
        progressDialog = new ProgressDialog(VerifyPhoneActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        mAuth = FirebaseAuth.getInstance();
        editTextCode = findViewById(R.id.editTextCode);
        view_number_line = findViewById(R.id.view_number_line);
        txt_change_number = findViewById(R.id.txt_change_number);
        linear_login = findViewById(R.id.linear_login);
        txt_number = findViewById(R.id.txt_number);
        txt_number_2 = findViewById(R.id.txt_number_2);
        ly_change_number = findViewById(R.id.ly_change_number);
        ly_resend_otp = findViewById(R.id.ly_resend_otp);
        prefManager = new PrefManager(VerifyPhoneActivity.this);
        //getting mobile number from the previous activity
        //and sending the verification code to the number
        Intent intent = getIntent();
        mobile = intent.getStringExtra("mobile");
        fname = intent.getStringExtra("finame");
        fname = intent.getStringExtra("sename");
        email = intent.getStringExtra("uemail");
        pass = intent.getStringExtra("upass");
        con_pass = intent.getStringExtra("uconpass");
        key = intent.getStringExtra("activity");
        if (key != null) {
            view_number_line.setVisibility(View.GONE);
            ly_change_number.setVisibility(View.GONE);
        }

        ly_change_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(VerifyPhoneActivity.this, Registration.class);
                Log.e("setnumbver==>", "" + mobile);
//                prefManager.setnumber("numbers", mobile);
               /* intent1.putExtra("mobile", mobile);
                intent1.putExtra("finame", fname);
                intent1.putExtra("sename", lname);
                intent1.putExtra("uemail", email);
                intent1.putExtra("upass", pass);
                intent1.putExtra("uconpass", con_pass);*/
                startActivity(intent1);
                finish();
            }
        });

        ly_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendVerificationCode(mobile, mResendToken);
            }
        });

        txt_number.setText(mobile);
        txt_number_2.setText(mobile);

        sendVerificationCode(mobile);

        //if the automatic sms detection did not work, user can also enter the code manually
        //so adding a click listener to the button
        findViewById(R.id.linear_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError("Enter valid code");
                    editTextCode.requestFocus();
                    return;
                }
                progressDialog.show();
                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });


    }

    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private void resendVerificationCode(String phoneNumber, PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                VerifyPhoneActivity.this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void verifyVerificationCode(String code) {
        /*//creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);*/
        try {

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            Toast toast = Toast.makeText(getApplicationContext(), "Verification Code is wrong, try again", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        try {
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(VerifyPhoneActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (key != null) {
                                if (task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                if (task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    //verification successful we will start the profile activity
                                    Intent intent = new Intent(VerifyPhoneActivity.this, AcademicActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                } else {
                                    progressDialog.dismiss();
//                                Toast.makeText(VerifyPhoneActivity.this, "Somthing is wrong, we will fix it soon...", Toast.LENGTH_LONG);
                                    //verification unsuccessful.. display an error message

                                    String message = "Somthing is wrong, we will fix it soon...";

                                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                        message = "Invalid code entered...";
                                    }

                                    Snackbar snackbar = Snackbar.make(findViewById(R.id.fab2), message, Snackbar.LENGTH_LONG);
                                    snackbar.setAction("Dismiss", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });
                                    snackbar.show();
                                }
                            }
                        }
                    });

        } catch (Exception e) {
            Log.e("err==>", "" + e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}
