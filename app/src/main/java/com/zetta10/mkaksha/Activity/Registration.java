package com.zetta10.mkaksha.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registration extends AppCompatActivity {

    TextView textView, txt_refer_code_generated;
    LinearLayout linearLayout_reg, linear_apply_refer, ly_close;
    EditText editText_fname, editText_lname, editText_email, editText_phone, editText_pass, editText_con_pass, edit_referral_code;
    String fname, lname, phone, email, pass, con_pass, referral_code, pass_refere;
    PrefManager prefManager;
    TextInputLayout txt_ed_refer;
    EditText ed_refer;
    String refere_code, referral_code_second;
    ProgressDialog progressDialog;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        MultiDex.install(this);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.registration_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });

        prefManager = new PrefManager(Registration.this);
        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        linearLayout_reg = findViewById(R.id.linear_for_next);
        txt_refer_code_generated = findViewById(R.id.txt_refer_code_generated);
        editText_fname = findViewById(R.id.edit_first_name);
        editText_lname = findViewById(R.id.edit_lastname);
        editText_email = findViewById(R.id.edit_email);
        editText_phone = findViewById(R.id.edit_phone);
        editText_pass = findViewById(R.id.edit_pass);
        editText_con_pass = findViewById(R.id.edit_con_pass);
        edit_referral_code = findViewById(R.id.edit_referral_code);
        txt_ed_refer = findViewById(R.id.txt_ed_refer);

        txt_refer_code_generated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent intent = new Intent(Registration.this, ReferelCodeActivity.class);
                prefManager.setValue("u_fname", editText_fname.getText().toString().trim());
                prefManager.setValue("u_lname", editText_lname.getText().toString().trim());
                prefManager.setValue("u_email", editText_email.getText().toString().trim());
                prefManager.setValue("u_phone", editText_phone.getText().toString().trim());
                prefManager.setValue("u_pass", editText_pass.getText().toString().trim());
                prefManager.setValue("u_con_pass", editText_con_pass.getText().toString().trim());

                Log.e("fname==>", "" + editText_fname.getText().toString().trim());
                Log.e("fname==>", "" + editText_lname.getText().toString().trim());
                Log.e("fname==>", "" + editText_phone.getText().toString().trim());
                Log.e("fname==>", "" + editText_email.getText().toString().trim());
                Log.e("fname==>", "" + editText_pass.getText().toString().trim());
                Log.e("fname==>", "" + editText_con_pass.getText().toString().trim());
                startActivity(intent);
                finish();*/
                dialog = new Dialog(Registration.this);
                dialog.setContentView(R.layout.referel_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ed_refer = dialog.findViewById(R.id.ed_refer);
                linear_apply_refer = dialog.findViewById(R.id.linear_apply_refer);
                ly_close = dialog.findViewById(R.id.ly_close);

                dialog.show();

                linear_apply_refer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        refere_code = ed_refer.getText().toString().trim();
                        if (TextUtils.isEmpty(refere_code)) {
                            Toast.makeText(getApplicationContext(), "Enter Refere_code", Toast.LENGTH_LONG).show();
                            return;
                        }
                        generate_code_check(refere_code);

                    }
                });

                ly_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
            }
        });
        linearLayout_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fname = editText_fname.getText().toString().trim();
                lname = editText_lname.getText().toString().trim();
                phone = editText_phone.getText().toString().trim();
                email = editText_email.getText().toString().trim();
                pass = editText_pass.getText().toString().trim();
                con_pass = editText_con_pass.getText().toString().trim();
                pass_refere = edit_referral_code.getText().toString().trim();

                if (TextUtils.isEmpty(fname)) {
                    Toast.makeText(getApplicationContext(), "Enter First name", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(lname)) {
                    Toast.makeText(getApplicationContext(), "Enter last name", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter Email", Toast.LENGTH_LONG).show();
                    return;
                }
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {

                } else {
                    Toast.makeText(Registration.this, "Enter valid email Address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(getApplicationContext(), "Enter Phone number", Toast.LENGTH_LONG).show();
                    return;
                } else if (phone.length() < 10) {
                    Toast.makeText(getApplicationContext(), "Enter valid Phone number", Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(pass)) {
                    Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pass.length() < 8) {
                    Toast.makeText(Registration.this, "Enter Password must be minimum 8 character", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(con_pass)) {
                    Toast.makeText(getApplicationContext(), "Enter Confirm Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!pass.equals(con_pass)) {
                    Toast.makeText(Registration.this, "Does not Password match", Toast.LENGTH_SHORT).show();
                    return;
                }

                prefManager.setValue("fname", fname);
                prefManager.setValue("lname", lname);
                prefManager.setValue("email", email);
                prefManager.setValue("phone", phone);
                prefManager.setValue("pass", pass);
                prefManager.setValue("con_pass", con_pass);
                prefManager.setValue("pass_refere", pass_refere);




              /*  Intent intent = new Intent(getApplicationContext(), PhoneAuthActivity.class);
                startActivity(intent);*/
                String mobile = editText_phone.getText().toString().trim();
                Intent intent = new Intent(Registration.this, VerifyPhoneActivity.class);
                intent.putExtra("mobile", mobile);
                intent.putExtra("finame", fname);
                intent.putExtra("sename", lname);
                intent.putExtra("uemail", email);
                intent.putExtra("upass", pass);
                intent.putExtra("uconpass", con_pass);
                intent.putExtra("referral_code", referral_code);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        textView = findViewById(R.id.text_signin);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("data", "");
        Bundle bundle = getIntent().getExtras();
        /*if (bundle != null) {
            txt_refer_code_generated.setVisibility(View.GONE);
            txt_ed_refer.setVisibility(View.VISIBLE);
            referral_code = bundle.getString("referral_code_second");
//            prefManager.getValue("referral_code");
            edit_referral_code.setText(referral_code);

            fname = bundle.getString("s_fname");
            lname = bundle.getString("s_lname");
            email = bundle.getString("s_email");
            phone = bundle.getString("s_mobile");
            pass = bundle.getString("s_pass");
            con_pass = bundle.getString("s_con_pass");

            editText_fname.setText(fname);
            editText_lname.setText(lname);
            editText_email.setText(email);
            editText_phone.setText(phone);
            editText_pass.setText(pass);
            editText_con_pass.setText(con_pass);
        }*/

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Bundle bundle = getIntent().getExtras();
        /*if (bundle != null) {
            txt_refer_code_generated.setVisibility(View.GONE);
            txt_ed_refer.setVisibility(View.VISIBLE);
            referral_code = bundle.getString("referral_code_second");
//            prefManager.getValue("referral_code");
            edit_referral_code.setText(referral_code);

            fname = bundle.getString("s_fname");
            lname = bundle.getString("s_lname");
            email = bundle.getString("s_email");
            phone = bundle.getString("s_mobile");
            pass = bundle.getString("s_pass");
            con_pass = bundle.getString("s_con_pass");

            editText_fname.setText(fname);
            editText_lname.setText(lname);
            editText_email.setText(email);
            editText_phone.setText(phone);
            editText_pass.setText(pass);
            editText_con_pass.setText(con_pass);
        }*/

    }


    private void generate_code_check(String refere_code) {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.CheckReferralCode(refere_code);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

//                    edit_referral_code.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");
                    if (status.equals("1")) {
                        txt_refer_code_generated.setVisibility(View.GONE);
                        txt_ed_refer.setVisibility(View.VISIBLE);
                        referral_code_second = jsonObject.getJSONObject("data").getString("referral_code");
                        String agent_id = jsonObject.getJSONObject("data").getString("agent_id");
                        String agent_name = jsonObject.getJSONObject("data").getString("agent_name");
                        String occupation = jsonObject.getJSONObject("data").getString("occupation");
                        Log.e("msg==>", "" + msg);
                        Toast.makeText(Registration.this, msg, Toast.LENGTH_LONG).show();

                        edit_referral_code.setText(referral_code_second);
                        dialog.cancel();
                        /*if (referral_code_second.equals(refere_code)) {
                            Toast.makeText(ReferelCodeActivity.this, "" + msg, Toast.LENGTH_LONG).show();
                        }*/
                    /*    Intent intent = new Intent(Registration.this, Registration.class);
                        Log.e("refer_code==>", "" + referral_code_second);
                        intent.putExtra("referral_code_second", referral_code_second);
                        intent.putExtra("s_fname", fname);
                        intent.putExtra("s_lname", lname);
                        intent.putExtra("s_email", email);
                        intent.putExtra("s_mobile", mobile);
                        intent.putExtra("s_pass", pass);
                        intent.putExtra("s_con_pass", con_pass);
                        startActivity(intent);
                        finish();*/
                    } else if (status.equals("0")) {
                        Toast.makeText(Registration.this, "" + msg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("==>", "" + e.getMessage());
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

      /*  finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });*/
    }

}
