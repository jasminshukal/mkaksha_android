package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MegaTestResultActivity extends AppCompatActivity {

    TextView quizResult;
    Button finish_quiz;
    String quizId, chapId, subId;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    LinearLayout finish_topic_list;
    String login_token, user_id, test_id;
    int correct_ans, wrong_ans, attend_question;
    String attempt_question, duration, duration_test;
    String hour;
    String minute;
    String sec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.megatest_result_activity);
        init();
        progressDialog = new ProgressDialog(MegaTestResultActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(MegaTestResultActivity.this);
        login_token = prefManager.getValue("login_token");
        user_id = prefManager.getValue("user_id");
        Bundle bundle = getIntent().getExtras();
        /* if (bundle != null) {*/
        // quizId = bundle.getString("quizId");
        int correct = getIntent().getIntExtra("correct", 0);
        int wrong = getIntent().getIntExtra("wrong", 0);
        test_id = getIntent().getStringExtra("test_id");
        attempt_question = String.valueOf(getIntent().getIntExtra("attempt", 0));
        hour = String.valueOf(getIntent().getLongExtra("test_hour", 0));
        minute = String.valueOf(getIntent().getLongExtra("test_minute", 0));
        sec = String.valueOf(getIntent().getLongExtra("test_sec", 0));
        duration_test = (hour + ":" + minute + ":" + sec);
        correct_ans = correct;
        wrong_ans = wrong;
        quizResult.setText("Correct Answers : " + correct + "\n" + "Wrong Answers : " + wrong + "\n" + "Attempted Question : " + attempt_question + "\n" + "Duration : " + duration_test);
        submit_test();
        finish_quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MegaTestResultActivity.this.finish();
            }
        });

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void submit_test() {
        try {
            progressDialog.show();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<ResponseBody> call_submit_test = apiinterface.submit_test(login_token, user_id, test_id, duration_test, correct_ans, wrong_ans, attend_question);

            call_submit_test.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String status = jsonObject.getString("status");
                        if (status.equals("1")) {
                            String msg = jsonObject.getString("msg");
                            Toast.makeText(MegaTestResultActivity.this, "" + msg, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
        }
    }

    private void init() {
        quizResult = findViewById(R.id.quizResult);
        finish_quiz = findViewById(R.id.finish_quiz);
        finish_topic_list = findViewById(R.id.finish_topic_list);
    }
}
