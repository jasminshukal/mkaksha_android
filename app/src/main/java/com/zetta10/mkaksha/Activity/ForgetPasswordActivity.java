package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText editText;
    LinearLayout linearLayout_submit;
    String number;
    String key = "forget_activity";
    String status, user_id, api_token;
    PrefManager prefManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.forget_password_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        editText = findViewById(R.id.edit_username);
        linearLayout_submit = findViewById(R.id.linear_submit);
        prefManager = new PrefManager(getApplicationContext());
        progressDialog = new ProgressDialog(ForgetPasswordActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        linearLayout_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                number = editText.getText().toString();

                if (TextUtils.isEmpty(number)) {
                    Toast.makeText(getApplicationContext(), "Enter Phone number", Toast.LENGTH_LONG).show();
                    return;
                } else if (number.length() < 10) {
                    Toast.makeText(getApplicationContext(), "Enter valid Phone number", Toast.LENGTH_LONG).show();
                    return;
                }
                get_phone_detail();

            }
        });
    }

    public void get_phone_detail() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> phonecall = apiinterface.getphonenumber(number);

        phonecall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = String.valueOf(jsonObject.getString("msg"));
                    Toast.makeText(ForgetPasswordActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    status = jsonObject.getString("status");
                    user_id = jsonObject_data.getString("id");
                    api_token = jsonObject_data.getString("api_token");
                    prefManager.setValue("user_id", user_id);
                    if (status.equals("3")) {
                        Intent intent = new Intent(getApplicationContext(), PopupActivity.class);
                        intent.putExtra("status", status);
                        startActivity(intent);
                        //   Toast.makeText(LoginActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                    }
                    if (status.equals("4")) {
                        Intent intent = new Intent(getApplicationContext(), PopupActivity.class);
                        intent.putExtra("status", status);
                        startActivity(intent);
                        //   Toast.makeText(LoginActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                    }

                    prefManager.setValue("api_key", api_token);
                    if (status.equals("1")) {
                        Intent intent = new Intent(getApplicationContext(), VerifyPhoneActivity.class);
                        intent.putExtra("mobile", number);
                        intent.putExtra("activity", key);
                        startActivity(intent);
                        finish();
                    }

                    //  Toast.makeText(LoginActivity.this, ""+jsonObject, Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
