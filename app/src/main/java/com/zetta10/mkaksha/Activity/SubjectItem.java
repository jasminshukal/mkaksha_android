package com.zetta10.mkaksha.Activity;

public class SubjectItem {

    public SubjectItem(String percentage, String sub_name) {
        this.percentage = percentage;
        this.sub_name = sub_name;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public String getCorrect() {
        return correct;
    }

    private String percentage, sub_name, answer2, answer3, answer4, correct;

}
