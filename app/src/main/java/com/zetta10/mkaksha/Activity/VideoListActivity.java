package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Adapter.VideoListAdapter;
import com.zetta10.mkaksha.Model.VideoListModel.Data;
import com.zetta10.mkaksha.Model.VideoListModel.VideoListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoListActivity extends AppCompatActivity {

    Integer topic_video_id;
    String v_topic_name;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    VideoListAdapter videoListAdapter;
    RecyclerView rv_video_list;
    List<Data> video_dataList;
    String api_token, user_id;
    TextView toolbar_title;
    LinearLayout finish_video_list,ly_dataNotFound;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.video_list_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        init();
        prefManager = new PrefManager(VideoListActivity.this);
        progressDialog = new ProgressDialog(VideoListActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            topic_video_id = bundle.getInt("v_topic_id");
            v_topic_name = bundle.getString("v_topic_name");
            Log.e("chapter_id", "" + topic_video_id);
            Log.e("chapter_name", "" + v_topic_name);

            Video_list(topic_video_id);
            toolbar_title.setText(v_topic_name);
        }

        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void init() {
        rv_video_list = findViewById(R.id.rv_video_list);
        toolbar_title = findViewById(R.id.toolbar_title);
        finish_video_list = findViewById(R.id.finish_video_list);
        ly_dataNotFound = findViewById(R.id.ly_dataNotFound);
    }

    private void Video_list(Integer topic_video_id) {
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();

            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<VideoListModel> call = appAPI.video_list_call(api_token, user_id, topic_video_id);
            call.enqueue(new Callback<VideoListModel>() {
                @Override
                public void onResponse(Call<VideoListModel> call, Response<VideoListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        video_dataList = new ArrayList<>();
                        video_dataList = response.body().getData();
                        Log.e("dataList==>", "" + video_dataList.size());
                        if (video_dataList.size() > 0) {
                            videoListAdapter = new VideoListAdapter(VideoListActivity.this, video_dataList);
                            rv_video_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(VideoListActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_video_list.setLayoutManager(mLayoutManager3);
                            rv_video_list.setItemAnimator(new DefaultItemAnimator());
                            rv_video_list.setAdapter(videoListAdapter);
                            videoListAdapter.notifyDataSetChanged();
                            rv_video_list.setVisibility(View.VISIBLE);
                            rv_video_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_video_list.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<VideoListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(progressDialog != null)
            progressDialog .dismiss();
    }

}
