package com.zetta10.mkaksha.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Adapter.Adapter_for_notification;
import com.zetta10.mkaksha.Model.NotificationModel.Pojo_for_notification;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.Session_for_notification;

import java.util.ArrayList;

public class Notification_display extends AppCompatActivity {

    RecyclerView recyclerview;
    SharedPreferences sharedPreferences;
    int title_key = 0, desc_key = 0;
    String title, desc;
    ArrayList<Pojo_for_notification> notificationArrayList;
    Session_for_notification session;
    TextView textView_for_back;
    LinearLayout finish_notification_list,ly_dataNotFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_display);

     //   String  key = String.valueOf(getIntent().getExtras().keySet());
        session = new Session_for_notification(getApplicationContext());
        title_key = session.getTitle_key();
        desc_key = session.getDesc_key();
        notificationArrayList = new ArrayList<>();
        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        finish_notification_list = findViewById(R.id.finish_notification_list);
        ly_dataNotFound = findViewById(R.id.ly_dataNotFound);
        finish_notification_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(layoutManager);
       /* sharedPreferences = getSharedPreferences("shared_sec_pref", Context.MODE_PRIVATE);
        title_key = sharedPreferences.getInt("title_key", 0);*/

        for (int i = -1; i < title_key; i++) {
            title = session.getTitle(i);
            desc = session.getDesc(i);
            Pojo_for_notification pojo_for_notification = new Pojo_for_notification();
            pojo_for_notification.setTitle(title);
            pojo_for_notification.setDescription(desc);
            /* if (!(title.equals("") && desc.equals(""))) {*/
            notificationArrayList.add(pojo_for_notification);
            /*}*/
        }

        if (notificationArrayList.size() != 0) {
            Adapter_for_notification adapter_for_notification = new Adapter_for_notification(getApplicationContext(), notificationArrayList);
            recyclerview.setAdapter(adapter_for_notification);
        }else {
            recyclerview.setVisibility(View.GONE);
            ly_dataNotFound.setVisibility(View.VISIBLE);
        }
    }
}
