package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.BuildConfig;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferedActivity extends AppCompatActivity {

    LinearLayout ly_referd_share, linear_refer, finish_topic_list;
    TextView txt_refer, txt_copy;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    String referral_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refere_activity);
        prefManager = new PrefManager(ReferedActivity.this);
        progressDialog = new ProgressDialog(ReferedActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        init();

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        linear_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generate_code_call();
                txt_copy.setVisibility(View.VISIBLE);
                ly_referd_share.setVisibility(View.VISIBLE);
            }
        });


        txt_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!referral_code.isEmpty()) {
                        ClipboardManager cm = (ClipboardManager) ReferedActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                        cm.setText(txt_refer.getText());
                        Toast.makeText(ReferedActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ReferedActivity.this, "Not Any text", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("data==>", "" + e.getMessage());
                }
            }
        });

        ly_referd_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!referral_code.isEmpty()) {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                        String shareMessage = "\n" + "Refer code : " + referral_code + "\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id="
                                + BuildConfig.APPLICATION_ID + "\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } else {
                        Toast.makeText(ReferedActivity.this, "Please Generated Code then Share", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });
    }


    private void generate_code_call() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.GenerateReferralCode(prefManager.gettokenId(), prefManager.getLoginId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String success = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");
                    referral_code = jsonObject.getJSONObject("data").getString("referral_code");
                    prefManager.setValue("referral_code",referral_code);
                    String agent_id = jsonObject.getJSONObject("data").getString("agent_id");
                    String agent_name = jsonObject.getJSONObject("data").getString("agent_name");
                    String occupation = jsonObject.getJSONObject("data").getString("occupation");
//                    jsonObject.getJSONObject("data").getString("referral_code");
                    Log.e("msg==>", "" + msg);
                    Log.e("referral_code==>", "" + referral_code);
                    txt_refer.setText(referral_code);
                    Toast.makeText(ReferedActivity.this, msg, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {

                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }

    private void init() {
        ly_referd_share = findViewById(R.id.ly_referd_share);
        linear_refer = findViewById(R.id.linear_refer);
        finish_topic_list = findViewById(R.id.finish_topic_list);

        txt_refer = findViewById(R.id.txt_refer);
        txt_copy = findViewById(R.id.txt_copy);
    }
}
