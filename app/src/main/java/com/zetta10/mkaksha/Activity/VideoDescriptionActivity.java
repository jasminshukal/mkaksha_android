package com.zetta10.mkaksha.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.TrackSelectionView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Adapter.PdfViewAdapter;
import com.zetta10.mkaksha.BuildConfig;
import com.zetta10.mkaksha.Model.CommentModel.CommentModel;
import com.zetta10.mkaksha.Model.PdfViewerModel.PdfViewerModel;
import com.zetta10.mkaksha.Model.VideoDesListModel.Data;
import com.zetta10.mkaksha.Model.VideoDesListModel.VideoDesListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ir.androidexception.andexalertdialog.AndExAlertDialog;
import ir.androidexception.andexalertdialog.AndExAlertDialogListener;
import ir.androidexception.andexalertdialog.Font;
import ir.androidexception.andexalertdialog.InputType;
import okhttp3.Cache;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoDescriptionActivity extends AppCompatActivity implements ExoPlayer.EventListener {


    //    PlayerView playerView;
    String video_url;
    List<Data> video_desc_dataList;
    List<com.zetta10.mkaksha.Model.PdfViewerModel.Data> pdf_dataList;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    Integer video_id;
    //    private SimpleExoPlayer simpleExoPlayer;
    String api_token, user_id;
    SeekBar seekBar;
    String video_path, video_heading, video_desc, topic_id, video_name, quize_id;
    String img_path;
    TextView textView_video_heading, textView_video_desc, textView_content, toolbar_title, exo_cur_position;
    ImageView image_like, imageView_comment, image_favorite, imageView_share, exo_forward, exo_backward, image_for_setting;
    int timeRemaining, sec, min, hour;
    long timeElapsed, finalTime;
    PlayerView playerView;
    SimpleExoPlayer simpleExoPlayer;
    DataSource.Factory mediaDataSorceFactory;
    LinearLayout finish_video_list, ly_dataNotFound;
    ImageView exo_fullscreen;
    private Dialog mFullScreenDialog;
    private boolean mExoPlayerFullscreen = false;
    ProgressBar progressBar;
    Long position, time, check_time;
    String status;
    MediaSource videoSource;
    private static final String PDF_DIRECTORY = "/MKaksha";
    String read_path, read_name;
    RecyclerView rv_pdf_name;
    PdfViewAdapter pdfViewAdapter;
    int flag = 1;
    boolean intentLaunched = false;
    private DefaultTrackSelector trackSelector;
    ExoPlayer exoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.video_description_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });

        prefManager = new PrefManager(VideoDescriptionActivity.this);
        progressDialog = new ProgressDialog(VideoDescriptionActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        textView_content = findViewById(R.id.textview_content);
        progressBar = findViewById(R.id.progressBar);
        exo_fullscreen = findViewById(R.id.exo_fullscreen_icon);
        textView_video_desc = findViewById(R.id.textview_video_desc);
        textView_video_heading = findViewById(R.id.textview_heading);
        finish_video_list = findViewById(R.id.finish_video_list);
        image_for_setting = findViewById(R.id.image_for_setting);
        exo_cur_position = findViewById(R.id.exo_cur_position);
        playerView = findViewById(R.id.playerView);
        seekBar = findViewById(R.id.seekbar);
        imageView_comment = findViewById(R.id.image_comment);
        image_favorite = findViewById(R.id.image_favorite);
        image_like = findViewById(R.id.image_like);
        imageView_share = findViewById(R.id.image_share);
        exo_forward = findViewById(R.id.exo_forward);
        exo_backward = findViewById(R.id.exo_backward);
        toolbar_title = findViewById(R.id.toolbar_title);

        Handler mHandler = new Handler();
        //Make sure you update Seekbar on UI thread
        VideoDescriptionActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (simpleExoPlayer != null) {
                    seekBar.setMax((int) (simpleExoPlayer.getDuration() / 1000));
                    long mCurrentPosition = simpleExoPlayer.getCurrentPosition() / 1000;
                    int current_pos = (int) mCurrentPosition;
                    seekBar.setProgress(current_pos);
                }
                mHandler.postDelayed(this, 1000);
                //  durationHandler.postDelayed(updateSeekBarTime,1000);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                String duration = String.valueOf(i);
                int temp_sec = 0;
                if (i <= 60) {
                    temp_sec = i;
                }
                sec = i * 1000;
                min = sec / 60;
                hour = min / 60;
                finalTime = simpleExoPlayer.getDuration();
                timeElapsed = simpleExoPlayer.getCurrentPosition();
                timeRemaining = (int) (finalTime - timeElapsed);

                long minute = TimeUnit.MILLISECONDS.toMinutes((long) sec);
                long second = TimeUnit.MILLISECONDS.toSeconds((long) sec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) sec));
               /* convertDate(minute);
                convertDate(second);*/
                //  exo_cur_position.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) sec), TimeUnit.MILLISECONDS.toSeconds((long) sec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) sec))));
                exo_cur_position.setText(convertDate(minute) + ":" + convertDate(second));
                // simpleExoPlayer.seekTo(i * 1000);
                //  seekBar.setProgress(i*100);
            }

            public String convertDate(long input) {
                if (input >= 10) {
                    return String.valueOf(input);
                } else {
                    return "0" + String.valueOf(input);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // int progress = seekBar.getProgress();
                // simpleExoPlayer.seekTo(progress*1000);
                //  seekBar.setProgress(seekBar.getProgress());
                // simpleExoPlayer.seekTo(i * 1000);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                simpleExoPlayer.seekTo(progress * 1000);
                //exo_position.setText();
            }
        });

        image_for_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*trackSelector = new DefaultTrackSelector();*/
                Dialog dialog = new Dialog(VideoDescriptionActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_for_exo_setting);
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                LinearLayout linear_for_quality = dialog.findViewById(R.id.linear_for_quality);
                LinearLayout linear_for_playback_speed = dialog.findViewById(R.id.linear_for_playback_speed);
                linear_for_quality.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                        if (mappedTrackInfo != null) {
                            CharSequence title = "Select Category";
                            int rendererIndex = 0; // renderer for video
                            int rendererType = mappedTrackInfo.getRendererType(rendererIndex);
                            boolean allowAdaptiveSelections =
                                    rendererType == C.TRACK_TYPE_VIDEO
                                            || (rendererType == C.TRACK_TYPE_AUDIO
                                            && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                                            == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);
                            Pair<AlertDialog, TrackSelectionView> dialogPair =
                                    TrackSelectionView.getDialog(VideoDescriptionActivity.this, title, trackSelector, rendererIndex);
                            dialogPair.second.setShowDisableOption(false);
                            dialogPair.second.setAllowAdaptiveSelections(allowAdaptiveSelections);
                            dialogPair.first.show();
                            dialogPair.second.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Toast.makeText(VideoDescriptionActivity.this, "" + exoPlayer.getCurrentTrackGroups(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });

                linear_for_playback_speed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Dialog dialog_for_play_speed = new Dialog(VideoDescriptionActivity.this);
                        dialog_for_play_speed.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog_for_play_speed.setContentView(R.layout.layout_for_playback_speed);
                        // dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,250);
                        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        LinearLayout layout_for_1_25x = dialog_for_play_speed.findViewById(R.id.layout_for_1_25x);
                        LinearLayout layout_for_1_5x = dialog_for_play_speed.findViewById(R.id.layout_for_1_5x);
                        LinearLayout layout_for_1_75x = dialog_for_play_speed.findViewById(R.id.layout_for_1_75x);
                        LinearLayout layout_for_2x = dialog_for_play_speed.findViewById(R.id.layout_for_2x);
                        LinearLayout layout_for_normal_speed = dialog_for_play_speed.findViewById(R.id.layout_for_normal_speed);

                        layout_for_normal_speed.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                float speed = 1f;
                                PlaybackParameters param = new PlaybackParameters(speed);
                                simpleExoPlayer.setPlaybackParameters(param);
                                dialog_for_play_speed.dismiss();
                            }
                        });

                        layout_for_1_25x.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                float speed = 1.25f;
                                PlaybackParameters param = new PlaybackParameters(speed);
                                simpleExoPlayer.setPlaybackParameters(param);
                                dialog_for_play_speed.dismiss();
                            }
                        });

                        layout_for_1_5x.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                float speed = 1.5f;
                                PlaybackParameters param = new PlaybackParameters(speed);
                                simpleExoPlayer.setPlaybackParameters(param);
                                dialog_for_play_speed.dismiss();
                            }
                        });

                        layout_for_1_75x.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                float speed = 1.75f;
                                PlaybackParameters param = new PlaybackParameters(speed);
                                simpleExoPlayer.setPlaybackParameters(param);
                                dialog_for_play_speed.dismiss();
                            }
                        });

                        layout_for_2x.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                float speed = 2f;
                                PlaybackParameters param = new PlaybackParameters(speed);
                                simpleExoPlayer.setPlaybackParameters(param);
                                dialog_for_play_speed.dismiss();
                            }
                        });

                        dialog_for_play_speed.show();
                        Window window = dialog_for_play_speed.getWindow();
                        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    }
                });

                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            video_id = bundle.getInt("video_id");
            video_name = bundle.getString("video_data");
            Log.e("video_ids==>", "" + video_id);
//            Video_deatils_get(video_id);
            toolbar_title.setText(video_name);
        }
        if (bundle != null) {
            video_id = bundle.getInt("video_id");
            Log.e("video_id", "" + video_id);

            Video_deatils_get(video_id);
            VideoWatchList();
        }


        exo_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time = simpleExoPlayer.getDuration();
                position = simpleExoPlayer.getCurrentPosition();
                check_time = time - 15000;
                if (position != null) {
                    if (check_time > position) {
                        simpleExoPlayer.seekTo(position + 15000);
                    }
                }
            }
        });


        exo_backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = simpleExoPlayer.getCurrentPosition();
                if (position != null) {
                    if (position > 15000) {
                        if (position != C.TIME_UNSET)
                            simpleExoPlayer.seekTo(position - 15000);
                    }
                }
            }
        });

        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        exo_fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!mExoPlayerFullscreen) {
                        openFullscreenDialog();
                    } else {
                        closeFullscreenDialog();
                    }
                } catch (Exception e) {
                    Log.e("fullscreenerr==>", "" + e.getMessage());
                }

            }
        });

        textView_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(VideoDescriptionActivity.this);
                dialog.setContentView(R.layout.content_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                rv_pdf_name = dialog.findViewById(R.id.rv_pdf_name);
                ly_dataNotFound = dialog.findViewById(R.id.ly_dataNotFound);
               /* LinearLayout linearLayout_for_content_pdf = dialog.findViewById(R.id.linear_for_content_pdf);
                LinearLayout linearLayout_for_practice_pdf = dialog.findViewById(R.id.linear_for_practice_pdf);
                LinearLayout linearLayout_for_imp_pdf = dialog.findViewById(R.id.linear_for_imp_pdf);*/

                /*linearLayout_for_content_pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("ckick_content_pdf", "content_pdf");
//                        DownloadBook();

                        Intent i1 = new Intent(VideoDescriptionActivity.this, PdfViewer.class);
                        i1.putExtra("read", read_path);
                        i1.putExtra("name", read_name);
                        startActivity(i1);
                    }
                });

                linearLayout_for_practice_pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("ckick_practice_pdf", "practice_pdf");
                        Intent i1 = new Intent(VideoDescriptionActivity.this, PdfViewer.class);
                        i1.putExtra("read", read_path);
                        i1.putExtra("name", read_name);
                        startActivity(i1);
                    }
                });

                linearLayout_for_imp_pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("ckick_practice_pdf", "practice_pdf");
                        Intent i1 = new Intent(VideoDescriptionActivity.this, PdfViewer.class);
                        i1.putExtra("read", read_path);
                        i1.putExtra("name", read_name);
                        startActivity(i1);
                    }
                });*/

                pdf_data_call();
                ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
                dialog.show();
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        imageView_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    String referral_code = prefManager.getValue("referral_code");
                    if (!referral_code.equals("0")) {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        String shareMessage1 = "\n" + "Refer code : " + referral_code + "\n\n";
                        shareMessage = shareMessage + shareMessage1 + "https://play.google.com/store/apps/details?id="
                                + BuildConfig.APPLICATION_ID + "\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } else {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id="
                                + BuildConfig.APPLICATION_ID + "\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    }
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });

        image_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prefManager.getLoginId().equalsIgnoreCase("0"))
                    watch_list();
                else
                    startActivity(new Intent(VideoDescriptionActivity.this, LoginActivity.class));

            }
        });

        image_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prefManager.getLoginId().equalsIgnoreCase("0"))
                    add_like();
                else
                    startActivity(new Intent(VideoDescriptionActivity.this, LoginActivity.class));

            }
        });


        imageView_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prefManager.getLoginId().equalsIgnoreCase("0"))
                    AddComment_Popup();
                else
                    startActivity(new Intent(VideoDescriptionActivity.this, LoginActivity.class));
            }
        });

        initFullscreenDialog();

//        pdf_data_call();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();

        if (orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270) {
            mFullScreenDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            ((ViewGroup) playerView.getParent()).removeView(playerView);
            mFullScreenDialog.addContentView(playerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            exo_fullscreen.setImageDrawable(ContextCompat.getDrawable(VideoDescriptionActivity.this, R.drawable.fullscreen_out));
            mExoPlayerFullscreen = true;
            mFullScreenDialog.show();
            // TODO: add logic for landscape mode here
        } else if (orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180) {
            mFullScreenDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ((ViewGroup) playerView.getParent()).removeView(playerView);
            ((FrameLayout) findViewById(R.id.root)).addView(playerView);
            mExoPlayerFullscreen = false;
            mFullScreenDialog.dismiss();
            exo_fullscreen.setImageDrawable(ContextCompat.getDrawable(VideoDescriptionActivity.this, R.drawable.fullscreen_in));
        }
    }

    private void VideoWatchList() {
        try {
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<ResponseBody> call = appAPI.SubmitWatch(prefManager.gettokenId(), prefManager.getLoginId(), video_id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = String.valueOf(jsonObject.get("msg"));
                        Log.e("msg", "" + msg);
                        Log.e("data=>", "" + jsonObject.getJSONObject("data").getString("user_id"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }

    }

    private void pdf_data_call() {
        try {
            Log.e("daatain=>", "" + prefManager.gettokenId());
            Log.e("user_id=>", "" + user_id);
            Log.e("video_id=>", "" + video_id);
            AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
            Call<PdfViewerModel> call = bookNPlayAPI.GetAttachments(prefManager.gettokenId(), prefManager.getLoginId(), video_id);
            call.enqueue(new Callback<PdfViewerModel>() {
                @Override
                public void onResponse(Call<PdfViewerModel> call, Response<PdfViewerModel> response) {
                    Log.e("daatain=>", "inn");
                    if (response.code() == 200) {
                        Log.e("daata=>", "" + response.body().getData());
//                        progressDialog.dismiss();

                        pdf_dataList = new ArrayList<>();
                        pdf_dataList = response.body().getData();

                       /* Log.e("pdf_data", "" + pdf_dataList.get(0).getPath());
                        read_path = pdf_dataList.get(0).getPath();
                        read_name = pdf_dataList.get(0).getName();*/

                        if (response.isSuccessful()) {
                            Log.e("data1", "" + response.body());
                            Log.e("data2", "" + response.body().getData());
                            pdf_dataList = new ArrayList<>();
                            pdf_dataList = response.body().getData();
                            Log.e("dataList==>", "" + pdf_dataList.size());
                            if (pdf_dataList.size() > 0) {
                                pdfViewAdapter = new PdfViewAdapter(VideoDescriptionActivity.this, pdf_dataList);
                                rv_pdf_name.setHasFixedSize(true);
                                RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(VideoDescriptionActivity.this,
                                        LinearLayoutManager.VERTICAL, false);

                                rv_pdf_name.setLayoutManager(mLayoutManager3);
                                rv_pdf_name.setItemAnimator(new DefaultItemAnimator());
                                rv_pdf_name.setAdapter(pdfViewAdapter);
                                pdfViewAdapter.notifyDataSetChanged();
                                rv_pdf_name.setVisibility(View.VISIBLE);
                                rv_pdf_name.setVisibility(View.VISIBLE);
                            } else {
                                rv_pdf_name.setVisibility(View.GONE);
                                ly_dataNotFound.setVisibility(View.VISIBLE);
                            }
                        }

                    }

                }

                @Override
                public void onFailure(Call<PdfViewerModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("errors==>", "" + t.getMessage());
                }
            });

        } catch (Exception e) {
            Log.e("pdf_error==>", "" + e.getMessage());
        }
    }

    private void DownloadBook() {
        Log.e("book_url==>", "" + pdf_dataList.get(0).getPath());
        new DownloadBook("save").execute(pdf_dataList.get(0).getPath());
    }


    public class DownloadBook extends AsyncTask<String, String, String> {
        private ProgressDialog pDialog;
        URL myFileUrl;
        String option;
        File file;

        DownloadBook(String option) {
            this.option = option;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(VideoDescriptionActivity.this, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.downloading));
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            try {
                myFileUrl = new URL(args[0]);
                String path = myFileUrl.getPath();
                String fileName = path.substring(path.lastIndexOf('/') + 1);
                //   File dir = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name) + "/");
                File dir = new File(Environment.getExternalStorageDirectory() + "" + PDF_DIRECTORY);
//                dir.mkdirs();
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                file = new File(dir, fileName);

                if (!file.exists()) {
                    HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    FileOutputStream fos = new FileOutputStream(file);
                    byte data[] = new byte[4096];
                    int count;
                    while ((count = is.read(data)) != -1) {
                        if (isCancelled()) {
                            is.close();
                            return null;
                        }
                        fos.write(data, 0, count);
                    }
                    fos.flush();
                    fos.close();

                    if (option.equals("save")) {
                        MediaScannerConnection.scanFile(VideoDescriptionActivity.this, new String[]{file.getAbsolutePath()},
                                null,
                                new MediaScannerConnection.OnScanCompletedListener() {
                                    @Override
                                    public void onScanCompleted(String path, Uri uri) {

                                    }
                                });
                    }
                    return "1";
                } else {
                    return "2";
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", "" + e.getMessage());
                return "0";
            }
        }

        @Override
        protected void onPostExecute(String args) {
            if (args.equals("1") || args.equals("2")) {
                switch (option) {
                    case "save":
                        if (args.equals("2")) {
                            Toast.makeText(VideoDescriptionActivity.this, "Already Download", Toast.LENGTH_SHORT).show();
                        } else {
//                            AddDownload();
                            Toast.makeText(VideoDescriptionActivity.this, "Download success", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } else {
                Toast.makeText(VideoDescriptionActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();
        }
    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        mFullScreenDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        mFullScreenDialog.addContentView(playerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        exo_fullscreen.setImageDrawable(ContextCompat.getDrawable(VideoDescriptionActivity.this, R.drawable.fullscreen_out));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        mFullScreenDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        ((FrameLayout) findViewById(R.id.root)).addView(playerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        exo_fullscreen.setImageDrawable(ContextCompat.getDrawable(VideoDescriptionActivity.this, R.drawable.fullscreen_in));
    }

    public void setProgressBar() {

        simpleExoPlayer.addListener(new Player.EventListener() {
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            public void onLoadingChanged(boolean isLoading) {

            }

            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.INVISIBLE);
                }

                try {
                    if (playbackState == ExoPlayer.STATE_ENDED) {
                        if (!intentLaunched) {
                            intentLaunched = true;
                            progressBar.setProgress(0);
                            Intent intent = new Intent(VideoDescriptionActivity.this, QuizeActivity.class);
                            intent.putExtra("quize_data", quize_id);
                            intent.putExtra("video_id_data", video_id);
                            startActivity(intent);
                            VideoDescriptionActivity.this.finish();
                        } else {
                            VideoDescriptionActivity.this.finish();
                        }
                    } else {
                        Log.e("error==>", "=====>");
                    }
                    flag++;

                } catch (Exception e) {
                    Log.e("error==>", "=====>" + e.getMessage());
                }

            }

            public void onRepeatModeChanged(int repeatMode) {

            }

            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            public void onPlayerError(ExoPlaybackException error) {
                progressBar.setVisibility(View.VISIBLE);
                simpleExoPlayer.stop();
                simpleExoPlayer.setPlayWhenReady(true);

            }

            public void onPositionDiscontinuity(int reason) {

            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            public void onSeekProcessed() {

            }
        });
    }


    private void add_like() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.LikeModel_call(prefManager.gettokenId(), user_id, video_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.e("output==>", "" + jsonObject);
                        String messge = jsonObject.getString("msg");
                        status = jsonObject.getString("status");
                        Log.e("status_video==>", "" + status);
                        if (status.equals("1")) {
                            image_like.setBackground(getResources().getDrawable(R.drawable.ic_like_full));
                            Toast.makeText(VideoDescriptionActivity.this, "" + messge, Toast.LENGTH_SHORT).show();
                        } else {
                            image_like.setBackground(getResources().getDrawable(R.drawable.ic_like));
                        }
                    } else {
                        Log.e("error==>", "error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                progressDialog.dismiss();
                Log.e("errors==>", "" + t.getMessage());
            }
        });
    }

    private void watch_list() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.VideoFev(prefManager.gettokenId(), user_id, video_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.e("output==>", "" + jsonObject);
                        String messge = jsonObject.getString("msg");
                        status = jsonObject.getString("status");
                        Log.e("status_video==>", "" + status);
                        prefManager.setValue("fullstatus", status);
                        if (status.equals("1")) {
                            image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark_fill));
                            Toast.makeText(VideoDescriptionActivity.this, "" + messge, Toast.LENGTH_SHORT).show();
                        } else {
                            image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark));
                        }
                    } else {
                        Log.e("error==>", "error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                progressDialog.dismiss();
                Log.e("errors==>", "" + t.getMessage());
            }
        });
    }

    private void AddComment_Popup() {

        new AndExAlertDialog.Builder(VideoDescriptionActivity.this)
                .setTitle("Please enter add your comment here")
                .setMessage("")
                .setPositiveBtnText("SUBMIT")
                .setNegativeBtnText("CANCEL")
//                .setEditText(true, false, "Add Your Comment Here", InputType.TEXT_MULTI_LINE)
                .setFont(Font.QUICK_SAND)
                .setEditText(true, false, "Add Your Comment Here", InputType.TEXT_MULTI_LINE)
                .setCancelableOnTouchOutside(false)
                .OnPositiveClicked(new AndExAlertDialogListener() {
                    @Override
                    public void OnClick(String input) {
                        try {
                            Log.e("po_click", "" + "you typed " + input);
                            if (input.isEmpty()) {
                                Toast.makeText(getApplication(), "Please Enter Comment Text", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e("comment", "" + input);
                                AddComment(input);
                                Toast.makeText(VideoDescriptionActivity.this, input, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e("pop_erorr", "" + e.getMessage());
                        }
                       /* if (!comment_name.isEmpty() || comment_name.trim().length() > 0) {

                        } else {
                            Toast.makeText(getApplication(),
                                    "Enter Text", Toast.LENGTH_SHORT).show();
                        }*/
                    }
                })
                .OnNegativeClicked(new AndExAlertDialogListener() {
                    @Override
                    public void OnClick(String input) {
                        Log.e("no_click", "No button clicked");
                    }
                })

                .setTitleTextColor(R.color.splash_color)
                .setMessageTextColor(R.color.splash_color)
                .setButtonTextColor(R.color.splash_color)
                .build();

    }

    private void AddComment(String comment) {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<CommentModel> call = bookNPlayAPI.SubmitComment(api_token, prefManager.getLoginId(), topic_id, comment);
        call.enqueue(new Callback<CommentModel>() {
            @Override
            public void onResponse(Call<CommentModel> call, Response<CommentModel> response) {
                Toast.makeText(VideoDescriptionActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                view_comments();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CommentModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    private void add_watch() {
        progressDialog.show();
        AppAPI bookNPlayAPI = BaseURL.getVideoAPI();
        Call<ResponseBody> call = bookNPlayAPI.LikeModel_call(prefManager.gettokenId(), user_id, video_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               /* if (response.code() == 200) {
                    progressDialog.dismiss();
                    Toast.makeText(VideoDescriptionActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    if (response.body().getStatus().equals("200")) {
                        image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark_fill));
                    } else {
                        image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark));
                    }
                }*/
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void Video_deatils_get(Integer video_id) {
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<VideoDesListModel> call = appAPI.video_desc_list_call(api_token, user_id, video_id);
            call.enqueue(new Callback<VideoDesListModel>() {
                @Override
                public void onResponse(Call<VideoDesListModel> call, Response<VideoDesListModel> response) {
                    if (response.code() == 200 && response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        video_desc_dataList = new ArrayList<>();
                        video_desc_dataList = response.body().getData();
                        Log.e("dataList==>", "" + video_desc_dataList.size());
                        if (video_desc_dataList.size() > 0) {
                            video_url = video_desc_dataList.get(0).getVideoPath();
                            Log.e("v_url==>", "" + video_url);

                            video_path = video_desc_dataList.get(0).getVideoPath();
                            img_path = video_desc_dataList.get(0).getThumbnail();
                            video_heading = video_desc_dataList.get(0).getVideoName();
                            video_desc = video_desc_dataList.get(0).getVideoDescription();
                            topic_id = video_desc_dataList.get(0).getTopicId();
                            quize_id = video_desc_dataList.get(0).getQuizId();

                            String self_Like = video_desc_dataList.get(0).getSelf_Like();
                            Log.e("quiz_id==>", "" + self_Like);
                            if (self_Like.equals("1")) {
                                image_like.setBackground(getResources().getDrawable(R.drawable.ic_like_full));
                            } else {
                                image_like.setBackground(getResources().getDrawable(R.drawable.ic_like));
                            }
                            String self_fav = video_desc_dataList.get(0).getSelf_fev();
                            Log.e("self_fav==>", "" + self_fav);
                            if (self_fav.equals("1")) {
                                image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark_fill));
                            } else {
                                image_favorite.setBackground(getResources().getDrawable(R.drawable.ic_bookmark));
                            }
                            set_video_det_data();
                            initialize();
                        } else {
                            Toast.makeText(VideoDescriptionActivity.this, "Record Not Found", Toast.LENGTH_LONG).show();
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<VideoDesListModel> call, Throwable t) {
//                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }
    }


    public void initialize() {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        Handler mainHandler = new Handler();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);



        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
                getApplicationContext(), Util.getUserAgent(getApplicationContext(),
                "com.a3iteam.exoplayertest"));
        Cache cache = new SimpleCache(cacheDir,
                new LeastRecentlyUsedCacheEvictor(maxBytes));
        dataSourceFactory = new CacheDataSourceFactory(cache,
                dataSourceFactory);






        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(),
                Util.getUserAgent(this, "TVLivePro"), bandwidthMeter);
        mediaDataSorceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"));


        // MediaSource mediaSource = new ProgressiveMediaSource.Factory(mediaDataSorceFactory).createMediaSource(Uri.parse(url));
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        Uri uri = Uri.parse(video_url);
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        if (video_url.endsWith(".m3u8")) {
            // videoSource = new HlsMediaSource(uri, dataSourceFactory, mainHandler, null);
        } else {
            videoSource = new ExtractorMediaSource(uri,
                    dataSourceFactory, extractorsFactory, null, null);
        }
        simpleExoPlayer.prepare(videoSource);
        playerView.setShutterBackgroundColor(Color.TRANSPARENT);
//        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
        playerView.setPlayer(simpleExoPlayer);
        simpleExoPlayer.setPlayWhenReady(true);
        playerView.requestFocus();
        finalTime = simpleExoPlayer.getDuration();

        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        simpleExoPlayer.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

       /* if (position != null) {
            simpleExoPlayer.seekTo(position);
        }*/
        setProgressBar();
    }

    protected void onStart() {
        super.onStart();
        // initalize();
//        get_video_detail();
       /* Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            video_id = bundle.getInt("video_id");
            Log.e("video_id", "" + video_id);

            Video_deatils_get(video_id);
            VideoWatchList();
        }*/

        if (position != null) {
            simpleExoPlayer.seekTo(position);
            simpleExoPlayer.getPlaybackState();
            simpleExoPlayer.setPlayWhenReady(true);
            playerView.requestFocus();
        }
    }

    public void releaseplayer() {
        try {
            simpleExoPlayer.release();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // initalize();
        if (position != null) {
            if (position != C.TIME_UNSET) {
                simpleExoPlayer.seekTo(position);
                simpleExoPlayer.getPlaybackState();
                simpleExoPlayer.setPlayWhenReady(true);
                playerView.requestFocus();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {

            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
            position = simpleExoPlayer.getCurrentPosition(); //then, save it on the bundle.
            Log.e("position==>", "" + position);
            prefManager.setValue("seek_current_position", "" + position);
        } catch (Exception e) {
            Log.e("e===>", e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseplayer();
    }

    public void set_video_det_data() {
        textView_video_heading.setText(video_heading);
        textView_video_desc.setText(video_desc);
    }

}
