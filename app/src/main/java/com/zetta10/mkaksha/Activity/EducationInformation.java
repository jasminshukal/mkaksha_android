package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EducationInformation extends AppCompatActivity {

    EditText edit_college_name;
    LinearLayout linearLayout, finish_video_list;
    String college_name;
    ProgressDialog progressDialog;
    String login_token, user_id;
    PrefManager prefManager;
    String str_name_institute;
    String str_standard, current_std;
    Spinner spinner_for_std;
    int[] std;
    ArrayList<String> array_std_value;
    ArrayList<String> array_std_key;
    String key, value;
    ArrayAdapter adpter_std;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_education_information);

        array_std_key = new ArrayList<String>();
        array_std_value = new ArrayList<String>();
        edit_college_name = findViewById(R.id.edit_college_name);
        finish_video_list = findViewById(R.id.finish_video_list);
        linearLayout = findViewById(R.id.linear_for_submit);
        spinner_for_std = findViewById(R.id.spinner_for_std);
        progressDialog = new ProgressDialog(EducationInformation.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getApplicationContext());
        login_token = prefManager.gettokenId();
        user_id = prefManager.getLoginId();
        std = getResources().getIntArray(R.array.standard);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 2000);


        get_profile_detail();

        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                college_name = edit_college_name.getText().toString().trim();
                if (TextUtils.isEmpty(college_name)) {

                    Toast.makeText(EducationInformation.this, "Enter your College or school or Institute Name", Toast.LENGTH_SHORT).show();
                    return;
                }

                edu_information();
                finish();
            }
        });

      /*  ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.standard, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner_for_std.setAdapter(adapter);*/

        /*spinner_for_std.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                standard = std[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        spinner_for_std.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_standard = array_std_key.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void get_standard_data() {

        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> standard_call = apiinterface.getstandard();

        standard_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject json_data = jsonObject.getJSONObject("data");

                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_std_value.add(value);
                        array_std_key.add(key);
                    }


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                setdata_std();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        /*RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_std, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    JSONObject json_data = jsonObject.getJSONObject("data");


                    Iterator iterator = json_data.keys();
                    while (iterator.hasNext()) {
                        key = (String) iterator.next();
                        value = json_data.getString(key);

                        array_std_value.add(value);
                        array_std_key.add(key);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setdata_std();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);*/

    }

    public void setdata_std() {

        adpter_std = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, array_std_value);
        //  adpter_std.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_for_std.setAdapter(adpter_std);
    }

    public void edu_information() {

//        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> stud_edu_info_call = apiinterface.student_edu_info(login_token, user_id, str_standard, college_name);

        stud_edu_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    //  JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    String msg = jsonObject.getString("msg");
                    Toast.makeText(EducationInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void get_profile_detail() {

        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(user_id, login_token);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(EducationInformation.this, ""+msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                    str_name_institute = jsonObject_stud.getString("name_institute");
                    current_std = jsonObject_stud.getString("current_std");
                    /*int c_data = Integer.parseInt(current_std);
                    spinner_for_std.setSelection(c_data);*/
                    set_stud_data();
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void set_stud_data() {

        edit_college_name.setText(str_name_institute);
        String dhoran = current_std;
        try {
            if (dhoran != null) {
                if (adpter_std != null) {
                    int spinner_pos = adpter_std.getPosition(dhoran);
                    spinner_for_std.setSelection(spinner_pos);
                }
            }
        } catch (Exception e) {
            Log.e("e==>", "" + e.getMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        get_standard_data();
    }
}
