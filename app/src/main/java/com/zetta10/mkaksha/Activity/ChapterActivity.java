package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Adapter.ChapterListAdapter;
import com.zetta10.mkaksha.Model.ChapaterListModel.ChapaterListModel;
import com.zetta10.mkaksha.Model.ChapaterListModel.Data;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChapterActivity extends AppCompatActivity {

    LinearLayout finish_ch_list,ly_dataNotFound;
    Integer c_id;
    String subject_name;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    List<Data> chapter_dataList;
    RecyclerView rv_chapter_list;
    ChapterListAdapter chapterListAdapter;
    TextView toolbar_title;
    String api_token, user_id;
    String payment_array_size = String.valueOf(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chapter_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        init();
        prefManager = new PrefManager(ChapterActivity.this);
        payment_array_size = prefManager.getValue("payment");
        progressDialog = new ProgressDialog(ChapterActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            c_id = bundle.getInt("chapter_id");
            subject_name = bundle.getString("subject");
            //  payment_array_size = bundle.getString("payment_data_size");
            Log.e("chapter_id", "" + c_id);
            Log.e("payment_array_size", "" + payment_array_size);
            chapter_list(c_id);
            toolbar_title.setText(subject_name);
        }

        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        finish_ch_list = findViewById(R.id.finish_ch_list);
        ly_dataNotFound = findViewById(R.id.ly_dataNotFound);
        rv_chapter_list = findViewById(R.id.rv_chapter_list);
        toolbar_title = findViewById(R.id.toolbar_title);
    }

    private void chapter_list(Integer sub_id) {
        progressDialog.show();
        try {
            progressDialog.show();
//            String api_token = "b77a19297421a69db9f3feccecc4073b1a89b7f8847a513d4cb6ced499afcd55";
//            int uid = 1;
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<ChapaterListModel> call = appAPI.chapter_list_call(api_token, user_id, sub_id);
            call.enqueue(new Callback<ChapaterListModel>() {
                @Override
                public void onResponse(Call<ChapaterListModel> call, Response<ChapaterListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        chapter_dataList = new ArrayList<>();
                        chapter_dataList = response.body().getData();
                        Log.e("dataList==>", "" + chapter_dataList.size());
                        if (chapter_dataList.size() > 0) {
                            chapterListAdapter = new ChapterListAdapter(ChapterActivity.this, chapter_dataList,payment_array_size);
                            rv_chapter_list.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(ChapterActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_chapter_list.setLayoutManager(mLayoutManager3);
                            rv_chapter_list.setItemAnimator(new DefaultItemAnimator());
                            rv_chapter_list.setAdapter(chapterListAdapter);
                            chapterListAdapter.notifyDataSetChanged();
                            rv_chapter_list.setVisibility(View.VISIBLE);
                            rv_chapter_list.setVisibility(View.VISIBLE);
                        } else {
                            rv_chapter_list.setVisibility(View.GONE);
                            ly_dataNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ChapaterListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onPause() {
        super.onPause();

        if(progressDialog != null)
            progressDialog .dismiss();
    }
}
