package com.zetta10.mkaksha.Activity;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.rezwan.knetworklib.KNetwork;
import com.squareup.picasso.Picasso;
import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.Fragment.HomeFragment;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    BottomNavigationView bottomNavigationView;
    PrefManager prefManager;
    ImageView search, profile;
    String Reference_number;
    TextView txt_name, txt_coin;
    String url, fname, lname, payment_array_size = String.valueOf(0);

    CircleImageView profilePic;
    boolean doubleBackToExitPressedOnce = false;

    String image_url;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        MultiDex.install(this);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        prefManager = new PrefManager(this);
        search = findViewById(R.id.search);

        View headerView = navigationView.getHeaderView(0);
        txt_name = (TextView) headerView.findViewById(R.id.txt_name);
        profilePic = headerView.findViewById(R.id.profilePic);
//        profile = findViewById(R.id.profile);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setItemIconTintList(null);

        try {
//            url = prefManager.getValue("image_urls");
            fname = prefManager.getValue("fname");
            lname = prefManager.getValue("lname");
//            Log.e("imagenames==>", "" + url);
            txt_name.setText(fname + "  " + lname);
//            Picasso.get().load(url).resize(300, 300).into(profilePic);
            get_profile_detail();
        } catch (Exception e) {
            Log.e("message==>", "" + e.getMessage());
        }

     /*   String coin = prefManager.getcoin();
        if (!coin.equals(" ")) {
            popup_coin(coin);
        } else {
            Log.e("error", "");
        }*/

        pushFragment(new HomeFragment());

        if (bottomNavigationView != null) {
            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            // Write code to perform some actions.
                            Log.e("item", "" + item);
                            selectFragment(item);
                            return false;
                        }
                    });
        }
        get_payment_detail();

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra("payment_check", payment_array_size);
                startActivity(intent);
            }
        });
    }

    private void popup_coin(String coin) {
        dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.coin_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_coin = dialog.findViewById(R.id.txt_coin);
        txt_coin.setText("you earn" + coin + "coins");

        ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
        dialog.show();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public void get_payment_detail() {
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> check_payment_call = apiinterface.checkpayment(prefManager.gettokenId(), prefManager.getLoginId());

        check_payment_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray jsonArray = jsonObject.getJSONArray("data");


                    // payment_data_list.add("payment_data");
                    payment_array_size = String.valueOf(jsonArray.length());
                    prefManager.setValue("payment", payment_array_size);

                  /*  Bundle bundle = new Bundle();
                    bundle.putString("payment_data", payment_array_size);
// set Fragmentclass Arguments
                    HomeFragment fragobj = new HomeFragment();
                    fragobj.setArguments(bundle);*/
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("e-->", "" + t.getMessage());
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.navigation_home) {
            pushFragment(new HomeFragment()
            );
        } else if (id == R.id.navigation_subscription) {
            startActivity(new Intent(MainActivity.this, SubscriptionActivity.class));
        } else if (id == R.id.navigation_my_journey) {
            startActivity(new Intent(MainActivity.this, MyJourneyActivity.class));
        } else if (id == R.id.navigation_manage_account) {
            startActivity(new Intent(MainActivity.this, Manage_Profile.class));
        } else if (id == R.id.navigation_wallet) {
            startActivity(new Intent(MainActivity.this, Wallet_Activity.class));
        } else if (id == R.id.navigation_watchlist) {
            startActivity(new Intent(MainActivity.this, WatchListActivity.class));
        } else if (id == R.id.navigation_Test_Result) {
            startActivity(new Intent(MainActivity.this, Available_Test_Result_Activity.class));
        } else if (id == R.id.navigation_notification) {
            startActivity(new Intent(MainActivity.this, Notification_display.class));
        } else if (id == R.id.navigation_invite_friend) {
            startActivity(new Intent(MainActivity.this, ReferedActivity.class));
        } else if (id == R.id.navigation_Privacy_Policy) {
            startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
        } else if (id == R.id.navigation_Terms_condition) {
            startActivity(new Intent(MainActivity.this, TermsConditionActivity.class));
        } else if (id == R.id.navigation_Help) {
            startActivity(new Intent(MainActivity.this, HelpActivity.class));
        } else if (id == R.id.navigation_logout) {
            if (prefManager.getLoginId().equalsIgnoreCase("0")) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            } else {
                logout();
            }
        }

        /*else if (id == R.id.navigation_profile) {
            if (prefManager.getLoginId().equalsIgnoreCase("0")) {
                startActivity(new Intent(MainActivity.this, SubjectListModel.class));
            } else {
                startActivity(new Intent(MainActivity.this, SubjectListModel.class));
            }
        }*/
        /*else if (id == R.id.navigation_watchlist) {
            startActivity(new Intent(MainActivity.this, WatchList.class));
        } else if (id == R.id.navigation_download) {
            startActivity(new Intent(MainActivity.this, DownloadList.class));
        } else if (id == R.id.navigation_premium) {
            startActivity(new Intent(MainActivity.this, Premium.class));
        } else if (id == R.id.nav_share) {
            startActivity(new Intent(MainActivity.this, Settings.class));
        } else if (id == R.id.nav_rate) {
            rateMe();
        } else if (id == R.id.nav_logout) {
            Log.e("LoginId",""+prefManager.getLoginId());
            if (prefManager.getLoginId().equalsIgnoreCase("0")) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            } else {
                logout();
            }
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void selectFragment(MenuItem item) {

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.bottom_home:
                pushFragment(new HomeFragment());
                break;
            case R.id.bottom_tv:
                pushFragment(new HomeFragment());
                break;
            case R.id.bottom_movie:
                pushFragment(new HomeFragment());
                break;
            case R.id.bottom_sport:
                pushFragment(new HomeFragment());
                break;
            case R.id.bottom_news:
                pushFragment(new HomeFragment());
                break;
        }
    }

    protected void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (ft != null) {
                ft.replace(R.id.rootLayout, fragment);
                ft.commit();
            }
        }
    }

    public void logout() {
        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogDanger))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        prefManager.setLoginId("0");
                        prefManager.settoken("0");
                        prefManager.set_check_payment("0");

                        String refer_code = String.valueOf(0);
                        prefManager.setValue("referral_code", refer_code);
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    public void get_profile_detail() {
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(prefManager.getLoginId(), prefManager.gettokenId());

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(AddressInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                    image_url = jsonObject_stud.getString("profile_img");
                    Log.e("urls===>", "" + image_url);
                    Picasso.get().load(image_url).resize(300, 300).placeholder(R.drawable.user_account_profile).into(profilePic);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        get_profile_detail();
    }

}
