package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuardianInformation extends AppCompatActivity {

    EditText edit_name_of_gua, edit_gua_phone;
    LinearLayout linear_for_submit;
    String name_of_gua, phone_of_gua;
    ProgressDialog progressDialog;
    String login_token, user_id;
    PrefManager prefManager;
    String str_guardian_name, str_guardian_number;
    LinearLayout finish_video_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_guardian_information);

        edit_name_of_gua = findViewById(R.id.edit_name_of_gua);
        edit_gua_phone = findViewById(R.id.edit_gua_phone);
        finish_video_list = findViewById(R.id.finish_video_list);

        linear_for_submit = findViewById(R.id.linear_for_submit);
        progressDialog = new ProgressDialog(GuardianInformation.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getApplicationContext());
        login_token = prefManager.gettokenId();
        user_id = prefManager.getLoginId();


        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        linear_for_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name_of_gua = edit_name_of_gua.getText().toString();
                phone_of_gua = edit_gua_phone.getText().toString();

                if (TextUtils.isEmpty(name_of_gua)) {
                    Toast.makeText(GuardianInformation.this, "Enter yout guardian Name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(phone_of_gua)) {
                    Toast.makeText(GuardianInformation.this, "Enter yout guardian Number", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (phone_of_gua.length() < 10) {
                    Toast.makeText(GuardianInformation.this, "Enter Valid Phone number", Toast.LENGTH_SHORT).show();
                }
                guardian_info();
            }
        });

        get_profile_detail();
    }

    public void guardian_info() {

        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> stud_gua_info_call = apiinterface.student_guardian_info(login_token, user_id, name_of_gua, phone_of_gua);

        stud_gua_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    //  JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    String msg = jsonObject.getString("msg");
                    Toast.makeText(GuardianInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void get_profile_detail() {

        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(user_id, login_token);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(GuardianInformation.this, ""+msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student_detail");
                    str_guardian_name = jsonObject_stud.getString("guardian_name");
                    str_guardian_number = jsonObject_stud.getString("guardian_number");
                    set_stud_data();
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void set_stud_data() {

        edit_name_of_gua.setText(str_guardian_name);
        edit_gua_phone.setText(str_guardian_number);
    }
}
