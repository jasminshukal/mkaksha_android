package com.zetta10.mkaksha.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.zetta10.mkaksha.Adapter.Myadapter_for_test_history;
import com.zetta10.mkaksha.R;

public class Test_history_Activity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    TextView toolbar_title;
    LinearLayout finish_ch_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_history);

        tabLayout = findViewById(R.id.tab_layout);
        toolbar_title = findViewById(R.id.toolbar_title);
        finish_ch_list = findViewById(R.id.finish_ch_list);
        viewPager = findViewById(R.id.view_pager);
        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Myadapter_for_test_history myadapter_for_test_history = new Myadapter_for_test_history(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(myadapter_for_test_history);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        // printDifference(date1,date2);
    }

   /* public void getavailable_test() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<Available_test_main_pojo> available_test_call = apiinterface.getavailable_test(login_token, user_id);

        available_test_call.enqueue(new Callback<Available_test_main_pojo>() {
            @Override
            public void onResponse(Call<Available_test_main_pojo> call, Response<Available_test_main_pojo> response) {

                try {
                    if (response.body().getStatus().equals("1")) {
                        for (int i = 0; i < response.body().data.availableTest.size(); i++) {

                            Available_test_list_pojo available_test_list_pojo = new Available_test_list_pojo();
                            available_test_list_pojo.setName(response.body().data.availableTest.get(i).getName());
                            available_test_list_pojo.setStartDate(response.body().data.availableTest.get(i).getStartDate());
                            available_test_list_pojo.setStartTimeOnly(response.body().data.availableTest.get(i).getStartTimeOnly());
                            available_test_list_pojo.setCurrentDate(response.body().data.availableTest.get(i).getCurrentDate());
                            available_test_list_pojo.setCurrentTime(response.body().data.availableTest.get(i).getCurrentTime());
                            available_test_list_pojo.setCurrentDateTime(response.body().data.availableTest.get(i).getCurrentDateTime());
                            available_test_list_pojo.setStartTime(response.body().data.availableTest.get(i).getStartTime());

                            arrayList_available_test.add(available_test_list_pojo);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView_for_avail_test.setLayoutManager(layoutManager);

                        Adapter_for_ava_test adapter_for_ava_test = new Adapter_for_ava_test(getApplicationContext(), arrayList_available_test);
                        recyclerView_for_avail_test.setAdapter(adapter_for_ava_test);
                        progressDialog.dismiss();
                    }

                } catch (JsonIOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Available_test_main_pojo> call, Throwable t) {

            }
        });
    }*/

}

