package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zetta10.mkaksha.Model.PromoModel.Data;
import com.zetta10.mkaksha.Model.PromoModel.PromoModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zetta10.mkaksha.Activity.PromoCodeActivity.code;
import static com.zetta10.mkaksha.Activity.PromoCodeActivity.ed_promo;
import static com.zetta10.mkaksha.Activity.PromoCodeActivity.offer_value;
import static com.zetta10.mkaksha.Activity.PromoCodeActivity.type;

public class PromoCodeActivity extends AppCompatActivity {

    LinearLayout finish_ch_list, linear_for_apply;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    RecyclerView rv_promo_code;
    TextView txt_apply;
    public static EditText ed_promo;
    List<Data> promo_dataList;
    PromoCodeAdapter promoCodeAdapter;
    int value, final_value, promo_minus, subscription_id_data, check_wallet, minus_value, check_for_remove, remove_checker, promo_remove_second, promo_count = 0, check_promo_sec, check_promo_wallet, Price = 0, api_counter = 0, temp_price = 0;
    LinearLayout ly_apply_pay;
    int check_promo, promo_remove;
    public static String type, code;
    public static int offer_value;
    boolean isChecked;
    String discount, description, create_time, subject, name, sub_price;
    int coin_reedem;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promo_code_activity);
        init();
        prefManager = new PrefManager(PromoCodeActivity.this);
        progressDialog = new ProgressDialog(PromoCodeActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        Intent intent = getIntent();
        //   value = intent.getIntExtra("promo_value", 0);
        //  check_wallet = intent.getIntExtra("check_wallet", 0);
        //   subscription_id_data = intent.getIntExtra("subscription_id_data", 0);
        //   minus_value = intent.getIntExtra("minus_value", 0);
        //     check_for_remove = intent.getIntExtra("check_for_remove",0);
        //   promo_remove = intent.getIntExtra("promo_remove", 0);
        //   remove_checker = intent.getIntExtra("remove_checker", 0);
        //   promo_remove_second = intent.getIntExtra("promo_remove_second", 0);
        //   promo_count = intent.getIntExtra("promo_count", 0);
        //    check_promo_sec = intent.getIntExtra("check_promo_sec", 0);
        //    check_promo_wallet = intent.getIntExtra("check_promo_wallet", 0);
        Price = intent.getIntExtra("Price", 0);
        discount = intent.getStringExtra("discount");
        description = intent.getStringExtra("description");
        create_time = intent.getStringExtra("create_time");
        subject = intent.getStringExtra("subject");
        name = intent.getStringExtra("name");
        sub_price = intent.getStringExtra("sub_price");
        api_counter = intent.getIntExtra("api_counter", 0);
        isChecked = intent.getBooleanExtra("ischecked", false);
        coin_reedem = intent.getIntExtra("coin_reedem", 0);
        Log.e("value-->", "" + value);
        Log.e("check_wallet-->", "" + check_wallet);


      /*  Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            value = bundle.getString("promo_value");
            Log.e("value-->", "" + value);
        }*/


        txt_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String data = ed_promo.getText().toString().trim();

                if (code.equals(data)) {

                    if (type.equals("1")) {
                        temp_price = Price;
                        Price = Price - Price * offer_value / 100;
                        promo_minus = temp_price * offer_value / 100;
                        Log.e("final_value==>", "" + final_value);
                        Log.e("promo_minus==>", "" + promo_minus);
                    } else if (type.equals("2")) {
                        Price = Price - offer_value;
                        promo_minus = offer_value;
                        Log.e("final_value2==>", "" + final_value);
                        Log.e("promo_minus2==>", "" + promo_minus);
                    }
//                Log.e("promo_minus==>", "" + promo_minus);

                    Intent intent1 = new Intent(PromoCodeActivity.this, Subsription_desc_Activity.class);
                    //     intent1.putExtra("promo_final_value", final_value);
                    //     intent1.putExtra("check_promo", check_promo);
                    intent1.putExtra("promo_minus", promo_minus);
                    intent1.putExtra("promo_code", code);
                    //    intent1.putExtra("subscription_id", subscription_id_data);
                    //    intent1.putExtra("check_wallet_data", check_wallet);
                    //    intent1.putExtra("minus_value_data", minus_value);
                    //    intent1.putExtra("check_for_remove", check_for_remove);
                    //    intent1.putExtra("promo_remove_data", promo_remove);
                    //   intent1.putExtra("remove_checker_data", remove_checker);
                    //   intent1.putExtra("promo_remove_second_data", promo_remove_second);
                    //   intent1.putExtra("promo_count_data", promo_count);
                    //   intent1.putExtra("check_promo_sec_data", check_promo_sec);
                    //   intent1.putExtra("check_promo_wallet_data", check_promo_wallet);
                    intent1.putExtra("Price", Price);
                    intent1.putExtra("discount", discount);
                    intent1.putExtra("description", description);
                    intent1.putExtra("create_time", create_time);
                    intent1.putExtra("subject", subject);
                    intent1.putExtra("name", name);
                    intent1.putExtra("api_counter", api_counter);
                    intent1.putExtra("ischecked", isChecked);
                    intent1.putExtra("sub_price", sub_price);
                    intent1.putExtra("coin_reedem", coin_reedem);
                    startActivity(intent1);
                    finish();
                } else {
                    Toast.makeText(PromoCodeActivity.this, "InValid PromoCode", Toast.LENGTH_SHORT).show();
                }
            }
        });

        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        get_promo_code();

    }

    private void get_promo_code() {
        progressDialog.show();
        try {
            progressDialog.show();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<PromoModel> call = appAPI.Promo(prefManager.gettokenId(), prefManager.getLoginId());
            call.enqueue(new Callback<PromoModel>() {
                @Override
                public void onResponse(Call<PromoModel> call, Response<PromoModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        promo_dataList = new ArrayList<>();
                        promo_dataList = response.body().getData();
                        Log.e("dataList==>", "" + promo_dataList.size());
                        if (promo_dataList.size() > 0) {
                            promoCodeAdapter = new PromoCodeAdapter(PromoCodeActivity.this, promo_dataList);
                            rv_promo_code.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(PromoCodeActivity.this,
                                    LinearLayoutManager.VERTICAL, false);

                            rv_promo_code.setLayoutManager(mLayoutManager3);
                            rv_promo_code.setItemAnimator(new DefaultItemAnimator());
                            rv_promo_code.setAdapter(promoCodeAdapter);
                            promoCodeAdapter.notifyDataSetChanged();
                            rv_promo_code.setVisibility(View.VISIBLE);
                            rv_promo_code.setVisibility(View.VISIBLE);
                        } else {
                            rv_promo_code.setVisibility(View.GONE);
//                            ly_paid_book.setVisibility(View.GONE);
                        }
                    }
                    progressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<PromoModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }
    }

    private void init() {
        finish_ch_list = findViewById(R.id.finish_ch_list);
        linear_for_apply = findViewById(R.id.linear_for_apply);
        ly_apply_pay = findViewById(R.id.ly_apply_pay);
        rv_promo_code = findViewById(R.id.rv_promo_code);
        txt_apply = findViewById(R.id.txt_apply);
        ed_promo = findViewById(R.id.ed_promo);
    }


}

class PromoCodeAdapter extends RecyclerView.Adapter<com.zetta10.mkaksha.Activity.PromoCodeAdapter.MyviewHolder> {
    Context mcontext;
    PrefManager prefManager;
    String from;
    private List<com.zetta10.mkaksha.Model.PromoModel.Data> promo_code_list;

    public PromoCodeAdapter(Context context, List<Data> promo_code_list) {
        this.promo_code_list = promo_code_list;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public com.zetta10.mkaksha.Activity.PromoCodeAdapter.MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promocode_layout_design, parent, false);

        return new com.zetta10.mkaksha.Activity.PromoCodeAdapter.MyviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull com.zetta10.mkaksha.Activity.PromoCodeAdapter.MyviewHolder holder, int position) {
        holder.txt_promo_details.setText(promo_code_list.get(position).getDetail());
        holder.txt_promo_value.setText(promo_code_list.get(position).getCode());
        holder.ly_promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ed_promo.setText(promo_code_list.get(position).getCode());
                type = promo_code_list.get(position).getType();
                offer_value = promo_code_list.get(position).getOfferValue();
                code = promo_code_list.get(position).getCode();
            }
        });


    }

    @Override
    public int getItemCount() {
        return promo_code_list.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView txt_promo_value, txt_promo_details;
        RelativeLayout ly_apply;
        LinearLayout ly_promo_code;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            txt_promo_details = itemView.findViewById(R.id.txt_promo_details);
            ly_apply = itemView.findViewById(R.id.ly_apply);
            txt_promo_value = itemView.findViewById(R.id.txt_promo_value);
            ly_promo_code = itemView.findViewById(R.id.ly_promo_code);
        }
    }
}
