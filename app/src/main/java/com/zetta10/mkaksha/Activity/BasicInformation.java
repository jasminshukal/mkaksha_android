package com.zetta10.mkaksha.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasicInformation extends AppCompatActivity {

    EditText edit_name, edit_phone, edit_email, edit_date_of_join;
    LinearLayout linear_for_save;
    String phone_number, name, email, date_of_join;
    ProgressDialog progressDialog;
    String login_token, user_id;
    PrefManager prefManager;
    String join_date, fname_stud, lname_stud, phno, email_stud;

    LinearLayout finish_video_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_basic_information);

        edit_name = findViewById(R.id.edit_name);
        edit_phone = findViewById(R.id.edit_phone);
        edit_email = findViewById(R.id.edit_email);
        edit_date_of_join = findViewById(R.id.edit_date_of_join);
        linear_for_save = findViewById(R.id.linear_for_save);
        finish_video_list = findViewById(R.id.finish_video_list);

        progressDialog = new ProgressDialog(BasicInformation.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        prefManager = new PrefManager(getApplicationContext());
        login_token = prefManager.gettokenId();
        user_id = prefManager.getLoginId();


        finish_video_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        linear_for_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                phone_number = edit_phone.getText().toString().trim();
                name = edit_name.getText().toString().trim();
                email = edit_email.getText().toString().trim();
                date_of_join = edit_date_of_join.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(BasicInformation.this, "Enter Name", Toast.LENGTH_SHORT).show();
                    return;

                }
                if (TextUtils.isEmpty(phone_number)) {
                    Toast.makeText(BasicInformation.this, "Enter a Mobile Number", Toast.LENGTH_SHORT).show();
                    return;
                } else if (phone_number.length() < 10) {
                    Toast.makeText(BasicInformation.this, "Enter a Valid Mobile Number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(BasicInformation.this, "Enter a Email Address", Toast.LENGTH_SHORT).show();
                    return;
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Toast.makeText(BasicInformation.this, "Enter Valid Email Address", Toast.LENGTH_SHORT).show();
                    return;
                }

                /*if (TextUtils.isEmpty(date_of_join)) {
                    Toast.makeText(BasicInformation.this, "Enter a Mobile Number", Toast.LENGTH_SHORT).show();

                }*/
                //  student_basic_info();
            }
        });
        get_profile_detail();
    }

    public void get_profile_detail() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_basic_info(user_id, login_token);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
//                    Toast.makeText(BasicInformation.this, "" + msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student");
                    join_date = jsonObject_stud.getString("JoinDate");
                    fname_stud = jsonObject_stud.getString("fname");
                    lname_stud = jsonObject_stud.getString("lname");
                    phno = jsonObject_stud.getString("phno");
                    email_stud = jsonObject_stud.getString("email");
                    set_stud_data();
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void student_basic_info() {

       /* Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> basic_info_call = apiinterface.student_get_profile(login_token,user_id);

        basic_info_call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");
                    Toast.makeText(BasicInformation.this, ""+msg, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                    JSONObject jsonObject_stud = jsonObject_data.getJSONObject("Student");
                    join_date = jsonObject_stud.getString("JoinDate");
                    fname_stud = jsonObject_stud.getString("fname");
                    lname_stud = jsonObject_stud.getString("lname");
                    phno = jsonObject_stud.getString("phno");
                    email_stud = jsonObject_stud.getString("email");
                    set_stud_data();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });*/

    }

    public void set_stud_data() {

        edit_date_of_join.setText(join_date);
        edit_phone.setText(phno);
        edit_name.setText(fname_stud + " " + lname_stud);
        edit_email.setText(email_stud);
    }
}
