package com.zetta10.mkaksha.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.zetta10.mkaksha.Api.Apiinterface;
import com.zetta10.mkaksha.Apiclient.Apiclient;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MegatestActivity extends AppCompatActivity {

    TextView option1, option2, option3, option4;
    LinearLayout linear_for_back, linear_for_skip, finish_topic_list, ly_option1, ly_option2, ly_option3, ly_option4;
    TextView txt_question, toolbar_title;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    List<QuizItem> quizItems;
    String quize_data, question, corrects, duration_test;
    int correct = 0, wrong = 0, attempt_question = 0;
    int currentQuestion = 0,skip;
    ProgressBar progressBar;
    String user_id, login_token;
    CountdownView countdownView;
    String duration;
    int milisecond_duration;
    String test_id;
    int hour, minute, sec;
    long remain_time, remain_minute;
    public long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds, different, different1;
    public long secondsInMilli, minutesInMilli, hoursInMilli, daysInMilli;
    int test_time;
    String temp_day, temp_hour = "0", temp_minute = "0", temp_sec = "0", is_gift, gift_desc;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.mega_test_activity);
        init();

        countdownView = findViewById(R.id.countdownview);
        Intent intent = getIntent();
        duration = intent.getStringExtra("duration");
        test_id = intent.getStringExtra("test_id");
        is_gift = intent.getStringExtra("is_gift");
        gift_desc = intent.getStringExtra("gift_desc");


        if (is_gift.equals("1")) {

            Dialog dialog = new Dialog(MegatestActivity.this);
            dialog.setContentView(R.layout.layout_for_reaward_des);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
            WebView webView = dialog.findViewById(R.id.webview_for_gift);
            webView.loadData(gift_desc, "text/html", "UTF-8");
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    test_time = Integer.parseInt(duration);
                    milisecond_duration = test_time * 60000;
                    countdownView.start(milisecond_duration);
                }
            });
            dialog.show();
        }
        if (is_gift.equals("0")) {

            test_time = Integer.parseInt(duration);
            milisecond_duration = test_time * 60000;
            countdownView.start(milisecond_duration);
        }

     /*   test_time = Integer.parseInt(duration);
        milisecond_duration = test_time * 60000;
        countdownView.start(milisecond_duration);
        remain_time = countdownView.getRemainTime();*/
        countdownView.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
            @Override
            public void onEnd(CountdownView cv) {
                different = milisecond_duration;
                secondsInMilli = 1000;
                minutesInMilli = secondsInMilli * 60;
                hoursInMilli = minutesInMilli * 60;
                daysInMilli = hoursInMilli * 24;

                elapsedDays = different / daysInMilli;
                different = different % daysInMilli;

                elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;

                elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;

                elapsedSeconds = different / secondsInMilli;
                if (elapsedDays != 0) {
                    temp_day = String.valueOf(elapsedDays);
                }
                if (elapsedHours != 0) {
                    temp_hour = String.valueOf(elapsedHours);
                }
                if (elapsedMinutes != 0) {
                    temp_minute = String.valueOf(elapsedMinutes);
                }
                if (elapsedSeconds != 0) {
                    temp_sec = String.valueOf(elapsedSeconds);
                }
                duration_test = (temp_hour + ":" + temp_minute + ":" + temp_sec);

                submit_test();
            }
        });
      /*  countdownView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = countdownView.getHour();
                //  duration_test = countdownView.getMinute();
                sec = countdownView.getSecond();
                remain_time = countdownView.getRemainTime();
            }
        });*/
        prefManager = new PrefManager(MegatestActivity.this);
        progressDialog = new ProgressDialog(MegatestActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        quizItems = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            quize_data = bundle.getString("quize_data");
            Log.e("quize_data", "" + quize_data);
        }

        linear_for_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
// QuizeActivity.this.finish();
                skip++;
                if (currentQuestion < quizItems.size() + 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
// yourMethod();
                            progressDialog.dismiss();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200); //3 seconds
                }

                int arr = quizItems.size();
                int minus = arr - 1;

                if (currentQuestion == minus) {
// linear_for_skip.setVisibility(View.GONE);
                    linear_for_skip.setEnabled(false);
                }
            }
        });

        linear_for_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MegatestActivity.this.finish();
            }
        });

        finish_topic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MegatestActivity.this)
                        .setMessage("Are you sure you want to exit This Test")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MegatestActivity.super.onBackPressed();
                            }
                        }).setNegativeButton("No", null)
                        .show();
            }
        });
        ly_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);

                //check if answer is correct
                if (quizItems.get(currentQuestion).getAnswer1()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
                    attempt_question++;
                    //      Toast.makeText(MegatestActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
//                    option1.setBackgroundColor(R.drawable.question_rounded_corner);
//                    ly_option1.setBackground(getResources().getDrawable(R.drawable.question_rounded_corner));
                    ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));

                } else {
                    //wrong
                    wrong++;
                    attempt_question++;
                    //      Toast.makeText(MegatestActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
//                    option1.setBackgroundColor(R.drawable.question_red);
                    ly_option1.setBackground(getResources().getDrawable(R.drawable.round_bor_gray));
                    //  ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    // ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            //  duration_test = countdownView.getMinute();
                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);

                            progressDialog.dismiss();
                            // yourMethod();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds

                } else {
                    //end result
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            //   duration_test = countdownView.getMinute();

                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);

                            Intent i = new Intent(getApplicationContext(), MegaTestResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("attempt", attempt_question);
                            i.putExtra("test_hour", elapsedHours);
                            i.putExtra("test_minute", elapsedMinutes);
                            i.putExtra("test_sec", elapsedSeconds);
                            i.putExtra("test_id", test_id);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds

                }
                /*option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));*/
            }
        });


        ly_option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if answer is correct
                progressBar.setVisibility(View.VISIBLE);
                /*progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);*/
                if (quizItems.get(currentQuestion).getAnswer2()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
                    attempt_question++;
//                    option2.setBackgroundColor(R.drawable.question_rounded_corner);
//                    ly_option2.setBackground(getResources().getDrawable(R.drawable.question_rounded_corner));
                    ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //       Toast.makeText(MegatestActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
                } else {
                    //wrong
                    wrong++;
                    attempt_question++;
                    //     Toast.makeText(MegatestActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
//                    option2.setBackgroundColor(R.drawable.question_red);
//                    ly_option2.setBackground(getResources().getDrawable(R.drawable.question_red));
                    ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            //   duration_test = countdownView.getMinute();
                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);
                            progressDialog.dismiss();
                            // yourMethod();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds


                } else {
                    //end result

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            //  duration_test = countdownView.getMinute();
                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);

                            Intent i = new Intent(getApplicationContext(), MegaTestResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("attempt", attempt_question);
                            i.putExtra("test_hour", elapsedHours);
                            i.putExtra("test_minute", elapsedMinutes);
                            i.putExtra("test_sec", elapsedSeconds);
                            i.putExtra("test_id", test_id);
                            startActivity(i);
                            finish();

                        }
                    }, 200);   //3 seconds
                }
            }
        });

       /* option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
            }
        });*/
       /* option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                option1.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option2.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
                option3.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.question_rounded_corner));
                option4.setBackground(ContextCompat.getDrawable(QuizeActivity.this, R.drawable.round_bor_gray));
            }
        });*/

        ly_option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if answer is correct
                progressBar.setVisibility(View.VISIBLE);
                /*progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);*/
                if (quizItems.get(currentQuestion).getAnswer3()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
                    attempt_question++;
                    //     Toast.makeText(MegatestActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
//                    option3.setBackgroundColor(R.drawable.question_rounded_corner);
//                    ly_option3.setBackground(getResources().getDrawable(R.drawable.question_rounded_corner));
                    ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                } else {
                    //wrong
                    wrong++;
                    attempt_question++;
                    //     Toast.makeText(MegatestActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
//                    option3.setBackgroundColor(R.drawable.question_red);
//                    ly_option3.setBackground(getResources().getDrawable(R.drawable.question_red));
                    ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //  ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            //   duration_test = countdownView.getMinute();
                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);
                            progressDialog.dismiss();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 200);   //3 seconds

                } else {
                    //end result
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            //   duration_test = countdownView.getMinute();
                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);
                            Intent i = new Intent(getApplicationContext(), MegaTestResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("attempt", attempt_question);
                            i.putExtra("test_hour", elapsedHours);
                            i.putExtra("test_minute", elapsedMinutes);
                            i.putExtra("test_sec", elapsedSeconds);
                            i.putExtra("test_id", test_id);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds
                }
            }
        });


        ly_option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if answer is correct
                progressBar.setVisibility(View.VISIBLE);
                /*progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);*/
                if (quizItems.get(currentQuestion).getAnswer4()
                        .equals(quizItems.get(currentQuestion).getCorrect())) {
                    //correct
                    correct++;
                    attempt_question++;
                    //      Toast.makeText(MegatestActivity.this, "Correct Answer...!!!", Toast.LENGTH_SHORT).show();
//                    option4.setBackgroundColor(R.drawable.question_rounded_corner);
//                    ly_option4.setBackground(getResources().getDrawable(R.drawable.question_rounded_corner));
                    ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //     ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //    ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                } else {
                    //wrong
                    wrong++;
                    attempt_question++;
                    //   Toast.makeText(MegatestActivity.this, "Wrong Answer!! Correct Answer is: " + quizItems.get(currentQuestion).getCorrect(), Toast.LENGTH_SHORT).show();
//                    option4.setBackgroundColor(R.drawable.question_red);
//                    ly_option4.setBackground(getResources().getDrawable(R.drawable.question_red));
                    ly_option4.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option1.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option2.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                    //   ly_option3.setBackground(ContextCompat.getDrawable(MegatestActivity.this, R.drawable.round_bor_gray));
                }

                //load next Question
                if (currentQuestion < quizItems.size() - 1) {
                    currentQuestion++;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            progressDialog.dismiss();
                            btnBg();
                            setquestion(currentQuestion);
                            progressBar.setVisibility(View.INVISIBLE);
                            //   duration_test = countdownView.getMinute();
                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);
                        }
                    }, 200);   //3 seconds

                } else {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // yourMethod();
                            //     duration_test = countdownView.getMinute();

                            sec = countdownView.getSecond();
                            remain_time = countdownView.getRemainTime();
                            remain_minute = remain_time / 60000;
                            milisecond_duration = test_time * 60000;
                            printDifference(milisecond_duration, remain_time);
                            Intent i = new Intent(getApplicationContext(), MegaTestResultActivity.class);
                            i.putExtra("correct", correct);
                            i.putExtra("wrong", wrong);
                            i.putExtra("quizId", quize_data);
                            i.putExtra("attempt", attempt_question);
                            i.putExtra("test_hour", elapsedHours);
                            i.putExtra("test_minute", elapsedMinutes);
                            i.putExtra("test_sec", elapsedSeconds);
                            i.putExtra("test_id", test_id);
                            startActivity(i);
                            finish();
                        }
                    }, 200);   //3 seconds
                }
            }
        });

        get_test_data();


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                // yourMethod();
                Collections.shuffle(quizItems);
                //load first Question
                setquestion(currentQuestion);
                progressBar.setVisibility(View.INVISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                //   duration_test = countdownView.getMinute();
                sec = countdownView.getSecond();
                remain_time = countdownView.getRemainTime();
                remain_minute = remain_time / 60000;
            }
        }, 3000);   //2 seconds

        minute = countdownView.getMinute();
    }

    /*public void submit_test() {
        try {
            progressDialog.show();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<ResponseBody> call_submit_test = apiinterface.submit_test(login_token, user_id, test_id, duration_test, correct, wrong, attempt_question);

            call_submit_test.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String status = jsonObject.getString("status");
                        if (status.equals("1")) {
                            String msg = jsonObject.getString("msg");
                            Toast.makeText(MegatestActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                            finish();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
        }
    }*/

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit This Test")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MegatestActivity.super.onBackPressed();
                    }
                }).setNegativeButton("No", null)
                .show();
    }

    private void get_test_data() {
        try {
            progressDialog.show();
            Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
            Call<ResponseBody> call_test_detail = apiinterface.get_test_detail(prefManager.gettokenId(), prefManager.getLoginId(), test_id);
            call_test_detail.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        progressDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String success = jsonObject.getString("status");
                        if (success.equals("1")) {
                            JSONArray arr = jsonObject.getJSONArray("data");
                            JSONObject listObj = null;
                            for (int i = 0; i < arr.length(); i++) {
                                listObj = arr.getJSONObject(i);
                                String question = listObj.getString("question");
                                String a = listObj.getString("a");
                                String b = listObj.getString("b");
                                String c = listObj.getString("c");
                                String d = listObj.getString("d");
                                String correct = listObj.getString("right_ans");
                                quizItems.add(new QuizItem(question, a, b, c, d, correct));
                            }

                        } else if (success.equals("0")) {

                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
        }

    }

    private void setquestion(int i) {
        try {
            if (quizItems.size() > 0) {
                question = quizItems.get(i).getQuestion();
                txt_question.setText(question);
                option1.setText(quizItems.get(i).getAnswer1());
                option2.setText(quizItems.get(i).getAnswer2());
                option3.setText(quizItems.get(i).getAnswer3());
                option4.setText(quizItems.get(i).getAnswer4());
            } else {
//                Toast.makeText("")
            }
        } catch (Exception e) {
            Log.e("question==>", "" + e.getMessage());
        }
    }

    private void init() {
        txt_question = findViewById(R.id.txt_question);
        toolbar_title = findViewById(R.id.toolbar_title);

        option1 = findViewById(R.id.option1);
        option2 = findViewById(R.id.option2);
        option3 = findViewById(R.id.option3);
        option4 = findViewById(R.id.option4);

        linear_for_back = findViewById(R.id.linear_for_back);
        linear_for_skip = findViewById(R.id.linear_for_skip);
        finish_topic_list = findViewById(R.id.finish_topic_list);

        ly_option1 = findViewById(R.id.ly_option1);
        ly_option2 = findViewById(R.id.ly_option2);
        ly_option3 = findViewById(R.id.ly_option3);
        ly_option4 = findViewById(R.id.ly_option4);

        progressBar = findViewById(R.id.quiz_progress);
    }

    public void btnBg() {
        ly_option1.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
        ly_option2.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
        ly_option3.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
        ly_option4.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_bor_gray));
    }

    public void printDifference(long test_dur, long test_time) {
        //milliseconds
        different = test_dur - test_time;
        different1 = test_dur - test_dur;


        secondsInMilli = 1000;
        minutesInMilli = secondsInMilli * 60;
        hoursInMilli = minutesInMilli * 60;
        daysInMilli = hoursInMilli * 24;

        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;

    }

    public void submit_test() {
        progressDialog.show();
        Apiinterface apiinterface = Apiclient.getRetrofit().create(Apiinterface.class);
        Call<ResponseBody> call_submit_test = apiinterface.submit_test(login_token, user_id, test_id, duration_test, correct, wrong, attempt_question);

        call_submit_test.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(MegatestActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject_data = jsonObject.getJSONObject("data");
                        String coin = jsonObject_data.getString("coin");
                        if (!coin.equals("0")) {
                            dialog = new Dialog(MegatestActivity.this);
                            dialog.setContentView(R.layout.coin_dialog);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            TextView txt_coin = dialog.findViewById(R.id.txt_coin);
                            txt_coin.setText("you Earn" + coin + "Coins");

                            ImageView imageView = dialog.findViewById(R.id.img_for_cancel);
                            dialog.show();
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                        }
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
