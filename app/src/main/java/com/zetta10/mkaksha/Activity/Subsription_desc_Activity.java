package com.zetta10.mkaksha.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.rezwan.knetworklib.KNetwork;
import com.zetta10.mkaksha.Model.CoinModel.History;
import com.zetta10.mkaksha.Model.SubscriptionListModel.Data;
import com.zetta10.mkaksha.Model.SubscriptionListModel.SubscriptionListModel;
import com.zetta10.mkaksha.R;
import com.zetta10.mkaksha.Utility.PrefManager;
import com.zetta10.mkaksha.Webservice.AppAPI;
import com.zetta10.mkaksha.Webservice.BaseURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Subsription_desc_Activity extends AppCompatActivity implements PaymentResultListener {


    List<Data> PackagesDetail_list_data;
    PrefManager prefManager;
    ProgressDialog progressDialog;
    String api_token, user_id, Reference_number;
    Integer subscription_id;

    TextView txt_rs, txt_rs_discount, txt_all_subject, txt_std, txt_month_plan, txt_show_price, txt_promo_code_price,
            txt_total_pay_price, txt_wallet_pay_balance_price, txt_total_price_num, txt_promo_code, txt_promo_remove;
    LinearLayout linear_for_pay, finish_ch_list, linear_show_pay, ly_for_cancel;
    CheckBox checkBox, wallet_coints;

    String currency_code, state, description, create_time, promo_code;
    int coin_reedem, value;
    WebView web_desc_load;
    int final_value, remove_checker = 0, temp_val, minus_value, promo_final_value, check_promo = 0, promo_minus, check_wallet = 0, val, check_checkbox = 0, click = 0, check_for_remove = 0, promo_remove = 0, promo_remove_second = 0, promo_count = 0, check_promo_sec = 0, check_promo_wallet = 0, api_counter = 0, Price = 0;
    List<History> coin_dataList;
    String total_coin, discount, subject, name, sub_price, price;
    boolean isChecked;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subsription_desc__list_activity);
        KNetwork.INSTANCE.bind(this, getLifecycle()).setConnectivityListener(new KNetwork.OnNetWorkConnectivityListener() {
            @Override
            public void onNetConnected() {

            }

            @Override
            public void onNetDisConnected() {

            }
        });
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        prefManager = new PrefManager(Subsription_desc_Activity.this);
        progressDialog = new ProgressDialog(Subsription_desc_Activity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        init();
        // isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
        Log.e("isChecked==>", "" + isChecked);
        Intent intent = getIntent();
       /* promo_final_value = intent.getIntExtra("promo_final_value", 0);
        check_promo = intent.getIntExtra("check_promo", 0);*/
        promo_minus = intent.getIntExtra("promo_minus", 0);
        promo_code = intent.getStringExtra("promo_code");
       /* check_wallet = intent.getIntExtra("check_wallet_data", 0);
        minus_value = intent.getIntExtra("minus_value_data", 0);
        check_for_remove = intent.getIntExtra("check_for_remove", 0);*/
        // promo_remove = intent.getIntExtra("promo_remove_data", 0);
        //  Log.e("promo_remove==>", "" + promo_remove);
        //  remove_checker = intent.getIntExtra("remove_checker_data", 0);
        //  promo_remove_second = intent.getIntExtra("promo_remove_second_data", 0);
        //  promo_count = intent.getIntExtra("promo_count_data", 0);
        //  check_promo_wallet = intent.getIntExtra("check_promo_wallet_data", 0);
        Price = intent.getIntExtra("Price", 0);
        discount = intent.getStringExtra("discount");
        description = intent.getStringExtra("description");
        create_time = intent.getStringExtra("create_time");
        subject = intent.getStringExtra("subject");
        name = intent.getStringExtra("name");
        api_counter = intent.getIntExtra("api_counter", 0);
        isChecked = intent.getBooleanExtra("ischecked", false);
        sub_price = intent.getStringExtra("sub_price");
        coin_reedem = intent.getIntExtra("coin_reedem", 0);
//        check_promo_sec = intent.getIntExtra("check_promo_sec_data", 0);

        Log.e("promo_minus1==>", "" + promo_minus);
        setdata();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            subscription_id = bundle.getInt("subscription_id");
            Log.e("chapter_id", "" + subscription_id);
            coin_Data();
            if (api_counter == 0) {
                api_counter++;
                subscription_desc_list(subscription_id);
            }
        }


        if (promo_code == null) {
            txt_promo_code.setText("Add Promo Code");
            txt_promo_remove.setVisibility(View.GONE);
        } else {
            txt_promo_code.setText(promo_code);
            txt_show_price.setText("Rs. " + Price);
            txt_promo_code.setEnabled(false);
            txt_promo_remove.setVisibility(View.VISIBLE);
        }

        if (isChecked) {
            wallet_coints.setChecked(true);
        }

        txt_rs_discount.setPaintFlags(txt_rs_discount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        linear_for_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("minus_value====>", "" + minus_value);
//                startPayment(PackagesDetail_list_data.get(0).getNewPrice() + "00", PackagesDetail_list_data.get(0).getName());
                startPayment(Price + "00", name);
            }
        });


        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    linear_for_pay.setVisibility(View.VISIBLE);
                } else {
                    linear_for_pay.setVisibility(View.INVISIBLE);
                }
            }
        });

        txt_promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Subsription_desc_Activity.this, PromoCodeActivity.class);

                //  intent.putExtra("promo_value", temp_val);
                //    intent.putExtra("subscription_id_data", subscription_id);
                //    intent.putExtra("check_wallet", check_wallet);
                //    intent.putExtra("minus_value", minus_value);
                //    intent.putExtra("check_for_remove", check_for_remove);
                //    intent.putExtra("promo_remove", promo_remove);
                //    intent.putExtra("remove_checker", remove_checker);
                //    intent.putExtra("promo_remove_second", promo_remove_second);
                //    intent.putExtra("promo_count", promo_count);
                //     intent.putExtra("check_promo_sec", check_promo_sec);
                //    intent.putExtra("check_promo_wallet", check_promo_wallet);
                intent.putExtra("ischecked", isChecked);
                intent.putExtra("Price", Price);
                intent.putExtra("discount", discount);
                intent.putExtra("description", description);
                intent.putExtra("create_time", create_time);
                intent.putExtra("subject", subject);
                intent.putExtra("name", name);
                intent.putExtra("api_counter", api_counter);
                intent.putExtra("ischecked", isChecked);
                intent.putExtra("sub_price", sub_price);
                intent.putExtra("coin_reedem", coin_reedem);
                /*intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                startActivity(intent);
                //  finish();
            }
        });

        wallet_coints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    /*if (check_promo == 1) {*/
                    if (((CheckBox) view).isChecked()) {
                        isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
                        int coins = Integer.parseInt(total_coin);
                        price = String.valueOf(Price);
                        int temp = Integer.parseInt(price);
                        coin_reedem = temp * 25 / 100;
                        if (coins < coin_reedem) {
                            Price = Price - coins;
                            minus_value = coins;
                            Log.e("total_coins1==>", "" + Price);
                            Log.e("minus_value==>", "" + minus_value);
                            txt_show_price.setText("Rs. " + Price);
                        } else if (coins > coin_reedem) {
                            Price = (int) (Price - coin_reedem);
                            minus_value = (int) coin_reedem;
                            Log.e("total_coins2==>", "" + Price);
                            Log.e("minus_value==>", "" + minus_value);
                            txt_show_price.setText("Rs. " + Price);
                        }
                        /*    // check_wallet = 1;
                            check_for_remove = 1;
                             isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
                            int total_coins = Integer.parseInt(total_coin);
                            if (total_coins <= 0) {
                                Toast.makeText(Subsription_desc_Activity.this, "Not Any Coins....", Toast.LENGTH_LONG).show();
                            }
                            if (check_wallet == 1) {
                                // check_wallet = 0;
                                if (promo_remove == 1) {
                                    if (promo_count >= 2) {
                                        if (check_promo_sec == 1) {
                                            check_promo_sec = 2;
                                            val = temp_val;
                                        } else {
                                            val = promo_final_value;
                                        }
                                        check_promo_sec = 2;
                                    } else {
                                        val = temp_val;
                                    }
                                } else {
                                    val = temp_val;
                                }
                            } else {
                                if (promo_remove == 1) {
                                    if (promo_count >= 2) {
                                        if (check_promo_sec == 1) {
                                            check_promo_sec = 2;
                                            val = temp_val;
                                        } else {
                                            val = promo_final_value;
                                        }
                                        check_promo_sec = 2;
                                    } else {
                                        val = temp_val;
                                    }
                                } else {
                                    val = promo_final_value;
                                }
                            }
                            Log.e("total_coins==>", "" + total_coin);
                            int total_con = Integer.parseInt(total_coin);
                            if (total_con < final_price) {
                                temp_val = val - total_con;
                                minus_value = total_con;
                                check_checkbox = 1;
                                Log.e("total_coins1==>", "" + temp_val);
                            } else if (total_con > final_price) {
                                temp_val = (int) (val - final_price);
                                minus_value = (int) final_price;
                                check_checkbox = 1;
                                Log.e("total_coins2==>", "" + temp_val);
                            }
                            txt_show_price.setText("Rs. " + temp_val);*/
                    } else {
                        isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
                        int coins = Integer.parseInt(total_coin);
                        price = String.valueOf(Price);
                        if (coins < coin_reedem) {
                            Price = Price + coins;
                            minus_value = 0;
                            Log.e("total_coins1==>", "" + Price);
                            Log.e("minus_value==>", "" + minus_value);
                            txt_show_price.setText("Rs. " + Price);
                        } else if (coins > coin_reedem) {
                            Price = (int) (Price + coin_reedem);
                            minus_value = 0;
                            Log.e("total_coins2==>", "" + Price);
                            Log.e("minus_value==>", "" + minus_value);
                            txt_show_price.setText("Rs. " + Price);
                        }
                            /*check_for_remove = 2;
                            isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
                            if (check_wallet == 1) {
//                                temp_val = Integer.parseInt(Price);
                                int t_coin = Integer.parseInt(total_coin);
                                if (promo_remove == 1) {
                                    if (promo_count >= 2) {
                                        //check_promo_sec = 2;
                                        if (check_promo_sec == 2) {
                                          */

                            /*  if (check_promo_sec == 2)
                                            val = temp_val;*/
                            /*
                                            temp_val = temp_val + t_coin;
                                            check_promo_sec = 1;
                                        } else {
                                            temp_val = promo_final_value + t_coin;
                                            check_promo_sec = 1;
                                        }
                                        */

                            /* txt_show_price.setText("Rs. " + temp_val);*//*
                                    } else {
                                        int t_coin2 = Integer.parseInt(total_coin);
                                        temp_val = temp_val + t_coin2;
                                    }
                                } else {
                                    temp_val = promo_final_value + t_coin;
                                }
                                minus_value = 0;
                                check_checkbox = 1;
                                txt_show_price.setText("Rs. " + temp_val);
                            } else {
                                // int t_coin = Integer.parseInt(total_coin);
                                if (promo_remove == 1) {
                                    Log.e("temp_val_out===>", "" + temp_val);
                                    int t_coins = Integer.parseInt(total_coin);
                                    *//* String t_price = String.valueOf(temp_val + t_coins);*//*
                                    if (promo_remove_second == 1) {
                                        if (promo_count >= 2) {
                                            if (check_promo_sec == 2) {
                                                temp_val = temp_val + t_coins;
                                                check_promo_sec = 1;
                                                txt_show_price.setText("Rs. " + temp_val);
                                            } else {
                                                check_promo_sec = 1;
                                                temp_val = promo_final_value + t_coins;
                                                txt_show_price.setText("Rs. " + temp_val);
                                            }
                                        } else {
                                            temp_val = temp_val + t_coins;
                                            Log.e("temp_vals===>", "" + temp_val);
                                            txt_show_price.setText("Rs. " + temp_val);
                                        }
//                                        promo_remove_second = 1;
//                                        promo_remove_second = 2;
                                    } else {
                                        int t_coin3 = Integer.parseInt(total_coin);
                                        temp_val = promo_final_value + t_coin3;
                                        txt_show_price.setText("Rs. " + temp_val);
//                                        promo_remove_second = 0;
                                    }

                                } else {
                                    temp_val = promo_final_value;
                                    minus_value = 0;
                                    check_checkbox = 1;
                                    txt_show_price.setText("Rs. " + temp_val);
                                }
//                                Log.e("promo_remove_second==>", "" + promo_remove_second);
                               *//* if (promo_remove_second == 1) {
                                    int t_coin3 = Integer.parseInt(total_coin);
                                    temp_val = promo_final_value + t_coin3;
                                    txt_show_price.setText("Rs. " + temp_val);
                                }*//*
                            }*/

                    }

                    /* }*/



                   /* else if (check_promo == 0) {
                        if (((CheckBox) view).isChecked()) {
                            isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
                            check_wallet = 1;
                            int total_coins = Integer.parseInt(total_coin);
                            if (total_coins <= 0) {
                                Toast.makeText(Subsription_desc_Activity.this, "Not Any Coins....", Toast.LENGTH_LONG).show();
                            }
                            int val = Integer.parseInt(Price);
                            Log.e("total_coins==>", "" + total_coin);
                            int total_con = Integer.parseInt(total_coin);
                            if (total_con < final_price) {
                                temp_val = val - total_con;
                                minus_value = total_con;
                                Log.e("total_coins1==>", "" + temp_val);
                            } else if (total_con > final_price) {
                                temp_val = (int) (val - final_price);
                                minus_value = (int) final_price;
                                Log.e("total_coins2==>", "" + temp_val);
                            }
                            txt_show_price.setText("Rs. " + temp_val);
                        } else {
                            check_wallet = 0;
                            isChecked = ((CheckBox) findViewById(R.id.wallet_coints)).isChecked();
                            if (check_promo == 1) {
//                                temp_val = Integer.parseInt(Price);
                                temp_val = promo_final_value;
                                minus_value = 0;
                                txt_show_price.setText("Rs. " + temp_val);
                            } else {
                                temp_val = Integer.parseInt(Price);
                                minus_value = 0;
                                txt_show_price.setText("Rs. " + temp_val);
                            }
                        }
                    }*/


                } catch (
                        Exception e) {
                    Log.e("data==>", "" + e.getMessage());
                }

            }
        });

        finish_ch_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), SubscriptionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        linear_show_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(Subsription_desc_Activity.this, R.style.DialogTheme);
                dialog.setContentView(R.layout.payment_info_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                txt_total_pay_price = dialog.findViewById(R.id.txt_total_pay_price);
                txt_promo_code_price = dialog.findViewById(R.id.txt_promo_code_price);
                txt_wallet_pay_balance_price = dialog.findViewById(R.id.txt_wallet_pay_balance_price);
                txt_total_price_num = dialog.findViewById(R.id.txt_total_price_num);

                txt_total_pay_price.setText(sub_price);
                int t_coin = Integer.parseInt(total_coin);
                if (isChecked) {
                    if (t_coin < coin_reedem) {
                        txt_wallet_pay_balance_price.setText("- " + total_coin);
                    }
                    if (t_coin > coin_reedem) {
                        txt_wallet_pay_balance_price.setText("- " + coin_reedem);
                    }
                }
                if (isChecked == false) {
                    txt_wallet_pay_balance_price.setText("0");
                }
                if (promo_code != null) {
                    String promo_deduct_amount = String.valueOf(promo_minus);
                    txt_promo_code_price.setText("- " + promo_deduct_amount);
                }

                if (promo_code == null) {
                    txt_promo_code_price.setText("0");
                }

                txt_total_price_num.setText("" + Price);
                ly_for_cancel = dialog.findViewById(R.id.ly_for_cancel);

                dialog.show();
                ly_for_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        txt_promo_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (click == 0) {
                    click = 1;
                    txt_promo_code.setEnabled(true);
                    txt_promo_code.setText("Add Promo Code");
                    txt_promo_remove.setVisibility(View.GONE);
                    promo_code = null;
                    Price = Price + promo_minus;
                    price = String.valueOf(Price);
                    txt_show_price.setText("Rs. " + price);

                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(), SubscriptionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void init() {
        txt_rs = findViewById(R.id.txt_rs);
        txt_promo_code = findViewById(R.id.txt_promo_code);
        txt_std = findViewById(R.id.txt_std);
        txt_month_plan = findViewById(R.id.txt_month_plan);
        txt_show_price = findViewById(R.id.txt_show_price);
        txt_rs_discount = findViewById(R.id.txt_rs_discount);
        web_desc_load = findViewById(R.id.web_desc_load);
        txt_all_subject = findViewById(R.id.txt_all_subject);
        linear_for_pay = findViewById(R.id.linear_for_pay);
        checkBox = findViewById(R.id.checkBox);
        wallet_coints = findViewById(R.id.wallet_coints);
        finish_ch_list = findViewById(R.id.finish_ch_list);
        linear_show_pay = findViewById(R.id.linear_show_pay);
        txt_promo_remove = findViewById(R.id.txt_promo_remove);
    }

    public void setdata() {
        txt_month_plan.setText(name);
        final WebSettings webSettings = web_desc_load.getSettings();
        webSettings.setTextSize(WebSettings.TextSize.SMALLEST);
        webSettings.setDefaultFontSize(30);//Larger number means larger font size

        String data = description;
        web_desc_load.loadData(data, "text/html", "UTF-8");
        price = String.valueOf(Price);
        txt_rs.setText(sub_price);
        txt_rs_discount.setText(discount);
        txt_all_subject.setText(subject);
        txt_show_price.setText("Rs. " + price);

    }

    private void subscription_desc_list(Integer subscription_id) {
        try {
            progressDialog.show();
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<SubscriptionListModel> call = appAPI.PackagesDetail_call(api_token, user_id, subscription_id);
            call.enqueue(new Callback<SubscriptionListModel>() {
                @Override
                public void onResponse(Call<SubscriptionListModel> call, Response<SubscriptionListModel> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        Log.e("data2", "" + response.body().getData());
                        PackagesDetail_list_data = new ArrayList<>();
                        PackagesDetail_list_data = response.body().getData();
                        Log.e("dataList==>", "" + PackagesDetail_list_data.size());
                        if (PackagesDetail_list_data.size() > 0) {
                            price = PackagesDetail_list_data.get(0).getNewPrice();
                            sub_price = price;
                            Price = Integer.parseInt(price);
                            description = PackagesDetail_list_data.get(0).getDescription();
                            create_time = PackagesDetail_list_data.get(0).getDuration();
                            discount = PackagesDetail_list_data.get(0).getOldPrice();
                            subject = PackagesDetail_list_data.get(0).getSubjects();
                            name = PackagesDetail_list_data.get(0).getName();
                           /* txt_rs.setText(PackagesDetail_list_data.get(0).getNewPrice());
                            txt_rs_discount.setText(PackagesDetail_list_data.get(0).getOldPrice());
                            txt_all_subject.setText(PackagesDetail_list_data.get(0).getSubjects());*/
//                            txt_term_condition.setText(PackagesDetail_list_data.get(0).getDescription());
                           /* final WebSettings webSettings = web_desc_load.getSettings();
                            webSettings.setTextSize(WebSettings.TextSize.SMALLEST);
                            webSettings.setDefaultFontSize(30);//Larger number means larger font size*/

                            //  String data = PackagesDetail_list_data.get(0).getDescription();
                            //  web_desc_load.loadData(data, "text/html", "UTF-8");
//                            txt_std.setText(PackagesDetail_list_data.get(0).getName());
                            //   txt_month_plan.setText(PackagesDetail_list_data.get(0).getName());
                            setdata();

                           /* temp_val = Integer.parseInt(Price);
                            if (check_promo != 1) {
                                txt_show_price.setText("Rs. " + temp_val);
                            }*/

                          /*  if (check_wallet == 1) {
                                // check_wallet = 0;
                                txt_show_price.setText("Rs. " + temp_val);
                            }else {
                                txt_show_price.setText("Rs. " + promo_final_value);
//                                val = promo_final_value;
                            }*/


//                            String value = "1000";
                            /*float temp = Float.parseFloat(Price);
                            final_price = temp * 25 / 100;

                            value = temp - final_price;


                            final_value = (int) value;*/

                            Log.e("f_price==>", "" + coin_reedem);
                            Log.e("f_price_==>", "" + value);
                            Log.e("f_price_done==>", "" + final_value);
                            Log.e("temp_val_done==>", "" + temp_val);

                        } else {
//                            rv_premium_list.setVisibility(View.GONE);
//                            ly_paid_book.setVisibility(View.GONE);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<SubscriptionListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }

    }

    @Override

    public void onPaymentSuccess(String s) {
        Log.e("on_success", "" + s);
        Toast.makeText(getApplicationContext(), "Payment Successfull", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Payment Successful: " + s, Toast.LENGTH_SHORT).show();
        currency_code = "INR";
        state = "approved";
        Log.e("successfully", "Payment Successfull");
        Log.e("s_id", "" + subscription_id);
        Log.e("u_id", "" + prefManager.getLoginId());
        Log.e("s_amout", "" + value);
        Log.e("s_currency_code", "" + currency_code);
        Log.e("s_short_description", "" + description);
        Log.e("s_create_time", "" + create_time);
        Log.e("minus_value", "" + minus_value);
//        Purchasebook();
        Add_amount_purchase(s, sub_price);
//        recreate();
    }

    private void Add_amount_purchase(String t_id, String Price) {

        try {
            user_id = prefManager.getLoginId();
            api_token = prefManager.gettokenId();

            Log.e("api_token==>", "" + api_token);
            Log.e("user_id==>", "" + user_id);
            Log.e("subscription_id==>", "" + subscription_id);
            Log.e("t_id==>", "" + t_id);
            Log.e("Price==>", "" + Price);
            Log.e("Reference_number==>", "" + Reference_number);

            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<ResponseBody> call = appAPI.AddTransaction_call(api_token, user_id, subscription_id, t_id, Price, Reference_number, 0, minus_value);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Log.e("data1", "" + response.body());
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            Log.e("output==>", "" + jsonObject);
                            String messge = jsonObject.getString("msg");
                            Log.e("output==>", "" + messge);
                            Toast.makeText(Subsription_desc_Activity.this, "Payment Successful: " + messge, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
//                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error_chapter=>", "" + e.getMessage());
        }

    }

    @Override
    public void onPaymentError(int i, String s) {
        Log.d("cancle", "Cancelled");
        Toast.makeText(Subsription_desc_Activity.this, s, Toast.LENGTH_SHORT).show();
    }

    /*================ Razor Pay =========================*/
    public void startPayment(String amount, String title) {
        Log.e("amount_pay", "" + amount);
        Log.e("book_title", "" + title);
        Checkout checkout = new Checkout();
        //checkout.setImage(R.drawable.logo);
        final Activity activity = this;
        try {
            final int min = 200000;
            final int max = 800000;
            final int random = new Random().nextInt((max - min) + 1) + min;
            Reference_number = prefManager.getLoginId() + "#" + random;
            Log.e("random===>", "" + random);

            JSONObject options = new JSONObject();
            options.put("name", title);
            options.put("amount", amount);
            options.put("description", Reference_number);
            options.put("currency", "INR");
            checkout.open(activity, options);
        } catch (Exception e) {
            Log.e("error", "Error in starting Razorpay Checkout", e);
            Log.e("error_msg", "msg" + e.getMessage());
        }
    }


    /*================ End Razor Pay =========================*/


    private void coin_Data() {
        try {
            progressDialog.show();
            Log.e("login_id", "" + prefManager.getLoginId());
            Log.e("==>login_token", "" + prefManager.gettokenId());
            AppAPI appAPI = BaseURL.getVideoAPI();
            Call<ResponseBody> call = appAPI.CoinList(prefManager.gettokenId(), prefManager.getLoginId());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    coin_dataList = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = String.valueOf(jsonObject.get("msg"));
                        Log.e("msg", "" + msg);

//                        txt_total_coin.setText();
                        total_coin = jsonObject.getJSONObject("data").getString("total_coin");

                        Log.e("data=>", "" + jsonObject.getJSONObject("data").getString("total_coin"));

                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("history");
                        JSONObject listObj = null;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            listObj = jsonArray.getJSONObject(i);
                            String id = listObj.getString("id");
                            String operation = listObj.getString("operation");
                            String massage = listObj.getString("massage");
                            String value = listObj.getString("value");
                            String student_id = listObj.getString("student_id");
                            String date = listObj.getString("date");
                            coin_dataList.add(new History(id, operation, massage, value, student_id, date));
                        }
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("er_error==>", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("error==>", "" + e.getMessage());
        }

    }


    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog != null)
            progressDialog.dismiss();
    }

}
