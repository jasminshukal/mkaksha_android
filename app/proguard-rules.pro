-dontwarn okhttp3.**
-dontwarn okio.**
-keepattributes *Annotation*
-dontwarn com.razorpay.**
-keep class com.razorpay.** {*;}
-optimizations !method/inlining/
-keepclasseswithmembers class * {
  public void onPayment*(...);
}
-dontwarn javax.annotation.**
-dontwarn org.conscrypt.**
-dontwarn io.realm.**

-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
-keep class  com.zetta10.mkaksha.Webservice.**{*;}
-keep class  com.zetta10.mkaksha.Model.ChapaterListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.BannerListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.ChapterAllModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.CommentModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.LikeModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.WatchListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.PackagesListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.PdfViewerModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.pojoclass.**{*;}
-keep class  com.zetta10.mkaksha.Model.PrivacyModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.QuizModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.SubjectListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.MySubjectWiseModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.MyJourneyModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.PromoModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.SubscriptionListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.SuccessfullModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.SuggetionModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.TopicListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.VideoDesListModel.**{*;}
-keep class  com.zetta10.mkaksha.Model.VideoListModel.**{*;}
-keep class com.zetta10.mkaksha.Model.NotificationModel.** {*;}
-keep class com.zetta10.mkaksha.Model.RewardModel.** {*;}
-keep class  com.zetta10.mkaksha.Apiclient.**{*;}
-keep class  com.zetta10.mkaksha.Activity.**{*;}
-keep class com.google.android.exoplayer.** {*;}
-keep class com.github.mikephil.charting.** { *; }
-keepclassmembers class com.zetta10.mkaksha.Activity.**{*;}
-keepclassmembernames class  com.zetta10.mkaksha.Activity.**{*;}


#-keepattributes Signature
#-keepattributes *Annotation*
#-keep class okhttp3.** { *; }
#-keep interface okhttp3.** { *; }
